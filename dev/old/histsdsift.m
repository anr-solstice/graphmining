function [ result ] = histsdsift(conf, set, filename)
%% Function that create histogram from BOW by knnsearch 
%
%Syntaxe
% [ result ] = hist(conf, set)
%
%Parameters
% All parameter are required
% - Conf (required)
%       -> Configuration struct. Cf config for more information.
% TODO : Complete this documentation
% - Set (required)
% -> Sub structure from conf with a leas this information : 
%       Set.imgIdx : Index of all images
%       Set.nbImg : Number of image in the set.
%
%Description
% Return one struct : 
%   - result.hknncell : Cell arrays with hist for each image.
%   - result.histsknn : Array with all hist for each image.
%   - result.kern     : Kernel computed with vl_homkermap.
% =========================================================================
%% Compute Hist ===========================================================
if ( nargin > 2 )
    temp.fileId = fopen(sprintf('%s',filename),'a');
    fprintf(temp.fileId,'%% === Compute Histogramms for Imgset, , \n');
end

if strcmp('knn', conf.quantizer )
    if ( nargin > 2 )
        [ tmp.hist , tmp.clust ] = knn(conf,set,temp.fileId) ;
    else
        [ tmp.hist , tmp.clust ] = knn(conf,set) ;
    end
else
    if ( nargin > 2 )
        [ tmp.hist , tmp.clust ] = kdtree(conf,set,temp.fileId) ;
    else
        [ tmp.hist , tmp.clust ] = kdtree(conf,set) ;
    end
    
end    
fclose(temp.fileId);

result.hknnclust = tmp.clust;
result.hknncell = tmp.hist ;
result.histsknn = cat(2,tmp.hist{:}) ;
result.kern = vl_homkermap(result.histsknn, 1, 'kchi2', 'gamma', .5) ;


% =========================================================================
%% Subfunction clusterize with knn ========================================
function [ hist , clust ] = knn(conf,set,fileId)
cinit = clock;
tmpd = {};
tmpdc = {}; % Temporary descriptor clustered
parfor i=1:set.nbImg
    c0 = clock;
    imc=imread(sprintf('%s%d.jpg', conf.basedirimg, set.imgIdx(i)));
    % Convert into gray image
    imb = single(rgb2gray(imc)) ;
    % Applying dsift
    [~,tmpd{i}] = vl_dsift(imb,'size', conf.dsift.bin ,...
                               'step', conf.dsift.step);
    c1 = clock;
    if ( nargin > 2 )
        fprintf(fileId,'vl_dsift,%03d.jpg,%d\n',set.imgIdx(i),etime(c1,c0));
    else
        fprintf('vl_dsift,%03d.jpg,%d\n',set.imgIdx(i),etime(c1,c0));
    end
    tmpdc{i} = knnsearch(conf.kmean.vocab', single(tmpd{i}'));
    tmpd{i} = histc(tmpdc{i},1:conf.kmean.nbwords);
    c2 = clock;
    if ( nargin > 2 )
        fprintf(fileId,'knnsearch,%03d.jpg,%d \n', set.imgIdx(i), etime(c2,c1));
        fprintf(fileId,'hist,%d.jpg,%d \n', set.imgIdx(i), etime(c2,c0));
    else
        fprintf('knnsearch,%03d.jpg,%d \n', set.imgIdx(i), etime(c2,c1));
        fprintf('hist,%d.jpg,%d \n', set.imgIdx(i), etime(c2,c0));
    end
    c(i)=etime(c2,c0);
end
cend = clock;
if ( nargin > 2 )
    fprintf(fileId,'histograms,%d,%d \n', etime(cend,cinit), mean(c));
else
    fprintf('histograms,%d,%d \n', etime(cend,cinit), mean(c));
end
hist = tmpd;
clust = tmpdc;
end

%% Subfunction clusterize with kdtree =====================================
function [ hist , clust ] = kdtree(conf,set,fileId)
cinit = clock;
tmpd = {};
tmpdc = {}; % Temporary descriptor clustered
parfor i=1:set.nbImg
    c0 = clock;
    imc=imread(sprintf('%s%d.jpg', conf.basedirimg, set.imgIdx(i)));
    % Convert into gray image
    imb = single(rgb2gray(imc)) ;
    % Applying dsift
    [~,tmpd{i}] = vl_dsift(imb);
    c1 = clock;
    if ( nargin > 2 )
        fprintf(fileId,'vl_dsift,%03d.jpg,%d\n',set.imgIdx(i),etime(c1,c0));
    else
        fprintf('vl_dsift,%03d.jpg,%d\n',set.imgIdx(i),etime(c1,c0));
    end
    tmpdc{i} = vl_kdtreequery(conf.kmean.kdtree, ...
                                          conf.kmean.vocab, ...
                                          single(tmpd{i}), ...
                                          'MaxComparisons', 50);
    tmpd{i} = histc(double(tmpdc{i})) ;
    c2 = clock;
    if ( nargin > 2 )
        fprintf(fileId,'kdtree,%03d.jpg,%d \n', set.imgIdx(i), etime(c2,c1));
        fprintf(fileId,'hist,%d.jpg,%d \n', set.imgIdx(i), etime(c2,c0));
    else
        fprintf('kdtree,%03d.jpg,%d \n', set.imgIdx(i), etime(c2,c1));
        fprintf('hist,%d.jpg,%d \n', set.imgIdx(i), etime(c2,c0));
    end
    c(i)=etime(c2,c0);
end
cend = clock;
if ( nargin > 2 )
    fprintf(fileId,'histograms,%d,%d \n', etime(cend,cinit), mean(c));
else
    fprintf('histograms,%d,%d \n', etime(cend,cinit), mean(c));
end
hist = tmpd;
end

end % End of function
