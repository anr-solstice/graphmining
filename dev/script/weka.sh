#!/bin/bash

DATA=../weka_B3S1/*.arff
HASH=-
#CLASSIF=rules.ZeroR trees.J48 bayes.NaiveBayes rules.JRip meta.ClassificationViaRegression functions.SMO lazy.KStar
#"lazy.KStar -E" "lazy.IBk -K 1" "lazy.IBk -K 2" "lazy.IBk -K 3" "lazy.IBk -K 4" "lazy.IBk -K 2 -I" "lazy.IBk -K 3 -I" "lazy.IBk -K 4 -I" "bayes.NaiveBayes -K" 
##################
# Classification en laissant weka gérer la cross-validation (avec nbFolds folders)
##################
export CLASSPATH=/home/rdeville/Documents/PFE-Fouille_de_graph/toolbox/classifier/weka-3-7-10/weka.jar:$CLASSPATH
export LANG=en
combi=algos-2-10
nbFolds=10
echo $CLASSPATH
for f in ${DATA}
do
    for classifier in trees.J48 bayes.NaiveBayes rules.JRip meta.ClassificationViaRegression functions.SMO
    do
    IFS=- 
    set $f
    d1=$1 ; d2=$2 ; d3=$3 
    r1=${d1/.\/weka_B3S1\//.\/weka_B3S1res\/} ; r2=$2 ; r3=${d3/arff/txt}
    echo $s
    case  $d2  in 
    "Train" )	
        echo "--> Cross Validation" 
	echo "****** classifier = $classifier"
	echo "****** classifier = $classifier" >> $r1-$r2-$r3
        echo "java weka.classifiers.$classifier -t $d1-$d2-$d3 -x $nbFolds  >> $r1-$r2-$r3"
        java -cp weka.jar weka.classifiers.$classifier -t $d1-$d2-$d3 -x $nbFolds  >> $r1-$r2-$r3
	echo >> $r1-$r2-$r3
	;;
    "Val" )
        echo "--> Validation with Val file"
	echo "****** classifier = $classifier"
	echo "****** classifier = $classifier" >> $r1-$r2-$r3
        java -cp weka.jar weka.classifiers.$classifier -t $d1-Train-$d3 -T $d1-$d2-$d3 >> $r1-$r2-$r3
	echo >> $r1-$r2-$r3
	;;
#    "Test" )
#        echo "--> Validation with Test file"
#	echo "****** classifier = $classifier"
#        echo "****** classifier = $classifier" >> $r1-$r2-$r3
#        java weka.classifiers.$classifier -t $d1-Train-$d3 -T $d1-$d2-$d3 >> $r1-$r2-$r3
#        echo >> $r1-$r2-$r3
#	;;
    esac
    done
done
