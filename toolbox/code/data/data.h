/***************************************************************************
 *   Copyright (C) 2010,2011 by Adriana Prado and Baptiste Jeudy           *
 *   baptiste.jeudy at univ-st-etienne fr                                  *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#ifndef DATA_H
#define DATA_H

#include <cstring>
#include <cstdlib>

#include <fstream>
#include <string>
#include <vector>
#include <iostream>

using namespace std;

class Data
{
public:

	Data(const string& fn); 
	Data(const string& fn, ostream *stream);
	~Data();

	void getLine(vector<string>* tokens);
	bool finished();
	int get_line_nb(); //baptiste
private:
	int line_nb; // baptiste
	ifstream in;
        ostream *out;
};

#endif


/*int main(int argc, char* argv[])
{
	Data data(argv[1]);

	vector<string> tokens;
	while (!data.finished())
	{
		data.getLine(&tokens);

		for (size_t i = 0; i < tokens.size(); i++)
			cout << tokens[i] << " " << endl;

		cout << endl;
	}

	return 0;
} */
