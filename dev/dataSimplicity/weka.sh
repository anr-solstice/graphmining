#!/bin/bash

if ! [ -z $1 ]
then
DATA=$1
HASH=-

##################
# Classification en laissant weka gérer la cross-validation (avec nbFolds folders)
##################
export CLASSPATH=/home/rdeville/Documents/PFE-Fouille_de_graph/toolbox/classifier/weka-3-7-10/weka.jar:$CLASSPATH
export LANG=en
combi=algos-2-10
nbFolds=10

touch weka_run.sh ; chmod 755 weka_run.sh;

echo $DATA

for f in ${DATA}*
do
    echo $f
    for classifier in functions.SMO
    do
    IFS=- 
    set $f
    d1=$1 ; d2=$2 ; d3=$3 ; d4=$4 
    r1=${d1/.\/wekafile\//.\/wekaresult\/} ; r2=$2 ; r3=$d3 ; r4=${d4/arff/txt}
    case  $d3  in 
# =============== CROS VALIDATION - QUITE USELESS
#    "Train" )	
#        echo "--> Cross Validation" 
#	     echo "****** classifier = $classifier"
#	     echo "****** classifier = $classifier" >> $r1-$r2-$r3
#        echo "java weka.classifiers.$classifier -t $d1-$d2-$d3 -x $nbFolds  >> $r1-$r2-$r3"
#        java weka.classifiers.$classifier -t $d1-$d2-$d3 -x $nbFolds  >> $r1-$r2-$r3
#	    echo >> $r1-$r2-$r3
#	    ;;
    "Val" )
    	echo "echo '****** classifier = $classifier'" >> weka_run.sh ;
	    echo "echo '****** classifier = $classifier' >> $r1-$r2-$r3-$r4" >> weka_run.sh ;
        echo "java -cp ./weka.jar:./LibSVM.jar weka.classifiers.$classifier -t $d1-$d2-Train-$d4 -T $d1-$d2-$d3-$d4 >> $r1-$r2-$r3-$r4" >> weka_run.sh;
	    ;;
    esac
    done
done

./weka_run.sh;

rm weka_run.sh;

else
    echo "Please provide weka file location like this : weka.sh /path/to/wekafile/"
fi
