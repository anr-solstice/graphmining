/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *   Copyright (C) 2010,2011 by Adriana Prado and Baptiste Jeudy           *
 *   baptiste.jeudy at univ-st-etienne fr                                  *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef GGRAPHCODE_H
#define GGRAPHCODE_H

#include "global.h"
#include "string.h"
#include <vector>

/* The GGraphCode consists of a sequence of Tokens. Each token correspond
 to an edge and is represented by: (i, j, label_i, label_edge, label_j)

 The index of the nodes (here i and j) are not the index of the node in the
 graph representation. The node are numbered as they appear in the code.

 This is valid a code of a triangle :
 (0,1,..,..,..)(1,2,..,..,..)(2,0,..,..,..)
 The last token is 'backward' because its destination node already appears
 before in the code.
*/
class GGraph;

enum GTokenDirection { backward, forward };

struct GToken {
  GNodeID from, to; // ends of the edges (not the same id as in the graph!)
  GNodeData fromdata; // label of "from"
  GEdgeData data; // label of edge
  GNodeData todata; // label of "to"
  GTokenDirection direction; // forward or backward
};

void printGToken ( GToken &token );
void printGToken ( vector<GToken> &token );
ostream& operator<<(ostream& stream, vector<GToken> &vtoken );

inline int cmpGToken ( const GToken &t1, const GToken &t2 ) {
  return memcmp ( (void*) &t1, (void*) &t2, sizeof(GNodeID) * 2 + 
		  sizeof(GNodeData) * 2 + sizeof(GEdgeData) );
}

// to order tokens
struct GTokenLt {
  bool operator()(const GToken &t1, const GToken &t2 ) const
  {
    return cmpGToken ( t1, t2 ) < 0;
  }
};

// to order vectors of tokens
struct VGTokenLt {
  bool operator()(const vector<GToken> &t1, const vector<GToken> &t2 ) const
  {
    int c;
    for (size_t i=0; i<t1.size()&&i<t2.size();i++) {
      c = cmpGToken(t1[i], t2[i]);
      if (c < 0)
	return true;
      if (c > 0)
	return false;
    }
    return t1.size() <  t2.size();
  }
};

class GGraphCode{
public:
    GGraphCode();
    ~GGraphCode();
    
    vector<GToken> tokens; // the code
};

#endif
