function [  ] = gspan( set, filename)
%% Function that write graph file for gspan
%
%Syntaxe
% [ ] = hist( set , filename)
%
%Parameters
%
%Description
%
% =========================================================================




width = 5 ;
height = 5;

for i=1:width*height
    switch mod(i,width)
        case 1
            if i== 1
                fprintf(fileID,'e %d %d %d \n',i,i+1,1);
                fprintf(fileID,'e %d %d %d \n',i,i+width,0);
            elseif i== (width*height-width+1)
                fprintf(fileID,'e %d %d %d \n',i,i-width,0);
                fprintf(fileID,'e %d %d %d \n',i,i+1,1);
            else
                fprintf(fileID,'e %d %d %d \n',i,i-width,0);
                fprintf(fileID,'e %d %d %d \n',i,i+1,1);
                fprintf(fileID,'e %d %d %d \n',i,i+width,0);
            end
        case 0
            if i == width
                fprintf(fileID,'e %d %d %d \n',i,i-1,1);
                fprintf(fileID,'e %d %d %d \n',i,i+width,0);
            elseif i==width*height
                fprintf(fileID,'e %d %d %d \n',i,i-width,0);
                fprintf(fileID,'e %d %d %d \n',i,i-1,1);
            else
                fprintf(fileID,'e %d %d %d \n',i,i-width,0);
                fprintf(fileID,'e %d %d %d \n',i,i-1,1);
                fprintf(fileID,'e %d %d %d \n',i,i+width,0);
            end
        otherwise
            if i < width
                fprintf(fileID,'e %d %d %d \n',i,i-1,1);
                fprintf(fileID,'e %d %d %d \n',i,i+1,1);
                fprintf(fileID,'e %d %d %d \n',i,i+width,0);
            elseif i > width*height-width
                fprintf(fileID,'e %d %d %d \n',i,i-1,1);
                fprintf(fileID,'e %d %d %d \n',i,i+1,1);
                fprintf(fileID,'e %d %d %d \n',i,i-width,0);
            else
                fprintf(fileID,'e %d %d %d \n',i,i-width,0);
                fprintf(fileID,'e %d %d %d \n',i,i-1,1);
                fprintf(fileID,'e %d %d %d \n',i,i+1,1);
                fprintf(fileID,'e %d %d %d \n',i,i+width,0);
            end
    end
end

end

