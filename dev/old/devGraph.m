%% Running multiple vocab size
% Step = 1px, binSize = 3px, quantize = knn, svm = default config
tmp.clust = 'knn' ; % Use knnsearch
tmp.binSize = 3;
tmp.step = 1;
tmp.svmT = '0' ; % Linear SVM
tmp.svmS = '0' ; % C-SVC
tmp.svmC = '1' ;
tmp.svmG = '4' ;
tmp.svmP = '0.01' ;

% Running For Simplicity B3 S1 W5000
tmp.nbwords = 500 ;
SimplB3S1W500 = SimplicityGridTiny(tmp.nbwords,tmp.binSize,tmp.step,tmp.clust,...
                            tmp.svmT,tmp.svmS,tmp.svmC,tmp.svmG,tmp.svmP);
save('SimplB3S1W500.mat','SimplB3S1W500');

width = 5 ;
height = 5;
fID = fopen('Simpl-GridB3S1W500-Train-gSpan.gsp','a');
dirimg = SimplB3S1W500.conf.basedirimg;
limg = SimplB3S1W500.conf.train.img;


for i=1:SimplB3S1W500.conf.train.nbImg/4
    c1 = clock;
    % Get Image dimension
    imc=imread(sprintf('%s%d.jpg', dirimg, limg(i,1)));
    w = size(imc,1)-9;
    h = size(imc,2)-9;
    % Write graph Id
    fprintf(fID,'t # %d\n',i);
    for j=1:1:w*h
        fprintf(fID,'v %d %d \n',j,SimplB3S1W500.train.hknnclust{i}(j));                   
    end
    etime(clock,c1)
    c2 = clock ;
    for j=1:1:w*h
        switch mod(j,w)
            case 1
                if j== 1
                    fprintf(fID,'e %d %d %d \n',j,j+1,1);
                    fprintf(fID,'e %d %d %d \n',j,j+w,0);
                elseif i== (w*h-w+1)
                    fprintf(fID,'e %d %d %d \n',j,j-w,0);
                    fprintf(fID,'e %d %d %d \n',j,j+1,1);
                else
                    fprintf(fID,'e %d %d %d \n',j,j-w,0);
                    fprintf(fID,'e %d %d %d \n',j,j+1,1);
                    fprintf(fID,'e %d %d %d \n',j,j+w,0);
                end
            case 0
                if i == w
                    fprintf(fID,'e %d %d %d \n',j,j-1,1);
                    fprintf(fID,'e %d %d %d \n',j,j+w,0);
                elseif i==w*h
                    fprintf(fID,'e %d %d %d \n',j,j-w,0);
                    fprintf(fID,'e %d %d %d \n',j,j-1,1);
                else
                    fprintf(fID,'e %d %d %d \n',j,j-w,0);
                    fprintf(fID,'e %d %d %d \n',j,j-1,1);
                    fprintf(fID,'e %d %d %d \n',j,j+w,0);
                end
            otherwise
                if i < w
                    fprintf(fID,'e %d %d %d \n',j,j-1,1);
                    fprintf(fID,'e %d %d %d \n',j,j+1,1);
                    fprintf(fID,'e %d %d %d \n',j,j+w,0);
                elseif i > w*h-w
                    fprintf(fID,'e %d %d %d \n',j,j-1,1);
                    fprintf(fID,'e %d %d %d \n',j,j+1,1);
                    fprintf(fID,'e %d %d %d \n',j,j-w,0);
                else
                    fprintf(fID,'e %d %d %d \n',j,j-w,0);
                    fprintf(fID,'e %d %d %d \n',j,j-1,1);
                    fprintf(fID,'e %d %d %d \n',j,j+1,1);
                    fprintf(fID,'e %d %d %d \n',j,j+w,0);
                end
        end
    end
    etime(clock,c2)
    etime(clock,c1)
end