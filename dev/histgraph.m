function [ hgraph ] = histgraph(data, occ)
%% Function that allow to have histogram for graph occurence from plagram 
% for each graph for writing weka file.
% 
% USAGE
% [ hgraph ] = histograph(data,m)
% 
% PARAMETER 
%    Data : Data struct previously build from Simplicity function for example wich
% have all parameter and data
%    M : Matrix of occurence output from plagram. Need to be created with grep and 
% sed linux function

%% Algorithm
% Initialization

hgraph = zeros(max(occ(:,1)),data.conf.train.nbImg+data.conf.train.nbImg)-1;
 
c=0 ;
half = zeros(max(occ(:,1)),1);

for i=1:size(occ,1)
    if i == 1 || occ(i,2) == occ(i-1,2)
        c = c+1;
    else
        if half(occ(i-1,1))==true
            hgraph(occ(i-1,1),occ(i-1,2)+500)=c;
        else
            hgraph(occ(i-1,1),occ(i-1,2))=c;
        end
        if  half(occ(i-1,1))==false && occ(i-1,2)>occ(i,2)
            half(occ(i-1,1))=true;
        end
        c=1;
    end 
end
if half(occ(i,1))==true
    hgraph(occ(i,1),occ(i,2)+500)=c;
else 
    hgraph(occ(i,1),occ(i,2))=c;
end 
hgraph(hgraph==-1)=0;
