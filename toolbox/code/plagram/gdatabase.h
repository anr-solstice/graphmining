/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *   Copyright (C) 2010,2011 by Adriana Prado and Baptiste Jeudy           *
 *   baptiste.jeudy at univ-st-etienne fr                                  *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef GDATABASE_H
#define GDATABASE_H


#include <vector>
#include <map>
#include "global.h"
#include "ggraph.h"
#include "ggraphcode.h"
#include "../data/data.h" 

struct GTidEid {  // This describe each occurrence of a pattern graph
  GTid tid;       // database graph id of the occurrence 
  GNodeID nodeid; // nodeid of "from" node of the first edge of the occurrence
  GEdgeID edgeid; // and edgeid of the first edge
};

struct GTokenDatabaseData {
  vector<GTidEid> tids;  // list of all occurences of an edge
  GFrequency frequency;  /* frequency of edge (less than the size of tids
   because there can be several occurrence of the edge in one database
   graph.*/
};

class GDatabase{
  void process ( GGraph *graph ); /*called by read() to compute tokendata*/
public:
    GDatabase();
    ~GDatabase();
    /* read the database in a file and store it, it also call process() */
    void read ( string filename ); 
    
    vector<GGraph*> graphs; // vector of the graphs of the database

    map<GToken,GTokenDatabaseData,GTokenLt> tokendata; 
    /* list of all canonical edge codes occuring in the database with their
       occurrence lists and frequencies.*/
};

#endif
