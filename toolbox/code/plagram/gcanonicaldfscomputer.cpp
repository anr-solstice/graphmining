/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *   Copyright (C) 2010,2011 by Adriana Prado and Baptiste Jeudy           *
 *   baptiste.jeudy at univ-st-etienne fr                                  *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "gcanonicaldfscomputer.h"
#include <algorithm>

GCanonicalDFSComputer::GCanonicalDFSComputer( GPattern *pat ):
  pattern ( pat ),
  code_index ( pat->graph.nodes.size() ),
  graph_index ( pat->graph.nodes.size() ),
  bannedNodes ( pat->graph.nodes.size() ),
  lastEdgeUsed ( pat->graph.nodes.size() ),
  nextactive ( pat->graph.nodes.size() ),
  isactive ( pat->graph.nodes.size() )
{
}


GCanonicalDFSComputer::~GCanonicalDFSComputer()
{
}


bool GCanonicalDFSComputer::isCanonical () 
{
  GNodeID j;
  int nodes_nb = pattern->graph.nodes.size ();
  
  //first iteration
  if (pattern->tokens.size() == 1)
    return true; 

  for (GNodeID i = 0; i < nodes_nb; i++ ) { 
    //build the code starting from node i
    vector<GGraphEdge> &edges = pattern->graph.nodes[i].edges;

    for (GEdgeID eid = 0; eid < edges.size ();  eid++ ) { 
      // build the code starting with edge eid = (i,j)
      j = edges[eid].dest_nid;
      for (int l = 0; l < nodes_nb; l++)
	code_index[l] = GNONODEID;
			
      // node 0 in code maps with node i in graph
      // and node 1 with node j
      graph_index[0] = i;
      code_index[i] = 0;
      graph_index[1] = j;
      code_index[j] = 1;

      GEdgeID nextedgeid; 
      GGraphEdge *nextEdge = pattern->graph.nextEdge(j, edges[eid].eq_eid,
						     &nextedgeid);
      if (nextEdge!= NULL) {
	lastEdgeUsed[i] = eid;

	GToken t;
	vector<GToken> p; // contains the last added extension

	t.from = 0;
	t.to = 1;
	t.fromdata = pattern->graph.nodes[i].data;
	t.data = edges[eid].data;
	t.todata = pattern->graph.nodes[j].data;
	t.direction = forward;
				
	p.push_back(t);

	activenodesend = 2; /* Initialized before other *active* fields
			     because it is needed in buildExtension*/ 
	buildExtension(t.to, nextEdge, nextedgeid, p);
	
	// initialize active nodes list;
	setupActiveNodes(p.size()); 
	updateBannedNodes();

	// tokens in old code (ie pattern->tokens) up to index are the same
	// as in the code being build
	uint index = 0;
	int result = 0;

	while ((result = compareFace(p, &index)) == 0 && 
	       index < pattern->tokens.size())
	  {
	    p.resize(0);
	    findPath(p); // find the new extension
	    updateActiveNodes(p.front().from, p.back().to, p.size()); 
	    //at this point numVertices == activenodesend
	    updateBannedNodes();
	  }
	/* if result is -1, the new code is bigger than pattern->tokens,
	   thus pattern->tokens cannot be canonical.
	   
	   if result is 1 or 0, the new code is not bigger than
	   pattern->tokens, and we must continue.
	 */
	if (result == -1)
	  return false;
      } 
    } // end loop on edges
  } //end loop on nodes
  return true;
}


void GCanonicalDFSComputer::findPath(vector<GToken> &p)
{
  GNodeID k; 
  bool valid=false;
  GGraphEdge *nextEdge;
  GEdgeID nextedgeid;

  k = activenodesend;
  do{  /* look for an extension starting with the largest node code-index
	  because it will have the largest code */
    k--;
    if (isactive[k] && bannedNodes[k]) {
      /* if k is banned, it means that either an extension starts from k or
	 an extension cannot starts from k because it is in outer face */
      nextEdge = pattern->graph.nextEdge(graph_index[k], 
					 lastEdgeUsed[graph_index[k]],
					 &nextedgeid);
      if (nextEdge != NULL) {
	GNodeID to = findPathDestination(nextEdge);
	valid = validateExtension(k, to);
      }
    }
  } while (!valid);
  // at this point, k is the index in active nodes of the first node of
  // next extension

  // build the extension
  buildExtension(k, nextEdge, nextedgeid, p);
}


void GCanonicalDFSComputer::buildExtension(GNodeID from,
					  GGraphEdge *edge, GEdgeID edgeid, 
					  vector<GToken> &p)
{ /* Add new extension in code p. The extension starts with
     code node id "from" and with graph edge "edge". 
   */
  GGraphEdge *nextEdge = edge;
  GEdgeID nextedgeid = edgeid;
	
  GNodeID  oldto = from;
  GNodeData oldtodata = pattern->graph.nodes[graph_index[from]].data;

  GNodeID  to_code_id = activenodesend;
  GToken t;
  do{
    from = to_code_id; // test
    /***** p.push_back(nextedge)  ************/
    if (code_index[nextEdge->dest_nid] == GNONODEID )
      { // forward edge
	graph_index[to_code_id] = nextEdge->dest_nid;
	code_index[nextEdge->dest_nid] = to_code_id;
	t.direction = forward;
      }
    else // backward edge
      t.direction = backward;
    
    t.from = oldto; 
    t.to = code_index[nextEdge->dest_nid];
    t.fromdata = oldtodata; 
    t.todata = pattern->graph.nodes[nextEdge->dest_nid].data; 
    t.data = nextEdge->data;
    
    p.push_back(t);
    
    lastEdgeUsed[graph_index[p.back().from]] = nextedgeid;

    /***************  nextedge = nextEdge(nextedge)  ****************/
    if (p.back().direction == forward) {
      nextEdge = pattern->graph.nextEdge(graph_index[p.back().to], 
					 nextEdge->eq_eid, &nextedgeid);
      oldto = p.back().to;
      oldtodata = p.back().todata;
      to_code_id++;
    }
  }while (p.back().direction != backward); 
}


GNodeID GCanonicalDFSComputer::findPathDestination(GGraphEdge *edge)
{ GGraphEdge *nextEdge = edge;
  GEdgeID u; // not used
	
  while ( code_index[nextEdge->dest_nid] == GNONODEID )
    nextEdge = pattern->graph.nextEdge(nextEdge->dest_nid, nextEdge->eq_eid, &u);
  return code_index[nextEdge->dest_nid];
}


int GCanonicalDFSComputer::compareFace(vector<GToken> &p, uint *index)
{
  for (uint i = 0; i < p.size(); i++)
    {
      int cmp = cmpGToken(p[i], pattern->tokens[*index]);
      if (cmp < 0)
	return 1;
      else if (cmp > 0)
	return -1;
      (*index)++;
    }
  return 0;
}


void GCanonicalDFSComputer::updateActiveNodes(GNodeID start, GNodeID stop,
					      uint edge_nb)
{
  GNodeID lastRemovedNext;
  unsigned int numRemoved = 0;

  //remove non-active nodes (those not in the outer face)
  GNodeID i = nextactive[start]; 
  while (i != stop) {
    isactive[i] = false; 
    lastRemovedNext = nextactive[i];
    numRemoved++;
    i = nextactive[i];
  }
  if (numRemoved>0)
    nextactive[start] = lastRemovedNext;
  
  //insert new active nodes
  if (edge_nb > 1) { // this test could be removed
    i = start;
    GNodeID startNext = nextactive[start];
    
    for (uint j = 0; j < edge_nb - 1; j++) {
      nextactive[i] = activenodesend;
      isactive[activenodesend] = true;
      i = activenodesend; 
      activenodesend++; 
    }
    nextactive[i] = startNext;
  }
  activenodesbegin = start;
}


void GCanonicalDFSComputer::setupActiveNodes (int node_nb) 
{
  int i;
  activenodesbegin = 0;
  for (i = 0; i < node_nb - 1; i++) {
    nextactive[i] = i+1;
    isactive[i] = true;
  }
  nextactive[i] = 0;
  isactive[i] = true;
  activenodesend = node_nb;
} 


void GCanonicalDFSComputer::updateBannedNodes () 
{ /* Update bannedNodes for all nodes in active nodes list.
     A node i is banned if:
     - a new extension can start from i
     - a new extension cannot start from i because i is in outerface
  */
  GEdgeID nextedgeid;
  GGraphEdge *nextEdge;
  GNodeID i = activenodesbegin;
  GNodeID j = nextactive[i];
  
  do {
    nextEdge = pattern->graph.nextEdge(graph_index[j], lastEdgeUsed[graph_index[j]], &nextedgeid);
    if (nextEdge == NULL)
      bannedNodes[j] = true;
    else if (nextEdge->dest_nid == graph_index[i] &&  
	     nextEdge->eq_eid == lastEdgeUsed[graph_index[i]])
      bannedNodes[j] = false;
    else
      bannedNodes[j] = true;
    i = j;
    j = nextactive[j];
  } while (i != activenodesbegin);	
}


bool GCanonicalDFSComputer::validateExtension(GNodeID start, GNodeID stop)
{
  GNodeID i = nextactive[start];
  while (i != stop) {
    if (bannedNodes[i]) return false;
    i = nextactive[i];
  }
  return true;
}
