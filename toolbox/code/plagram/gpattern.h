/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *   Copyright (C) 2010,2011 by Adriana Prado and Baptiste Jeudy           *
 *   baptiste.jeudy at univ-st-etienne fr                                  *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef GPATTERN_H
#define GPATTERN_H

#include "ggraphcode.h"
#include "ggraph.h"

/* The GPattern stores the code and graph of the current pattern P.  It
   also store the list of "active" nodes, ie the nodes that lie on the outer
   face of P (the node from which a new extension can be added).

   The active nodes are stored in a vector seen as a simply linked list.
   Some nodes are still in the vector and not in the list (to be able
   to restore the list later when the algo backtracks).
*/

struct GActive
{
  GNodeID index; // node id of active node
  GNodeID next;  // index of the next active node in activenodes vector
};

struct GUndo {// used to restore the previous state of the activenodes list
  unsigned int numRemoved;
  GNodeID start;
  GNodeID startNext;
};


class GPattern:public GGraphCode{
 public:

  /* the graph and code, maintained by push_back and pop_back */
  GGraph graph;
  // vector<GToken> tokens; herited from GGraphCode

  vector<GNodeID> nextactive;// active node list, simply linked list.
  vector<bool> isactive; // to know if a node is in active node list

  /* activenodesbegin : index of a node in the activenodes list (it can
     be any node of the list). necessary because all nodes in the vector
     are not actually part of the list.*/
  // activenodesend : index where to store new nodes in activenodes
  int activenodesbegin, activenodesend; 

  vector<GUndo> undo; // undo info used for pop_back

  GPattern();
  ~GPattern();
  bool quickPrune ( const GToken &token );
  // true means adding token to the code would yield a non-canonical code
  // false means we don't know if the code would be canonical

  void push_back ( const vector<GToken> &vtoken );
  // Add an extension (path) to the pattern: update graph, code and undo info
  void pop_back (const vector<GToken> &vtoken); 
  // remove an extension from the pattern. Undo what was done in push_back
  bool isCanonical ();
  // test if the code is canonical
  void updateNode (GNodeID nodeid, bool flag);
  // change the flag (which indicate if the node is in outerface)
  void printActiveNodes();
  // print the activenodes list. Can be used to debug

 private:
  void setup ( const GToken &token ); 
  // called by push_back for the first token

};

#endif
