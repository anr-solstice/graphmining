#!/bin/bash

# Sortie imm�diate lorsqu'une commande simple se termine avec un code non nul. 
set -e

# Consid�rer  les  variables inexistantes comme des erreurs durant l'expansion 
# des param�tres. Si on tente de d�velopper une variable inexistante, le shell 
# affiche un message d'erreur, et, s'il n'est pas interactif, se termine avec 
# un code de retour non-nul.
set -u

# Apr�s l'expansion de chaque commande simple, bash affiche la valeur de PS4, 
# suivie par la commande et ses arguments d�velopp�s.
# set -x
# egalement option -D (debug)

#############################################################################
## Variables
#############################################################################
# le fichier awk doit etre dans le meme repertoire que $0
awk_code=$(dirname $0)/split_graphs.awk
rm_temp=n # remove temp files ?

#############################################################################
# Fonctions
#############################################################################

function usage() {
cat <<EOF > /dev/stderr
usage : $0 [-Dh] [-r] tempdir infile outfile

Triangulate the nodes in infile and put results in outfile.
- infile and outfile are in graph format.
- tempdir is a temporary directory used by the program to store files. 
- There should be no file with extension .node in tempdir.
See documentation.
Options :
r : remove all the created temporary files in tempdir
h : help
D : Debug (set -x)
EOF
}

function mytriangulation() {
    # Read on stdin a list of files .node containing the nodes of each graph.
    # Call triangle on each of them and generate the graph output file.
    # Uses $rm_temp and $out_graph, uses temporary files.
    while read f; do
    # triangulation
        triangle "$f" #> /dev/null
        file=$(dirname "$f")"/"$(basename "$f" ".node")
        frameid=$(basename "$f" | cut -d"_" -f1)
        triangle_to_graph "$file" $frameid
        cat "${file}_graph.txt" >> "$out_graph"
        if [[ rm_temp == y ]]; then
            rm -f "$file"{.node,.1.node,.1.ele,_graph.txt} 
        fi
    done
}

##############################################################################
# debut
##############################################################################


# param�tres
while getopts Dhr opt; do  # abhm:n:
    case "$opt" in
 #       a) echo option a;; 
 #       b) echo option b;; 
        D) set -x ;;
        h) usage; exit ;;
        r) rm_temp=y;; 
 #       n) echo option n $OPTARG;; 
        ?) usage; exit 1;;
    esac
done

shift $(($OPTIND - 1))

# $# contient le nb d'arguments restants

if (( $# != 3 )); then 
    echo "Incorrect parameter number" > /dev/stderr
    usage; exit 1;
fi

out_graph="$3"
tempdir="$1"
mkdir -p "$tempdir"

if \ls "$tempdir"/*.node > /dev/null 2>&1; then
    echo 'Error: There are *.node files in ' "$tempdir" > /dev/stderr
    echo 'Delete them (?) and run again' > /dev/stderr
    exit 2
fi

if [[ ! -r "$awk_code" ]]; then
    echo "Error: cannot read file: " "$awk_code" > /dev/stderr
    exit 4
fi

echo "# Triangulation : $0 $* ($(date))" > "$out_graph"

awk -v "out_dir=$tempdir" -v "comment_file=$out_graph" -f "$awk_code" "$2" | mytriangulation


