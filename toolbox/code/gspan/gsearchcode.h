/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef GSEARCHCODE_H
#define GSEARCHCODE_H
#include "ggraphcode.h"
#include "ggraph.h"

// The GSearchCode is a more restricted type of code.
// In this code it is assumed that if a token introduces a new node, the number
// of the new node is the smallest number that has not been used in the code yet.
// This code must store a set of "activenodes" which may be used by the 
// the search procedure to determine for which nodes extensions are searched.
// (0,1,..,..,..)(1,2,..,..,..)(2,0,..,..,..) is a valid code.
// (1,2,..,..,..) is not a valid code
/**
@author Siegfried Nijssen
*/
class GSearchCode:public GGraphCode {
public:
    GSearchCode();
    ~GSearchCode();
    
    vector<GNodeID> activenodes; 
    int activenodesbegin, activenodesend; // the code may choose to use parts of the array for other purposes
    
    virtual bool isCanonical () = 0;
    virtual bool quickPruneBackward ( const GToken &token ) = 0;
    virtual bool quickPruneForward ( const GToken &token ) = 0;
    
    GGraph graph;
      // the graph to which this code is associated, is maintained by push_back and pop_back
    
    virtual void setup ( const GToken &token ); 
      // for the first token both setup and push_back have to be called
    virtual void push_back ( const GToken &token ); 
      // extends the code with the token. The token must maintain a valid code, but this is not checked
    virtual void pop_back ();
};

#endif
