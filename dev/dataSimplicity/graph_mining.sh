#!/bin/bash

# Script that will apply graph mining via gSpan and plagram

# CONST
## DIR
GRAPHDIR="data/graph/"
RESPLGDIR="data/graph/resultplg/"
RESGSPDIR="data/graph/resultgsp/"
RESGRIDDIR="grid/"
RESTRIDIR="tri/"
RESKNNDIR="knn/"


# Create dir for results
mkdir $RESPLGDIR
mkdir $RESGSPDIR
mkdir $RESGSPDIR$RESGRIDDIR
mkdir $RESGSPDIR$RESKNNDIR
mkdir $RESPLGDIR$RESGRIDDIR
mkdir $RESPLGDIR$RESKNNDIR

[ -f $GSPGRIDLOG ] && cat $GSPGRIDLOG >> ${LOGDIR}${GSPGRIDLOG}".old" && rm $GSPGRIDLOG
[ -f $GSPKNNLOG ] && cat $GSPKNNLOG >> ${LOGDIR}${GSPKNNLOG}".old" && rm $GSPKNNLOG
[ -f $PLGGRIDLOG ] && cat $PLGGRIDLOG >> ${LOGDIR}${PLGGRIDLOG}".old" && rm $PLGGRIDLOG
[ -f $PLGTRILOG ] && cat $PLGKNNLOG >> ${LOGDIR}${PLGKNNLOG}".old" && rm $PLGKNNLOG



echo "======================================================"
echo "!                      GRID MINING                   !"
echo "======================================================"
echo "===== PLAGRAM  ====="
for f in ${GRAPHDIR}*B*S*Full.plg ; do
    echo "====="
    echo "FILE : $f"
    tmp="$RESPLGDIR$RESGRIDDIR${f#${GRAPHDIR}}";
    for i in 1000 750 500 400 300 250 200 150 125 100 ; do 
    	tmp="${tmp%${PLG}}"
    	echo "FREQ : $i"
        ./plagram -cqo -f $i $f $tmp-$i$PLG >> ${LOGDIR}$PLGGRIDLOG
    	echo "================================================== " >> ${LOGDIR}$PLGGRIDLOG
    done
done
echo "===== GSPAN ====== EXTENDED GRAPH"
for f in ${GRAPHDIR}*B*S*Full.plg ; do 
    echo "====="
    echo "FILE : $f"
    tmp="$RESGSPDIR$RESGRIDDIR${f#${GRAPHDIR}}"
    for i in 1000 750 500 400 300 250 200 150 125 100 ; do
    	tmp="${tmp%${PLG}}"
    	echo "FREQ : $i"
        ./gSpan2005 $i $f $tmp-$i$PLG 60 30 >> ${LOGDIR}$GSPGRIDLOG
    	echo "================================================== " >> ${LOGDIR}$GSPGRIDLOG
    done
done
echo "===== GSPAN ===== NORMAL GRAPH"
for f in ${GRAPHDIR}*B*S*Full.gsp ; do 
    echo "====="
    echo "FILE : $f"
    tmp="$RESGSPDIR$RESGRIDDIR${f#${GRAPHDIR}}"
    for i in 1000 750 500 400 300 250 200 150 125 100 75 50 ; do 
        tmp="${tmp%${GSP}}"
        echo "FREQ : $i"
        ./gSpan2005 $i $f $tmp-$i$GSP 60 30 >> ${LOGDIR}$GSPGRIDLOG
        echo "================================================= " >> ${LOGDIR}$GSPGRIDLOG
    done
done

echo "======================================================"
echo "!                      TRI MINING                    !"
echo "======================================================"
echo "===== PLAGRAM ====="
for f in ${GRAPHDIR}*K*D*Full.plg ; do 
    echo "====="
    echo "FILE : $f"
    tmp="$RESPLGDIR$RESTRIDIR${f#${GRAPHDIR}}"
    for i in 500 400 300 250 200 150 125 100 ; do 
        tmp="${tmp%${PLG}}"
        echo "FREQ : $i"
        ./plagram -cqo -f $i $f $tmp-$i$PLG >> ${LOGDIR}$PLGTRILOG
        echo "=============================================== " >> ${LOGDIR}$PLGTRILOG
    done
done

echo "======================================================"
echo "!                     KNN MINING                     !"
echo "======================================================"
echo "===== GSPAN ====="
for f in ${GRAPHDIR}*K*D*Full.gsp ; do
    echo "====="
    echo "FILE : $f"
    tmp="$RESGSPDIR$RESKNNDIR${f#${GRAPHDIR}}"
    for i in 500 400 300 250 200 150 125 100 ; do 
        tmp="${tmp%${GSP}}"
        echo "FREQ : $i"
        ./gSpan2005 $i $f $tmp-$i$GSP 60 60 >> ${LOGDIR}$GSPKNNLOG
        echo "=============================================== " >> ${LOGDIR}$GSPKNNLOG
    done
done

