function [ Out ] = wgraphtest( nbgraph, w , h , filename)
%% Function creating file for weka
%
%Syntaxe
% [ ] = weka (hist, nbwords, filename)
%
%Parameters
% All parameter are required
% - Hist (required)
%       -> Histograme construct from [h,c]=hist(desc, model).
% - Class (required)
%       -> List of all class
% - imgclass (required)
%       -> matrix of size nbimg x 2 such that :
%       -> Dimension 1 : imgID
%       -> Dimension 2 : classID
% - filename
%       -> Name of the file where data will be written.
%
%Description
% Return an error message if the openning file failed.

% =========================================================================end
%% Initialise train file for weka :
% Trace
conf.method = 'grid' ;

nblabel = 5;

% Generate graph data
knnclust = {} ;
for i=1:nbgraph
Graph{i} = randi(nblabel,w,h);
tmp = Graph{i}';
HistGraph{i}= cat(1,tmp(:));

end
AllGraph = cat(3,Graph{:});
Hist = cat(2,HistGraph{:});

% Openning File
[fileID.gsp,~] = fopen(sprintf('%s.gsp',filename), 'w');
[fileID.plg,~] = fopen(sprintf('%s.plg',filename), 'w');
for i=1:nbgraph
c1 = clock;
% Get Image dimension
% Standard grid representation
fprintf(fileID.gsp,'t %d\n',i);
fprintf(fileID.gsp,'n %d\n',(w-1)*h+(h-1)*w);
% Grid adaptation for plagram
fprintf(fileID.plg,'t %d\n',i);
fprintf(fileID.plg,'n %d\n',((w*2-1)*(h*2)+(h*2-1)*(w*2))*2);
for j=1:1:size(HistGraph{i},1)
% Standard grid representation
fprintf(fileID.gsp,'v %d %d \n',j-1,HistGraph{i}(j));
% Grid adaptation for plagram
fprintf(fileID.plg,'v %d %d 0 \n',(j-1)*4,HistGraph{i}(j));
fprintf(fileID.plg,'v %d %d 0 \n',(j-1)*4+1,HistGraph{i}(j));
fprintf(fileID.plg,'v %d %d 0 \n',(j-1)*4+2,HistGraph{i}(j));
fprintf(fileID.plg,'v %d %d 0 \n',(j-1)*4+3,HistGraph{i}(j));
end
etime(clock,c1);
c2 = clock ;
% Standard grid representatio
for j=1:1:size(HistGraph{i},1)
switch mod(j,w)
case 1
if j == 1
tledge(fileID.gsp, j-1 , w , { '2' , '1' } ) ;
elseif j == (w*h-w+1)
bledge(fileID.gsp, j-1 , w , { '2' , '1' } ) ;
else
ledge(fileID.gsp, j-1 , w , { '2' , '1' , '2' } ) ;
end
case 0
if j == w
tredge(fileID.gsp, j-1 , w , { '1' , '2' } ) ;
elseif j==w*h
bredge(fileID.gsp, j-1 , w , { '2' , '1' } ) ;
else
redge(fileID.gsp, j-1 , w , { '2' , '1' , '2'} ) ;
end
otherwise
if j < w
tedge(fileID.gsp, j-1 , w , { '1' , '2' , '1' } ) ;
elseif j > w*h-w
bedge(fileID.gsp, j-1 , w , { '1' , '2' , '1' } ) ;
else
edge(fileID.gsp, j-1 , w , { '1' , '2'  , '1' , '2'} ) ;
end
end
end
ide = 1;
e = double.empty(0,2);
% Grid adaptation for plagram
for j=1:1:size(HistGraph{i},1)
% Index of topleft node
size(HistGraph{i},1)*4

k= (j-1)*4+1
switch mod(j,w)
case 1
if j == 1
tledgePLG(fileID.plg, k-1 , 2*w ) ;
elseif j == (w*h-w+1)
bledgePLG(fileID.plg, k-1 , 2*w ) ;
else
ledgePLG(fileID.plg, k-1 , 2*w ) ;
end
case 0
if j == w
tredgePLG(fileID.plg, k-1 , 2*w ) ;
elseif j==w*h
bredgePLG(fileID.plg, k-1 , 2*w ) ;
else
redgePLG(fileID.plg, k-1 , 2*w ) ;
end
otherwise
if j < w
tedgePLG(fileID.plg, k-1 , 2*w ) ;
elseif j > w*h-w
bedgePLG(fileID.plg, k-1 , 2*w ) ;
else
edgePLG(fileID.plg, k-1 , 2*w ) ;
end
end
end
etime(clock,c2);
etime(clock,c1);
end

Out.Graph = Graph;
Out.HistGraph = HistGraph;
Out.Hist = Hist;
Out.AllGraph = AllGraph;


%% Subfunction for grid file writing
% For each subfunction :
% PARAMETER
%    f = File ID in which edge will be written
%    i = idx of vertex
%    w = width of grid
%    l = matrix with label for each edge : {'0','1','2','1'}.
%         -> If not specified, all value of l = '0'.
%
% Each subfunction will write specific edge define by first letter of
% the function such that :
% t       = top edge
% b       = bottom edge
% l       = left edge
% r       = right edge
% tl/tr   = top left/right edge
% bl/br   = bottom left/right edge
% Nothing = middle edge
function [ ] = tledge(f,i,w,l)
if nargin ~= 4
l = { '0' ; '0' } ;
end
fprintf(f,'e %d %d %s \n',i,i+w,l{1});
fprintf(f,'e %d %d %s \n',i,i+1,l{2});
end
function [ ] = tedge(f,i,w,l)
if nargin ~= 4
l = { '0' ; '0' ; '0' };
end
fprintf(f,'e %d %d %s \n',i,i-1,l{1});
fprintf(f,'e %d %d %s \n',i,i+w,l{2});
fprintf(f,'e %d %d %s \n',i,i+1,l{3});
end
function [ ] = tredge(f,i,w,l)
if nargin ~= 4
l = { '0' ; '0' } ;
end
fprintf(f,'e %d %d %s \n',i,i-1,l{1});
fprintf(f,'e %d %d %s \n',i,i+w,l{2});
end
function [ ] = bledge(f,i,w,l)
if nargin ~= 4
l = { '0' ; '0' } ;
end
fprintf(f,'e %d %d %s \n',i,i+1,l{1});
fprintf(f,'e %d %d %s \n',i,i-w,l{2});
end
function [ ] = bedge(f,i,w,l)
if nargin ~= 4
l = { '0' ; '0' ; '0' };
end
fprintf(f,'e %d %d %s \n',i,i-1,l{1});
fprintf(f,'e %d %d %s \n',i,i+1,l{2});
fprintf(f,'e %d %d %s \n',i,i-w,l{3});
end
function [ ] = bredge(f,i,w,l)
if nargin ~= 4
l = { '0' ; '0' } ;
end
fprintf(f,'e %d %d %s \n',i,i-w,l{1});
fprintf(f,'e %d %d %s \n',i,i-1,l{2});
end
function [ ] = ledge(f,i,w,l)
if nargin ~= 4
l = { '0' ; '0' ; '0' } ;
end
fprintf(f,'e %d %d %s \n',i,i-w,l{1});
fprintf(f,'e %d %d %s \n',i,i+1,l{2});
fprintf(f,'e %d %d %s \n',i,i+w,l{3});
end
function [ ] = edge(f,i,w,l)
if nargin ~= 4
l = { '0' ; '0' ; '0' ; '0' };
end
fprintf(f,'e %d %d %s \n',i,i-1,l{1});
fprintf(f,'e %d %d %s \n',i,i+w,l{2});
fprintf(f,'e %d %d %s \n',i,i+1,l{3});
fprintf(f,'e %d %d %s \n',i,i-w,l{4});
end
function [ ] = redge(f,i,w,l)
if nargin ~= 4
l = { '0' ; '0' } ;
end
fprintf(f,'e %d %d %s \n',i,i-w,l{1});
fprintf(f,'e %d %d %s \n',i,i-1,l{2});
fprintf(f,'e %d %d %s \n',i,i+w,l{3});
end

%% Subfunction for grid file writing specific to plagram
function [ ] = tledgePLG(f,i,w)
ei = i;
% e1 = tledge
e = [ e ; ei ei+2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+1 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0',ide-1); ide=ide+1;
% e2 = tedge
ei = i+1 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei+2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+3 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
% e3 = ledge
ei = i+2 ;
e = [ e ; ei ei+2*w-2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+1 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
% e4 = edge
ei = i + 3 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei+2*w-2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+3 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
end
function [ ] = tedgePLG(f,i,w)
% e1 = tedge
ei = i ;
fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei + 2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+1 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
% e2 = tedge
ei = i+1 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei + 2] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+3] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
% e3 = edge
ei = i+2 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei+2*w-2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+1 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
% e4 = edge
ei = i + 3 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei+2*w-2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+3] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
end
function [ ] = tredgePLG(f,i,w)
% e1 = tedge
ei = i ;
fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei + 2] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+1 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
% e2 = tredge
ei = i+1 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei + 2] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
% e3 = edge
ei = i+2 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei+2*w-2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+1 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
% e4 = redge
ei = i + 3 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei+2*w-2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
end
function [ ] = bledgePLG(f,i,w)
% e1 = ledge
ei = i ;
e = [ e ; ei ei + 2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+1 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
% e2 = edge
ei = i+1 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei + 2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+3] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
% e3 = bledge
ei = i+2 ;
e = [ e ; ei ei+1 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
% e4 = bedge
ei = i + 3 ;
e = [ e ; ei ei+3] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
end
function [ ] = bedgePLG(f,i,w)
% e1 = edge
ei = i ;
fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei + 2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+1 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
% e2 = edge
ei = i+1 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei + 2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+3] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
% e3 = bedge
ei = i+2 ;
e = [ e ; ei ei+1 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
% e4 = bedge
ei = i + 3 ;
e = [ e ; ei ei+3] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
end
function [ ] = bredgePLG(f,i,w)
% e1 = edge
ei = i ;
fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei + 2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+1 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
% e2 = redge
ei = i+1 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei + 2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
% e3 = bedge
ei = i+2 ;
e = [ e ; ei ei+1 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
% e4 = bredge
ei = i + 3 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
end
function [ ] = ledgePLG(f,i,w)
% e1 = ledge
ei = i ;
e = [ e ; ei ei + 2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+1 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
% e2 = edge
ei = i+1 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei + 2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+3] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
% e3 = ledge
ei = i+2 ;
e = [ e ; ei ei + 2*w-2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+1 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
% e4 = edge
ei = i + 3 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei+2*w-2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+3] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
end
function [ ] = edgePLG(f,i,w)
% e1 = edge
ei = i ;
fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei + 2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+1 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
% e2 = edge
ei = i+1 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei + 2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+3] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
% e3 = edge
ei = i+2 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei+2*w-2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+1 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
% e4 = edge
ei = i + 3 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei+2*w-2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+3] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
end
function [ ] = redgePLG(f,i,w)
% e1 = edge
ei = i ;
fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei + 2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+1 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
% e2 = redge
ei = i+1 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei + 2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
% e3 = edge
ei = i+2 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei+2*w-2 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
e = [ e ; ei ei+1 ] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
% e4 = redge
ei = i + 3 ;
fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
e = [ e ; ei ei+2*w-2] ;
fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
end


end % End of function




