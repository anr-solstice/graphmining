/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef GCANONICALDFSCOMPUTER_H
#define GCANONICALDFSCOMPUTER_H
#include <vector>
#include "ggraph.h"
#include "gdfscode.h"
#include "global.h"

struct GCanonicalDFSToken {
  GToken token;
  GNodeID nodeid;
};

/**
 This is a class which wraps around all procedures and data required to compute a canonical code.
 Usually, it will be created for this computation, and then deleted again.
@author Siegfried Nijssen
*/
class GCanonicalDFSComputer{
public:
    GCanonicalDFSComputer( GGraph *graph );

    ~GCanonicalDFSComputer();
    
    vector<GNodeID> mapstonew;
    vector<GNodeID> mapstoold;
    vector<GCanonicalDFSToken> stack;
    void recurse ();
    void recurse2 ( GNodeID parentid, GNodeID nodeid );
    bool isCanonical ( GDFSCode *code );    
    
    GGraph *graph;
    GDFSCode *code;
    int nrnodes, pos;
    bool stop;
    
};

#endif
