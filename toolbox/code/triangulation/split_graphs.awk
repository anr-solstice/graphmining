function fin_graphe() {
    close(cur_file);
    print nb_vertex, 2, nb_attribute, 0 > cur_file ".node";
    close(cur_file ".node");
    system("cat " cur_file " >> " cur_file ".node"); 
    system("rm -f "cur_file);
    print cur_file".node";
    nb_vertex = 0;
}

BEGIN {
    # out_dir="tt/"; 
    # comment_file=out_dir "/cmt.txt";
    input_name="_frame";
    nb_vertex=0;
}

/^#/ { print $0 >> comment_file;}

/^t/ {
    if (nb_vertex != 0) fin_graphe();
    id = $2;
    cur_file = out_dir "/" id input_name;
}

/^v/ {
    # vertex of id=0 ignored (external face)
    if ($2 == 0) next; 
    nb_vertex++;
    if (nb_vertex == 1) 
	nb_attribute = NF - 5;
    else
	if (nb_attribute != NF - 5) {
	    print "Erreur ligne " NR " nombre d'attributs incorrect" > /dev/stderr;
	    exit 1;
	}
    if ($2 != nb_vertex) print "Erreur ligne " NR "vertex index incorrect" > /dev/stderr;
    # index, x, y, label
    printf "%d %d %d %d",$2-1, $5, $6, $3 >> cur_file;
    # other attributes
    for (i = 7; i<=NF; i++) printf " %s", $i >> cur_file;
    printf "\n" >> cur_file;
}

END {
    fin_graphe();
    fflush();
}


    
    
	
	    
