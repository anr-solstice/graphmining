/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef GDFSCODE_H
#define GDFSCODE_H
#include "gsearchcode.h"

/**
@author Siegfried Nijssen
*/
class GDFSCode:public GSearchCode {
public:
    GDFSCode();
    ~GDFSCode();
    
    // these functions also maintain the associated graph, and the activenodes arrays, if any
    void setup ( const GToken &token );
    void push_back ( const GToken &token ); 
    void pop_back (); 
    bool isCanonical ();
    bool quickPruneBackward ( const GToken &token );
    bool quickPruneForward ( const GToken &token );
private:    
    // Please note: in the GDFSCode activenodes is the rightmostpath.
    // the following data is maintained in subgraphs during the search
    vector<GNodeID> in_activenodes; // the index in the activenodes array of a node
    vector<GNodeID> parents; // what is the parent of a node according to the code
    vector<pair<GNodeID,int> > undo; // to allow pop_back to restore above arrays after a push_back
};

#endif
