function [ errmsg] = gridtex( graph , filename)
%GRAPHTEX
%   Function that generate LaTeX code for grid.
%
% PARAMETER
% Grid : Structure of t grid of size n by m.
% Filename where the graphs will be written.

% Get label, for coloring.
clr = unique(cat(1,graph{:}));
file.id = fopen(sprintf('%s.tex',filename),'w');
if size(clr) < 6
    clr = [ 255 0 0 ;
        255 255 0 ;
        255 0 255 ;
        0 255 0 ;
        0 255 255 ;
        0 0 255 ];
    for i=1:size(clr)
        fprintf(file.id,sprintf('%sdefinecolor{color%d}{RGB}{%f,%f,%f}\n',...
            '\\',i,clr(i,1),clr(i,2),clr(i,3)));
        clrname{i}=sprintf('color%d',i');
    end
else
    for i=1:size(clr)
        fprintf(file.id,sprintf('%sdefinecolor{color%d}{RGB}{%f,%f,%f}\n',...
            '\\',i,floor(rand()*255),floor(rand()*255),floor(rand()*255)));
        clrname{i}=sprintf('color%d',i');
    end
end

for t=1:size(graph,2)
    w = size(graph{t},1);
    h = size(graph{t},2);
    file.graph = fopen(sprintf('Graph%d.tex',t),'w');
    fprintf(file.id,sprintf('%sinput{Graph%d}  \n','\\',t));
    fprintf(file.graph,'\\begin{tikzpicture}\n');
    fprintf(file.graph,'\\tikzstyle{every state}=[text=black,circle,minimum size=0.25cm]\n');
    for i=1:size(graph{t},1)
        for j=1:size(graph{t},2)
            % First Node
            if i==1 && j==1
                fprintf(file.graph,...
                    sprintf('%snode[state, fill=%s] (%d) {%d}; \n',...
                    '\\',clrname{graph{t}(1)},1,graph{t}(1)));
                % Top Node
            elseif i==1 && j~=1
                fprintf(file.graph,...
                    sprintf('%snode[state, fill=%s] (%d) [right of=%d]  {%d}; \n',...
                    '\\',clrname{graph{t}(1,j)},j,j-1,graph{t}(1,j)));
                % Other Nodes
            elseif i~=1
                fprintf(file.graph,...
                    sprintf('%snode[state, fill=%s] (%d) [below of=%d]  {%d}; \n',...
                    '\\',clrname{graph{t}(i,j)},(i-1)*size(graph{t},1)+j,...
                    (i-2)*size(graph{t},1)+j,graph{t}(i,j)));
            end
        end
    end
    fprintf(file.graph,'\\path ');
    for j=1:size(graph{t},1)*size(graph{t},1)
        switch mod(j,w)
            case 1
                if j == 1
                    tledge(file.graph, j , w ) ;
                elseif j == (w*h-w+1)
                    bledge(file.graph, j , w ) ;
                else
                    ledge(file.graph, j, w ) ;
                end
            case 0
                if j == w
                    tredge(file.graph, j , w ) ;
                elseif j==w*h
                    bredge(file.graph, j , w ) ;
                else
                    redge(file.graph, j , w ) ;
                end
            otherwise
                if j < w
                    tedge(file.graph, j , w ) ;
                elseif j > w*h-w
                    bedge(file.graph, j , w ) ;
                else
                    edge(file.graph, j , w ) ;
                end
        end
        
        %
        %   \path (A) edge              node {0,1,L} (B)
        %             edge              node {1,1,R} (C)
        %         (B) edge [loop above] node {1,1,L} (B)
        %             edge              node {0,1,L} (C)
        %         (C) edge              node {0,1,L} (D)
        %             edge [bend left]  node {1,0,R} (E)
        %         (D) edge [loop below] node {1,1,R} (D)
        %             edge              node {0,1,R} (A)
        %         (E) edge [bend left]  node {1,0,R} (A);
        % \end{tikzpicture}
        
        
    end
    fprintf(file.graph,'\\end{tikzpicture}\n');
end

    function [ ] = tledge(f,i,w)
        fprintf(f,'(%d) edge (%d) \n',i,(i+w));
        fprintf(f,'(%d) edge (%d) \n',i,(i+1));
    end
    function [ ] = tedge(f,i,w)
        fprintf(f,'(%d) edge (%d) \n',i,(i-1));
        fprintf(f,'(%d) edge (%d) \n',i,(i+w));
        fprintf(f,'(%d) edge (%d) \n',i,(i+1));
    end
    function [ ] = tredge(f,i,w)
        fprintf(f,'(%d) edge (%d) \n',i,(i-1));
        fprintf(f,'(%d) edge (%d) \n',i,(i+w));
    end
    function [ ] = bledge(f,i,w)
        fprintf(f,'(%d) edge (%d) \n',i,(i+1));
        fprintf(f,'(%d) edge (%d) \n',i,(i-w));
    end
    function [ ] = bedge(f,i,w)
        fprintf(f,'(%d) edge (%d) \n',i,(i-1));
        fprintf(f,'(%d) edge (%d) \n',i,(i+1));
        fprintf(f,'(%d) edge (%d) \n',i,(i-w));
    end
    function [ ] = bredge(f,i,w)
        fprintf(f,'(%d) edge (%d) \n',i,(i-w));
        fprintf(f,'(%d) edge (%d); \n',i,(i-1));
    end
    function [ ] = ledge(f,i,w)
        fprintf(f,'(%d) edge (%d) \n',i,(i-w));
        fprintf(f,'(%d) edge (%d) \n',i,(i+1));
        fprintf(f,'(%d) edge (%d) \n',i,(i+w));
    end
    function [ ] = edge(f,i,w)
        fprintf(f,'(%d) edge (%d) \n',i,(i-1));
        fprintf(f,'(%d) edge (%d) \n',i,(i+w));
        fprintf(f,'(%d) edge (%d) \n',i,(i+1));
        fprintf(f,'(%d) edge (%d) \n',i,(i-w));
    end
    function [ ] = redge(f,i,w)
        fprintf(f,'(%d) edge (%d) \n',i,(i-w));
        fprintf(f,'(%d) edge (%d) \n',i,(i-1));
        fprintf(f,'(%d) edge (%d) \n',i,(i+w));
    end
end