/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <cstdlib>
#include <fstream>
#include "ggraph.h"
#include "gdatabase.h"
#include "gdfscode.h"
#include "gextensioncollect.h"
#include "gsubgraphiso.h"
#include <fstream>
#include <cstdio>

using namespace std;

//ofstream outfile ( "output" );
ofstream outfile;

clock_t timeTmp;
double totTmp;

clock_t timeIso;
double totIso;

clock_t timeCanonical;
double totCanonical;

double totMatching;
double totExt;
double totMap;

long long int count;
long long int numMatchings;
long long int szMatchings;
long long int totalSz; //patterns

int nb_intervals;
int interval_size;
int nb_int_before_kill;

void printStatistics(int counter)
{
  double tmpMatching, tmpMap, tmpExt, tmpIso, tmpCanonical;
  printf("#!  %11d\t%13lld\t%12lld\t%14lld\t%13lld\t",counter, totalSz, numMatchings, szMatchings, count);
 
  tmpMatching = totMatching/(double)CLOCKS_PER_SEC;
  tmpMap= totMap/(double)CLOCKS_PER_SEC;
  tmpExt= totExt/(double)CLOCKS_PER_SEC;
  tmpIso = totIso/(double)CLOCKS_PER_SEC;
  tmpCanonical = totCanonical/(double)CLOCKS_PER_SEC;
  printf("%8f\t%14f\t%13f\t%14f\n",totTmp + (double) interval_size*nb_intervals, tmpExt, tmpMatching - tmpExt, tmpCanonical);
  fflush(stdout);
  //  cout << "Iso time: " << "[" << tmpIso << "]" << endl;
  //  cout << "Extension time (including map): " << "[" << tmpExt << "]" << endl;
  //  cout << "Map time: " << "[" << tmpMap << "]" << endl;
}


void search ( GDatabase &database, GFrequency minfreq, GSearchCode *searchcode, GFrequency frequency, const GToken &token, vector<GTid> &tids, int &counter ) {
  searchcode->push_back ( token );

  //if ( frequency >= minfreq && searchcode->isCanonical () ) {  

  if ( frequency >= minfreq)
  {
    timeCanonical = clock();
    bool ok = searchcode->isCanonical();
    timeCanonical = clock() - timeCanonical;
    totCanonical += timeCanonical;
 
    if (ok)
    {
    counter++;
    totalSz += searchcode->tokens.size();

    //if ( counter % 1000 == 0 )
    //  cout << counter << endl;

    outfile << "t # " << counter << " " << frequency << endl;
    outfile << searchcode->graph << flush;

    GExtensionCollect collect;
    GSubGraphIso subgraphiso ( &collect, &searchcode->graph );
    vector<GTid> tids2;
    
    timeIso = clock();
    for ( int i = 0, size = tids.size (); i < size; ++i )
      if ( subgraphiso.run ( tids[i] ) )
        tids2.push_back ( tids[i] );

    timeIso = clock() - timeIso;
    totIso += timeIso;
    totMatching += subgraphiso.totMatching;
    numMatchings += subgraphiso.numMatchings;
    szMatchings += subgraphiso.szMatchings;
	//cout << numMatchings << "\t" << szMatchings << endl;

    totExt += collect.totExt;	
    totMap += collect.totMap;
    count += collect.count;

    
    for ( map<GToken,GExtensionData,GTokenLt>::iterator i = collect.extensions.begin ();
          i != collect.extensions.end ();
          ++i )
      search ( database, minfreq, searchcode, i->second.frequency, i->first, tids2, counter );

    }

    totTmp += (clock() - timeTmp)/(double)CLOCKS_PER_SEC;
    timeTmp = clock();

    if (interval_size != -1) {
      if (totTmp >= (double) interval_size)
	{
	  nb_intervals++;
	  printStatistics(counter);
	  if (nb_int_before_kill != -1 && nb_int_before_kill <= nb_intervals) {
	    cout << "Time limit exceded, end." << endl;
	    exit(0);
	  }
	  totTmp -= (double) interval_size;
	}
    }
  }
  searchcode->pop_back ();
}

void search ( GDatabase &database, GFrequency minfreq, int &counter ) {
  map<GToken,GTokenDatabaseData,GTokenLt>::iterator i = database.tokendata.begin ();
  GDFSCode searchcode; // we use the DFS code for the search
  
  while ( i != database.tokendata.end () ) {
    if ( i->second.tids.size () >= minfreq ) {
      searchcode.setup ( i->first );
      search ( database, minfreq, &searchcode, i->second.tids.size (), i->first, i->second.tids, counter );
    }
    ++i;
  }
}


int main(int argc, char *argv[])
{
  nb_intervals=0;
  timeTmp = clock();

  GDatabase database;
  cout << "YAI-Span: Yet Another Implementation of gSpan" << endl
       << "Siegfried Nijssen (2005)" << endl
       << "=============================================" << endl;

  if (argc < 4 || argc > 6)
  {
	cerr << "usage: " << argv[0] << " <frequency threshold (absolute)> <input file> <output file> [<stat_interval_size> [<nb_interval_before_being_killed]]" << endl;
	return EXIT_FAILURE;
  }
  
  if (argc >= 5) {
    interval_size = atoi(argv[4]);
    printf("Interval size: %d s\n", interval_size);
  }
  else
    interval_size = -1;
  
  if (argc == 6) {
    nb_int_before_kill = atoi(argv[5]);
    printf("Killed after %d intervals\n", nb_int_before_kill);
  }
  else
    nb_int_before_kill = -1;
    

  cout << "Opening " << argv[2] << endl;
  database.read ( argv[2] );

  GFrequency minfreq = atoi ( argv[1] );
  cout << "Frequency threshold: " << minfreq << endl;

  outfile.open(argv[3]);
  cout << "Output file: " << argv[3] << endl;

  outfile << "# gSpan results" << endl 
      << "#  Input file: " << argv[2] << endl 
      << "#  Frequency threshold: " << minfreq << endl;
  
  int counter = 0;
  totMap = totMatching = totExt = totIso = totCanonical = 0; //time
  totalSz = numMatchings = szMatchings = count = 0; //number

  cout << "Searching... " << endl;

  printf("#!# nb_patterns\tpatterns_size\tnb_matchings\tmatchings_size\tnb_extensions\t");
  printf("tot_time\textension_time\tmatching_time\tcanonical_time\n");
  search ( database, minfreq, counter );

  printf("final stats:\nt");
  printStatistics(counter); 
  
  outfile.close();
  
  return EXIT_SUCCESS;
}
