#!/bin/bash

# Script that will apply graph mining via gSpan and plagram

# CONST
## DIR
GRAPHDIR="data/graph/"
RESPLGDIR="data/graph/resultplg/"
RESGSPDIR="data/graph/resultgsp/"
RESGRIDDIR="grid/"
RESTRIDIR="tri/"
RESKNNDIR="knn/"
# GRAPH EXT
GSP=".gsp"
PLG=".plg"
# STAT FILE
GSPGRIDSTAT="GraphStatGridGSP.txt"
PLGGRIDSTAT="GraphStatGridPLG.txt"
GSPKNNSTAT="GraphStatKNNGSP.txt"
PLGTRISTAT="GraphStatTriPLG.txt"

# Create dir for results
mkdir $RESPLGDIR
mkdir $RESGSPDIR
mkdir $RESGSPDIR$RESGRIDDIR
mkdir $RESGSPDIR$RESKNNDIR
mkdir $RESPLGDIR$RESGRIDDIR
mkdir $RESPLGDIR$RESKNNDIR

[ -f $GSPGRIDSTAT ] && cat $GSPGRIDSTAT >> ${GSPGRIDSTAT}".old" && rm $GSPGRIDSTAT
[ -f $GSPKNNSTAT ] && cat $GSPKNNSTAT >> ${GSPKNNSTAT}".old" && rm $GSPKNNSTAT
[ -f $PLGGRIDSTAT ] && cat $PLGGRIDSTAT >> ${PLGGRIDSTAT}".old" && rm $PLGGRIDSTAT
[ -f $PLGTRISTAT ] && cat $PLGKNNSTAT >> ${PLGKNNSTAT}".old" && rm $PLGKNNSTAT

f=$1

# Plagram mining
echo "======================================================"
echo "!                      GRID MINING                   !"
echo "======================================================"
echo "===== PLAGRAM  ====="
echo "====="
echo "FILE : $f"
tmp="$RESPLGDIR$RESGRIDDIR${f#${GRAPHDIR}}";
for i in 1000 750 500 400 300 250 200 150 125 100 75 50  ; do 
	echo "FREQ : $i"
    ./plagram -cqo -f $i ${f}.plg $tmp-$i$PLG >> $PLGGRIDSTAT
	echo "================================================== " >> $PLGGRIDSTAT
done
echo "===== GSPAN ====== EXTENDED GRAPH"
echo "====="
echo "FILE : $f"
tmp="$RESGSPDIR$RESGRIDDIR${f#${GRAPHDIR}}"
for i in 1000 750 500 400 300 250 200 150 125 100 75 50 ; do
	echo "FREQ : $i"
    ./gSpan2005 $i ${f}.plg $tmp-$i$PLG 60 30 >> $GSPGRIDSTAT
echo "================================================== " >> $GSPGRIDSTAT

echo "===== GSPAN ===== NORMAL GRAPH"
echo "====="
echo "FILE : $f"
tmp="$RESGSPDIR$RESGRIDDIR${f#${GRAPHDIR}}"
for i in 1000 750 500 400 300 250 200 150 125 100 75 50 ; do 
    echo "FREQ : $i"
	./gSpan2005 $i ${f}.gsp $tmp-$i$GSP 60 30 >> $GSPGRIDSTAT
    echo "================================================= " >> $GSPGRIDSTAT
done
