/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *   Copyright (C) 2010,2011 by Adriana Prado and Baptiste Jeudy           *
 *   baptiste.jeudy at univ-st-etienne fr                                  *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef GCANONICALDFSCOMPUTER_H
#define GCANONICALDFSCOMPUTER_H


#include <vector>
#include "ggraph.h"
#include "gpattern.h"
#include "global.h"

/*
 This is a class which wraps around all procedures and data required to
 compute a canonical code.  It will be created for this computation, and
 then deleted again.

 A lot of the code is more or less the same as in gextensioncollect or 
 gsubgraphiso, but it seems difficult to factorize.
*/

class GCanonicalDFSComputer{
public:
    GCanonicalDFSComputer( GPattern *pattern );
    ~GCanonicalDFSComputer();
    bool isCanonical (); 

private:
    GPattern *pattern;

    // mapping of node ids in the pattern graph and in the current code
    // if i is the index of a node in the code then
    // graph_index[i] is the corresponding index in the graph and
    // code_index[graph_index[i]] == i
    vector<GNodeID> code_index;
    vector<GNodeID> graph_index;
    
    /* active nodes id that cannot appears between the starting and ending
       nodes of an extension (as in gextensioncollect) */
    vector<bool> bannedNodes; 
    /* lastEdgeUsed[i] : id of the last edge from i used in the current
       code (i and lastEdgeUsed[i] are indexes in the graph) (similar to
       gsubgraphiso) */
    vector<GEdgeID> lastEdgeUsed; 

    // the following have the same meaning as in gpattern.h
    vector<GNodeID> nextactive;
    vector<bool> isactive;
    int activenodesbegin, activenodesend;
    
    GNodeID findPathDestination (GGraphEdge *edge);
    void buildExtension(GNodeID to, GGraphEdge *edge, 
		       GEdgeID edgeid, vector<GToken> &p);
    int compareFace(vector<GToken> &p, uint *index);
    void updateActiveNodes(GNodeID start, GNodeID stop, uint edge_nb);
    void setupActiveNodes(int node_nb);
    void updateBannedNodes();
    void findPath(vector<GToken> &p);
    bool validateExtension(GNodeID start, GNodeID stop);
};

#endif
