#!/bin/bash

DIR="data/graph/resultplg/grid/"
WEKADIR="wekafile/"

PLG=".plg"
GSP=".gsp"
WEKA=".arff"

mkdir $DIR$WEKADIR

# For each img
#for i in {1..1000..1} ; do
# For each min threshold
#   echo "Image : $i"i
for j in 250 200 150 125 100 ; do
	for f in $DIR*${j}.plg ; do
		echo "Min Threshold : $j"
		echo $f
		tmp="$DIR$WEKADIR${f#${DIR}}"
		tmp="${tmp%${PLG}}$WEKA"
		echo $tmp
		echo "@relation ImageClassif" >> $tmp 
		echo "" >> $tmp
		nbpattern=$(grep -c "t " $f)
		echo "Nb Pattern : $nbpattern"
	    for i in {1..1000..1} ; do 
			for (( k=1; k<${nbpattern}; k++ )) ; do
                echo -n $(grep -c "o $k $i " $f), >> $tmp
            done
            cl=$(( $(( $i + 1 )) / 50))
            case $cl in 
                0 ) echo "Class1" >> $tmp ;;
                1 ) echo "Class2" >> $tmp;;
                2 ) echo "Class3" >> $tmp ;;
                3 ) echo "Class4" >> $tmp ;;
                4 ) echo "Class5" >> $tmp ;;
                5 ) echo "Class6" >> $tmp ;;
                6 ) echo "Class7" >> $tmp ;;
                7 ) echo "Class8" >> $tmp ;;
                8 ) echo "Class9" >> $tmp ;;
                9 ) echo "Class10" >> $tmp ;;
                * ) echo "ClassX" >> $tmp ;;
            esac
        done
    done
done

