/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *   Copyright (C) 2010,2011 by Adriana Prado and Baptiste Jeudy           *
 *   baptiste.jeudy at univ-st-etienne fr                                  *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "ggraphcode.h"
#include "ggraph.h"

GGraphCode::GGraphCode()
{
}


GGraphCode::~GGraphCode()
{
}

void printGToken ( GToken &token ) 
{
  int to = token.to;
  int todata = token.todata;
  int data = token.data;

  printf ( "(%i,%i,%i,%i,%i) ", token.from, to, token.fromdata, data, todata );

  if ( token.direction == forward )
    printf ( " [fw] )" );
  else
    printf ( " [bw] )" ); 
}


void printGToken ( vector<GToken> &vtoken ) 
{
  printf("# ");
  for (uint i=0;i<vtoken.size();i++)
    printGToken(vtoken[i]);
}


ostream& operator<<(ostream& stream, vector<GToken> &vtoken )
{

  for (uint i=0;i<vtoken.size();i++)
    {
      stream << "(" << vtoken[i].from << "," << vtoken[i].to << "," << vtoken[i].fromdata << "," << vtoken[i].data << "," << vtoken[i].todata << "," << vtoken[i].direction << ") ";
  }

  stream << endl;   
  return stream;
}





