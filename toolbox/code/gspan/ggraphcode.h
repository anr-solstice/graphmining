/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef GGRAPHCODE_H
#define GGRAPHCODE_H
#include "global.h"
#include <vector>
#include <string.h>

class GGraph;

enum GTokenDirection { backward, forward };

struct GToken {
  GNodeID from, to;
  GNodeData fromdata;
  GEdgeData data;
  GNodeData todata;
  GTokenDirection direction;
};

void printGToken ( GToken &token );

inline int cmpGToken ( const GToken &t1, const GToken &t2 ) {
  return memcmp ( (void*) &t1, (void*) &t2, sizeof(GNodeID) * 2 + sizeof(GNodeData) * 2 + sizeof(GEdgeData) );
}

// a default comparison for tokens
struct GTokenLt {
  bool operator()(const GToken &t1, const GToken &t2 ) const
  {
    return cmpGToken ( t1, t2 ) < 0;
  }
};

// The GGraphCode consists of a sequence of Tokens.
// Algorithms that use this function assume that every prefix of the code represents a connected graph.
// The first node in a tuple must represent a node that was listed in the code before, except
// for the first token. The direction is 'backward' if also the second node is listed in the code
// before.
// This is a valid code:
// (1,3,..,..,..)(0,1,..,..,..)(1,2,..,..,..)(0,2,..,..,..)
// The last token is 'backward'.
// A GGraphCode for a GGraph is required by the Subgraph Isomorphism procedure.
/**
@author Siegfried Nijssen
*/
class GGraphCode{
public:
    GGraphCode();

    ~GGraphCode();
    
    vector<GToken> tokens;
};

#endif
