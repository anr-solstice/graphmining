/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "gdatabase.h"
#include <fstream>

GDatabase::GDatabase()
{
    
}


GDatabase::~GDatabase()
{
}


/*void GDatabase::read ( string filename ) {
  //char header[100];
  ifstream input ( filename.c_str () );

  if (!input.is_open()){
	cout << "Could not open file " << filename.c_str() << endl;
        exit(1);
  }

  GGraph *graph = new GGraph ();
  
  //  input.getline ( header, 100 ); // we assume a header before each graph
  input >> *graph;
  while ( !input.eof () ) {
    process ( graph );
    graphs.push_back ( graph );
    graph = new GGraph ();
    //input.getline ( header, 100 );
    input >> *graph;
  }
  delete graph;
  
  input.close ();
} */

//new
void GDatabase::read ( string filename )
{
  GGraph *graph;
  vector<string> tokens;

  Data *data = new Data(filename);
  data->getLine(&tokens); //read line of type "t"
  
  while ( !data->finished() ) 
  {
    graph = new GGraph ();
    graph->read(data); //read graph and instantiate data structure 
    process ( graph );
    graphs.push_back ( graph );
  }
  delete data;
} 


void GDatabase::process ( GGraph *graph ) {
  pair<GToken,GTokenDatabaseData> d;
  d.first.from = 0;
  d.first.to = 1;
  d.first.direction = forward;
  
  // maintain tid lists for each token
  for ( int i = 0, size = graph->nodes.size (); i < size; ++i ) {
    GGraphNode &node = graph->nodes[i];
    d.first.fromdata = node.data;
    
    
    vector<GGraphEdge> &edges = graph->nodes[i].edges;
    for ( int j = 0, size2 = edges.size (); j < size2; ++j ) {
      d.first.data = edges[j].data;
      d.first.todata =  graph->nodes[edges[j].nodeid].data;
      
      if ( d.first.fromdata >= d.first.todata ) {
        pair<map<GToken,GTokenDatabaseData,GTokenLt>::iterator,bool> p = tokendata.insert ( d );
        if ( p.first->second.tids.empty () ||
             p.first->second.tids.back () != (GTid) graph )
          p.first->second.tids.push_back ( (GTid) graph );
      }
    }
  }
}
