function [ errmsg ] = wgraph( data, set , filename)
%% Function creating file for weka
%
%Syntaxe
% [ ] = weka (hist, nbwords, filename)
%
%Parameters
% All parameter are required
% - Hist (required)
%       -> Histograme construct from [h,c]=hist(desc, model).
% - Class (required)
%       -> List of all class
% - imgclass (required)
%       -> matrix of size nbimg x 2 such that :
%       -> Dimension 1 : imgID
%       -> Dimension 2 : classID
% - filename
%       -> Name of the file where data will be written.
%
%Description
% Return an error message if the openning file failed.

% =========================================================================
%% Initialise train file for weka :
% Trace
fprintf('Writing graph files \n');

conf = data.conf;

if strcmp(set,'full')
    set = data.train;
    wmethod = 'w';
    write(wmethod);
    set = data.val;
    wmethod = 'a';
    write(wmethod);
elseif strcmp(set,'train')
    set = data.train;
    wmethod = 'w';
    write(wmethod);
elseif strcmp(set, 'val')
    set = data.val;
    wmethod = 'w';
    write(wmethod);
end


    function [] = write(wmethod)
        if strcmp(conf.method,'grid')
            % Openning File
            [fileID.gsp,~] = fopen(sprintf('%s.gsp',filename), wmethod);
            [fileID.plg,~] = fopen(sprintf('%s.plg',filename), wmethod);
            for i=1:size(set.hknnclust,2)
                c1 = clock;
                w = set.frame{i}(1);
                h = set.frame{i}(2);
                % Write graph Id
                % Standard grid representation
                if strcmp(wmethod,'a')
                    fprintf(fileID.gsp,'t %d\n',i+500);
                else
                    fprintf(fileID.gsp,'t %d\n',i);
                end
                fprintf(fileID.gsp,'n %d\n',(w-1)*h+(h-1)*w);
                % Grid adaptation for plagram
                if strcmp(wmethod,'a')
                    fprintf(fileID.plg,'t %d\n',i+500);
                else
                    fprintf(fileID.plg,'t %d\n',i);
                end
                fprintf(fileID.plg,'n %d\n',((w*2-1)*(h*2)+(h*2-1)*(w*2)));
                for j=1:1:size(set.hknnclust{i},1)
                    % Standard grid representation
                    fprintf(fileID.gsp,'v %d %d 0 \n',j-1,set.hknnclust{i}(j));
                    % Grid adaptation for plagram
                    fprintf(fileID.plg,'v %d %d 0 \n',(j-1)*4,set.hknnclust{i}(j));
                    fprintf(fileID.plg,'v %d %d 0 \n',(j-1)*4+1,set.hknnclust{i}(j));
                    fprintf(fileID.plg,'v %d %d 0 \n',(j-1)*4+2,set.hknnclust{i}(j));
                    fprintf(fileID.plg,'v %d %d 0 \n',(j-1)*4+3,set.hknnclust{i}(j));
                end
                etime(clock,c1);
                c2 = clock ;
                ide = 1; % index of edge
                e = double.empty(0, 2); % list of already writtend edge
                % Standard grid representatio
                for j=1:1:size(set.hknnclust{i},1)
                    switch mod(j,w)
                        case 1
                            if j == 1
                                tledge(fileID.gsp, j-1 , w , { '0' , '0' } ) ;
                            elseif j == (w*h-w+1)
                                bledge(fileID.gsp, j-1 , w , { '0' , '0' } ) ;
                            else
                                ledge(fileID.gsp, j-1 , w , { '0' , '0' , '0' } ) ;
                            end
                        case 0
                            if j == w
                                tredge(fileID.gsp, j-1 , w , { '0' , '0' } ) ;
                            elseif j==w*h
                                bredge(fileID.gsp, j-1 , w , { '0' , '0' } ) ;
                            else
                                redge(fileID.gsp, j-1 , w , { '0' , '0' , '0'} ) ;
                            end
                        otherwise
                            if j < w
                                tedge(fileID.gsp, j-1 , w , { '0' , '0' , '0' } ) ;
                            elseif j > w*h-w
                                bedge(fileID.gsp, j-1 , w , { '0' , '0' , '0' } ) ;
                            else
                                edge(fileID.gsp, j-1 , w , { '0' , '0'  , '0' , '0'} ) ;
                            end
                    end
                end
                % Grid adaptation for
                ide = 1; % index of edge
                e = double.empty(0, 2); % list of already writtend edge
                for j=1:1:size(set.hknnclust{i},1)
                    % Index of topleft node
                    k= (j-1)*4+1;
                    switch mod(j,w)
                        case 1
                            if j == 1
                                tledgePLG(fileID.plg, k-1 , 2*w ) ;
                            elseif j == (w*h-w+1)
                                bledgePLG(fileID.plg, k-1 , 2*w ) ;
                            else
                                ledgePLG(fileID.plg, k-1 , 2*w ) ;
                            end
                        case 0
                            if j == w
                                tredgePLG(fileID.plg, k-1 , 2*w ) ;
                            elseif j==w*h
                                bredgePLG(fileID.plg, k-1 , 2*w ) ;
                            else
                                redgePLG(fileID.plg, k-1 , 2*w ) ;
                            end
                        otherwise
                            if j < w
                                tedgePLG(fileID.plg, k-1 , 2*w ) ;
                            elseif j > w*h-w
                                bedgePLG(fileID.plg, k-1 , 2*w ) ;
                            else
                                edgePLG(fileID.plg, k-1 , 2*w ) ;
                            end
                    end
                end
                etime(clock,c2);
                etime(clock,c1);
            end
        else
            % Opening file
            [fileID.gsp,errmsg] = fopen(sprintf('%s.gsp',filename), wmethod);
            [fileID.plg,errmsg] = fopen(sprintf('%s.plg',filename), wmethod);
            
            % Writing header
            fprintf(fileID.gsp,'# Gspan file \n \n');
            fprintf(fileID.plg,'# Plagram file \n \n');
            % Writing words
            for i=1:size(set.hknncell,2)
                v = size(set.neighbor{i},1);
                % GSPAN
                % Writing graph number line
                fprintf(fileID.gsp,'t %d \n',i);
                e = double.empty ;
                nbe = 0;
                for j=1:v
                    % Writing edge for each vertex with label
                    for k=1:conf.sift.kneighbor
                        if (size(e,1)==0) ||...
                                isempty(find(j == e(:,2), 1)) ||...
                                ~ismember(set.neighbor{i}(j,k),e(j==e(:,2)))
                            nbe = nbe +1;
                            e = [ e ; j set.neighbor{i}(j,k) ] ;
                        end
                    end
                end
                % Writing number of edge (nb of interest point*nb of kneighbor)
                fprintf(fileID.gsp,'n %d \n',nbe);
                for j=1:v
                    % Writing vertex with label
                    fprintf(fileID.gsp,'v %d %d 0 \n',j-1, set.hknnclust{i}(j) );
                end
                for j=1:nbe
                    fprintf(fileID.gsp,'e %d %d 0 \n',e(j,1)-1, e(j,2)-1);
                end
                
                % PLAGRAM TRIANGULATION
                e = double.empty ;
                nbe = 0;
                for j=1:v
                    % Code found on support matlab for getting duplicate index :
                    f = set.frame{i};
                    c = set.hknnclust{i};
                    [ ~, a ~ ] = unique(f,'rows');
                    dupeinds = setdiff(1:size(f,1),a); % Duplicate indexes !!!
                    % Remove first duplicate
                    f(dupeinds,:)=[];
                    c(dupeinds,:)=[];
                    % Applying delaunay triangulation
                    TRI = DelaunayTri(f);
                    Tri = sortrows(TRI.Triangulation);
                end
                % Writing edge for each vertex with label in matrix e
                for j=1:size(Tri,1)
                    for k=2:3
                        if (size(e,1)==0) ||...
                                isempty(find(and(Tri(j,1)==e(:,1),Tri(j,k)==e(:,2)), 1))
                            nbe = nbe +1;
                            e = [ e ; Tri(j,1) Tri(j,k) ] ;
                        end
                    end
                end
                % Writing graph number line and number edge
                fprintf(fileID.plg,'t %d \n',i);
                fprintf(fileID.plg,'n %d \n',nbe);
                for j=1:size(c)
                    % Writing vertex with label
                    fprintf(fileID.plg,'v %d %d 0 \n',j-1, c(j) );
                end
                % Writing Edges
                for j=1:size(c,1)
                    allj=find(j==e(:,1));
                    for k=1:size(allj)
                        fprintf(fileID.plg,'e %d %d %s %d \n',e(allj(k),1)-1,e(allj(k),2)-1,'0', allj(k)-1);
                    end
                    allj=find(j==e(:,2));
                    for k=1:size(allj)
                        fprintf(fileID.plg,'e %d %d %s %d \n',e(allj(k),2)-1,e(allj(k),1)-1,'0', allj(k)-1);
                    end
                end
            end
            fclose(fileID.gsp);
            fclose(fileID.plg);
        end
        
        %% Subfunction for grid file writing
        % For each subfunction :
        % PARAMETER
        %    f = File ID in which edge will be written
        %    i = idx of vertex
        %    w = width of grid
        %    l = matrix with label for each edge : {'0','1','0','0'}.
        %         -> If not specified, all value of l = '0'.
        %
        % Each subfunction will write specific edge define by first letter of
        % the function such that :
        % t       = top edge
        % b       = bottom edge
        % l       = left edge
        % r       = right edge
        % tl/tr   = top left/right edge
        % bl/br   = bottom left/right edge
        % Nothing = middle edge
        function [ ] = tledge(f,i,w,l)
            if nargin ~= 4
                l = { '0' ; '0' } ;
            end
            e = [ e ; i i+w ] ;
            fprintf(f,'e %d %d %s %d \n',i,i+w,l{1}, ide-1); ide=ide+1;
            e = [ e ; i i+1 ] ;
            fprintf(f,'e %d %d %s %d \n',i,i+1,l{2}, ide-1); ide=ide+1;
        end
        function [ ] = tedge(f,i,w,l)
            if nargin ~= 4
                l = { '0' ; '0' ; '0' };
            end
            fprintf(f,'e %d %d %s %d \n',i,i-1,l{1}, find(and(i-1==e(:,1),i==e(:,2)))-1);
            e = [ e ; i i+w ] ;
            fprintf(f,'e %d %d %s %d \n',i,i+w,l{2}, ide-1); ide=ide+1;
            e = [ e ; i i+1 ] ;
            fprintf(f,'e %d %d %s %d \n',i,i+1,l{3}, ide-1); ide=ide+1;
        end
        function [ ] = tredge(f,i,w,l)
            if nargin ~= 4
                l = { '0' ; '0' } ;
            end   
            fprintf(f,'e %d %d %s %d \n',i,i-1,l{1}, find(and(i-1==e(:,1),i==e(:,2)))-1);
            e = [ e ; i i+w ] ;
            fprintf(f,'e %d %d %s %d \n',i,i+w,l{2}, ide-1); ide=ide+1;
        end
        function [ ] = bledge(f,i,w,l)
            if nargin ~= 4
                l = { '0' ; '0' } ;
            end
            e = [ e ; i i+1 ] ;
            fprintf(f,'e %d %d %s %d \n',i,i+1,l{1}, ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',i,i-w,l{2}, find(and(i-w==e(:,1),i==e(:,2)))-w);
            
        end
        function [ ] = bedge(f,i,w,l)
            if nargin ~= 4
                l = { '0' ; '0' ; '0' };
            end
            fprintf(f,'e %d %d %s %d \n',i,i-1,l{1}, find(and(i-1==e(:,1),i==e(:,2)))-1);
            e = [ e ; i i+1 ] ;
            fprintf(f,'e %d %d %s %d \n',i,i+1,l{2}, ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',i,i-w,l{3}, find(and(i-w==e(:,1),i==e(:,2)))-w);
        end
        function [ ] = bredge(f,i,w,l)
            if nargin ~= 4
                l = { '0' ; '0' } ;
            end
            fprintf(f,'e %d %d %s %d \n',i,i-w,l{1}, find(and(i-w==e(:,1),i==e(:,2)))-w);
            fprintf(f,'e %d %d %s %d \n',i,i-1,l{2}, find(and(i-1==e(:,1),i==e(:,2)))-1);
        end
        function [ ] = ledge(f,i,w,l)
            if nargin ~= 4
                l = { '0' ; '0' ; '0' } ;
            end
            fprintf(f,'e %d %d %s %d \n',i,i-w,l{1}, find(and(i-w==e(:,1),i==e(:,2)))-w);
            e = [ e ; i i+1 ] ;
            fprintf(f,'e %d %d %s %d \n',i,i+1,l{2}, ide-1); ide=ide+1;
            e = [ e ; i i+w ] ;
            fprintf(f,'e %d %d %s %d \n',i,i+w,l{3}, ide-1); ide=ide+1;            
        end
        function [ ] = edge(f,i,w,l)
            if nargin ~= 4
                l = { '0' ; '0' ; '0' ; '0' };
            end
            fprintf(f,'e %d %d %s %d \n',i,i-1,l{1}, find(and(i-1==e(:,1),i==e(:,2)))-1);
            e = [ e ; i i+w ] ;
            fprintf(f,'e %d %d %s %d \n',i,i+w,l{2}, ide-1); ide=ide+1;
            e = [ e ; i i+1 ] ;
            fprintf(f,'e %d %d %s %d \n',i,i+1,l{3}, ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',i,i-w,l{4}, find(and(i-w==e(:,1),i==e(:,2)))-w);
        end
        function [ ] = redge(f,i,w,l)
            if nargin ~= 4
                l = { '0' ; '0' } ;
            end
            fprintf(f,'e %d %d %s %d \n',i,i-w,l{1}, find(and(i-w==e(:,1),i==e(:,2)))-w);
            fprintf(f,'e %d %d %s %d \n',i,i-1,l{2}, find(and(i-1==e(:,1),i==e(:,2)))-1);
            e = [ e ; i i+w ] ;
            fprintf(f,'e %d %d %s %d \n',i,i+w,l{3}, ide-1); ide=ide+1;
            
        end
        
        %% Subfunction for grid file writing specific to plagram
        function [ ] = tledgePLG(f,i,w)
            ei = i;
            % e1 = tledge
            e = [ e ; ei ei+2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+1 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0',ide-1); ide=ide+1;
            % e2 = tedge
            ei = i+1 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei+2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+3 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
            % e3 = ledge
            ei = i+2 ;
            e = [ e ; ei ei+2*w-2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+1 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
            % e4 = edge
            ei = i + 3 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei+2*w-2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+3 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
        end
        function [ ] = tedgePLG(f,i,w)
            % e1 = tedge
            ei = i ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei + 2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+1 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
            % e2 = tedge
            ei = i+1 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei + 2] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+3] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
            % e3 = edge
            ei = i+2 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei+2*w-2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+1 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
            % e4 = edge
            ei = i + 3 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei+2*w-2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+3] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
        end
        function [ ] = tredgePLG(f,i,w)
            % e1 = tedge
            ei = i ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei + 2] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+1 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
            % e2 = tredge
            ei = i+1 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei + 2] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
            % e3 = edge
            ei = i+2 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei+2*w-2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+1 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
            % e4 = redge
            ei = i + 3 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
            fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei+2*w-2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
        end
        function [ ] = bledgePLG(f,i,w)
            % e1 = ledge
            ei = i ;
            e = [ e ; ei ei + 2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+1 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
            % e2 = edge
            ei = i+1 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei + 2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+3] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
            % e3 = bledge
            ei = i+2 ;
            e = [ e ; ei ei+1 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
            % e4 = bedge
            ei = i + 3 ;
            e = [ e ; ei ei+3] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
            fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
        end
        function [ ] = bedgePLG(f,i,w)
            % e1 = edge
            ei = i ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei + 2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+1 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
            % e2 = edge
            ei = i+1 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei + 2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+3] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
            % e3 = bedge
            ei = i+2 ;
            e = [ e ; ei ei+1 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
            fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
            % e4 = bedge
            ei = i + 3 ;
            e = [ e ; ei ei+3] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
            fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
        end
        function [ ] = bredgePLG(f,i,w)
            % e1 = edge
            ei = i ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei + 2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+1 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
            % e2 = redge
            ei = i+1 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
            fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei + 2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
            % e3 = bedge
            ei = i+2 ;
            e = [ e ; ei ei+1 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
            fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
            % e4 = bredge
            ei = i + 3 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
        end
        function [ ] = ledgePLG(f,i,w)
            % e1 = ledge
            ei = i ;
            e = [ e ; ei ei + 2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+1 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
            % e2 = edge
            ei = i+1 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei + 2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+3] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
            % e3 = ledge
            ei = i+2 ;
            e = [ e ; ei ei + 2*w-2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+1 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
            % e4 = edge
            ei = i + 3 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei+2*w-2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+3] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
        end
        function [ ] = edgePLG(f,i,w)
            % e1 = edge
            ei = i ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei + 2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+1 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
            % e2 = edge
            ei = i+1 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei + 2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+3] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
            % e3 = edge
            ei = i+2 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei+2*w-2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+1 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
            % e4 = edge
            ei = i + 3 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei+2*w-2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+3] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+3,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
        end
        function [ ] = redgePLG(f,i,w)
            % e1 = edge
            ei = i ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei + 2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+1 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
            % e2 = redge
            ei = i+1 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2*w+2,'0', find(and(ei-2*w+2==e(:,1),ei==e(:,2)))-1);
            fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei + 2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2,'0', ide-1); ide=ide+1;
            % e3 = edge
            ei = i+2 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-3,'0', find(and(ei-3==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei+2*w-2 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
            e = [ e ; ei ei+1 ] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+1,'0', ide-1); ide=ide+1;
            fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
            % e4 = redge
            ei = i + 3 ;
            fprintf(f,'e %d %d %s %d \n',ei,ei-1,'0', find(and(ei-1==e(:,1),ei==e(:,2)))-1);
            fprintf(f,'e %d %d %s %d \n',ei,ei-2,'0', find(and(ei-2==e(:,1),ei==e(:,2)))-1);
            e = [ e ; ei ei+2*w-2] ;
            fprintf(f,'e %d %d %s %d \n',ei,ei+2*w-2,'0', ide-1); ide=ide+1;
        end
    end





end % End of function
