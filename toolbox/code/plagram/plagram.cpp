/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *   Copyright (C) 2010,2011 by Adriana Prado and Baptiste Jeudy           *
 *   baptiste.jeudy at univ-st-etienne fr                                  *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <unistd.h>
#include <getopt.h>

#include <iostream>
#include <cstdlib>
#include <fstream>
#include "ggraph.h"
#include "gdatabase.h"
#include "gpattern.h"
#include "gextensioncollect.h"
#include "gsubgraphiso.h"
#include <fstream>

using namespace std;

ofstream outfile;


int freq_pattern_nb;
int nb_closed;
int output_occ;
bool do_quickprune; 
bool output_non_closed_pat;
// list of occurrences of current pattern, also used in gsubgraphiso (as
// largegraph_index[] field)
vector<GNodeID> occ_list;

/*********************  for stats **********************/
long long int nprocess=0, nrun=0;

clock_t timeEdge;
double totEdge;

clock_t timeIso;
double totIso;

clock_t timeCanonical;
double totCanonical;

clock_t timetid;
double tottid;

clock_t timeoutput;
double totoutput;

clock_t timet1;
double tott1;

clock_t timet2;
double tott2;

double totMatching;
double totExt;
double totMap;
double totValid;

long long int count;
long long int quickpruned;
long long int countNotValid;

long int tot_notcanonic;

long long int numMatchings;
long long int szMatchings;
long long int numEdges; //for the extensions
long long int totalSz; //patterns
/*
void printStatistics(GFrequency minfreq, int nb_patterns)
{

  printf("#!  %11d %13d %15lld %14lld %16lld %15lld %17lld \t", 
	 minfreq, nb_patterns, totalSz, numMatchings, szMatchings, count, numEdges);
  printf("9:tot_time 10:extension_time 11:matching_time 12:canonical_time 13:init_time\n",);
  

  cout << "init time: " << "[" << totInit << "]" << endl;	
  printf("Number of patterns: [%d]\n",freq_pattern_nb);	
  cout << "------Total size of patterns: " << totalSz << endl; 
  cout << "Matching time: " << "[" << totMatching << "]" << endl;	
  cout << "------Total number of matchings: " << numMatchings << endl; 
  cout << "------Total size of matchings: " << szMatchings << endl;  
  cout << "Extension time (including map): " << "[" << totExt << "]" << endl;
  cout << "Map time: " << "[" << totMap << "]" << endl;
  cout << "------Total number of extensions: " << count << endl; 
  cout << "-----------Total number of edges: " << numEdges << endl;
  cout << "nprocess " << nprocess << " nrun " << nrun << endl; 
  cout << "tott1: " << "[" << tott1/(double)CLOCKS_PER_SEC << "]" << endl;
  cout << "tott2: " << "[" << tott2/(double)CLOCKS_PER_SEC << "]" << endl;
  cout << "tottid: " << "[" << tottid << "]" << endl;
  cout << "totIso: " << "[" << totIso << "]" << endl;
  cout << "Canonical-test time: " << "[" << totCanonical << "]" << endl;
  cout  << "**************************" << endl;



  fflush(stdout);
}
*/
/******************************************************/


/* Recursive function that actually explore the code search space.  The
   current pattern is P = "pattern"+"last_extension" ("last_extension" is
   the last added extension) and P is frequent.  This function mine all
   frequent and canonical super patterns of P.

   The first level of the search space consists of frequent one-edge graphs
   (the "pattern" is the empty graph and "last_extension" is only one edge).

   At other levels n>1, P consists of (n-1) complete faces.

   This function return true if P is canonical (ie, P is a frequent canonical
   pattern code).
*/
bool search ( GDatabase &database, // graphs of the database
	      GFrequency minfreq, int maxgap, 
	      GPattern *pattern, 
	      GFrequency frequency, // frequency of P
	      const vector<GToken> &last_extension, 
	      vector<GTidEid> &tids)  // list of occurrences of "pattern"
{
  GFrequency nb_occ = 0; // nb of occurrence of P (to check if P is closed)
  GFrequency freq = 0; // frequency of P (computed again, to check for bugs)
  GTid last_occ = NULL; // in which large graph was the last occurrence of P
  // add "last_extension" to "pattern": 
  // "pattern" now contain pattern P code and graph
  pattern->push_back( last_extension ); 

  /*********************** check if P is a canonical code *****************/
  timeCanonical = clock();
  bool is_canonical = pattern->isCanonical();
  timeCanonical = clock() - timeCanonical;
  totCanonical += timeCanonical;
 
  if (! is_canonical) {
    // restore the value of pattern
    pattern->pop_back(last_extension);
    tot_notcanonic++;
    return false;
  }
  
  /******** Computes all the extensions of P in the whole database. *********/
  GExtensionCollect collect(minfreq, maxgap, do_quickprune);
  GSubGraphIso subgraphiso(pattern, &occ_list);
  vector<GTidEid> tids2;
  // tids contain a list of the occurences of previous "pattern",
  // thus the occurrences of P are a subset of tids
  // The occurrences of P with valid extensions will be stored in tids2

  timeIso = clock();
  for ( int j = 0, size = tids.size (); j < size; ++j ) {
    // subisomorphism test and generation of all extensions
    timet1 = clock();
    nrun++;
    bool find_occ = subgraphiso.run ( tids[j].tid, tids[j].nodeid, tids[j].edgeid );
    tott1 += clock() - timet1;
    if (find_occ) {
      nb_occ++;
      if (last_occ != tids[j].tid) {
	freq++;
	last_occ = tids[j].tid;
      }
      // compute all extensions of this occurrence
      timet2 = clock();
      nprocess++;
      int valid_ext_nb = collect.process( &subgraphiso ); 
      tott2 += clock() - timet2;
      // if tids[j] is an occurrence of P with valid extensions, 
      // it is stored in tids2.
      if ( valid_ext_nb > 0) {
	timetid = clock();
	tids2.push_back ( tids[j] );
	tottid += clock() - timetid;
      }
    }
  }

  // stats
  timeIso = clock() - timeIso; // also contains time to compute extensions
  totIso += timeIso;
  totMatching += subgraphiso.totMatching;
  numMatchings += subgraphiso.numMatchings;
  szMatchings += subgraphiso.szMatchings;

  totExt += collect.totExt;				
  totValid += collect.totValid;
  totMap += collect.totMap;

  count += collect.count;
  quickpruned += collect.quickpruned;
  countNotValid += collect.countNotValid;
  numEdges += collect.numEdges;

  // check frequency
  if (frequency != freq && maxgap == int(database.graphs.size())) {
    cerr << "**** Internal Error : wrong frequency for pattern *****" << endl;
    exit(1);
  }


  //---------------------- check if P is closed 
  bool closed = true;
  if (pattern->tokens.size() > 1) {
    for ( map<vector<GToken>,GExtensionData,VGTokenLt>::iterator i = 
	    collect.extensions.begin ();
	  i != collect.extensions.end (); ++i ) // for each extension of P
      if (nb_occ == i->second.nb_occ) closed = false;
    if (closed) nb_closed ++;
  }
  
  /********************* outputs P and its occurrences *********************/
  // only outputs if P is not just one edge (ie, not the first level)
  timeoutput = clock();
  if (pattern->tokens.size() > 1) {
    freq_pattern_nb++;
    if (closed || output_non_closed_pat ) {
      outfile << "t " << freq_pattern_nb << " " << frequency << endl;
      outfile << "n " << pattern->tokens.size() << endl;
      outfile << pattern->graph;
      outfile << "c " << pattern->tokens; // the code of P
      totalSz += pattern->tokens.size();
      // output the occurrences: "o pattern_id largegraph_id occ_id matchings"
      if (output_occ) {
	uint occ_id = 0;
	uint node_nb = pattern->graph.nodes.size(); 
	uint end_of_occ = (subgraphiso.last_occ_valid) ?  subgraphiso.offset + node_nb : subgraphiso.offset - 1;
	uint i = 0;
	while (i < end_of_occ) {
	  outfile << "o " << freq_pattern_nb <<" "<< occ_list[i++] << " " << occ_id;
	  occ_id++;
	  for (uint k = 0; k < node_nb; k++)
	    outfile << " " << occ_list[i++];
	  outfile << endl;
	}
      }
    }
  }
  totoutput += clock() - timeoutput;

  // occ_list is not needed anymore. It must be cleaned for later use.
  subgraphiso.clear_occ_list();
  /*********************************** makes recursive calls to extend P */
  bool isleaf = true;
  for ( map<vector<GToken>,GExtensionData,VGTokenLt>::iterator i = 
	  collect.extensions.begin ();
	i != collect.extensions.end (); ++i ) // for each extension of P
    {  
      if (i->second.frequency >= minfreq) 
	// recursive calls on frequent extensions
	isleaf = ! search(database, minfreq, maxgap, pattern, 
			  i->second.frequency, i->first, tids2) && isleaf;
    }
  if (isleaf && pattern->tokens.size() > 1){
    /* Recursive calls above did not find any frequent canonical
       patterns. It means that pattern P is a leaf in the search
       tree. This is interesting because all maximal patterns are
       leaves. */
    outfile << "# graph above is a leaf" << endl;
  }
  
  // restore the value of pattern
  pattern->pop_back(last_extension);
  
  return true; 
}



void search ( GDatabase &database, GFrequency minfreq, 
	      int maxgap) 
{  /* search function called at the first level. At this point, extensions
      are only 1-edge (stored in database.tokendata) and the current
      pattern P is empty. */
  
  map<GToken,GTokenDatabaseData,GTokenLt>::iterator i = 
    database.tokendata.begin ();
  GPattern pattern; 

  vector<GToken> v;
  
  while ( i != database.tokendata.end() ) // for all canonical 1-edge extensions
    {
      timeEdge = clock();
      if ( i->second.frequency >= minfreq ) 
	{ // if this edge is frequent
	  v.push_back(i->first);// because search expects a vector as parameter
	  /**************** recursive call **************************/
	  search( database, minfreq, maxgap, &pattern, i->second.frequency,
		  v, i->second.tids);
	  v.pop_back();
	}
      ++i;
      timeEdge = clock() - timeEdge;
      totEdge += timeEdge;
    }
}

void usage() {
  cerr << "usage: plagram -f <frequency threshold (absolute)> <input file> <output file>" << endl;
  cerr << "options:" << endl;
  cerr << " -c : outputs only closed patterns (if used with -q) or pseudo-closed patterns." << endl;
  cerr << " -q : do not use quickprune techniques in extension generation." << endl;
  cerr << " -o : outputs the occurrences lists of every pattern." << endl;
  cerr << " -g <maxgap> : define the maxgap parameter. maxgap is infinite otherwise." << endl;
  cerr << "    maxgap = 1 means occurrences of a pattern must be in consecutive frames." << endl;
  cerr << "    Given two occurrences of a pattern in frameid1 and frameid2," << endl; 
  cerr << "    if (frameid2 > frameid1 + maxgap) then they do not belong to the same spacio-temporal pattern" << endl;
}

int main(int argc, char *argv[])
{
  char *cinfile, *coutfile;

  //  parsing of the command line 
  char opt;
  output_occ = 0;
  do_quickprune = true;
  output_non_closed_pat = true;
  GFrequency minfreq = -1;
  int maxgap = -1; //for temporal "dynamic" patterns
  while ((opt = getopt(argc, argv, "cf:g:oq")) != -1)
    switch (opt) {
    case 'c':
      output_non_closed_pat = false;
      break;
    case 'f':
      minfreq = atoi(optarg);
      break;
    case 'g':
      maxgap = atoi(optarg);
      break;
    case 'o':
      output_occ = 1;
      break;
    case 'q':
      do_quickprune = false;
      break;
    default:
      usage();
      return EXIT_FAILURE;
    }
  
  if (argc - optind != 2 || minfreq == -1) { 
    usage();
    return EXIT_FAILURE;
  }
  cinfile = argv[optind];
  coutfile = argv[optind+1];
  // end of command line parsing

  GDatabase database;
  cout << endl 
       << "==============================================================================" << endl
       << "Plagram : Plane graph mining" << endl
       << "Siegfried Nijssen (2005), Adriana Prado, Baptiste Jeudy, Elisa Fromont (2010,2011)." << endl
       << "==============================================================================" << endl;
  cout << "Size of long long int: " << sizeof(long long int) << ", ";
  cout << "Size of clock_t : " << sizeof(clock_t) << ", ";
  cout << "CLOCKS_PER_SEC: " << CLOCKS_PER_SEC << endl;

  clock_t start = clock();

  cout << "Frequency threshold: " << minfreq << endl;

  cout << "Opening " << cinfile << endl;
  /*********************** reads the database *********************************
  and initialise the extension list (database.tokendata) with 1-edge patterns */
  database.read ( cinfile );
  /****************************************************************************/

  if (maxgap > int (database.graphs.size()) || maxgap == -1)
    maxgap = database.graphs.size();
  cout << "Max gap: " << maxgap << endl;

  outfile.open(coutfile);
  cout << "Output file: " << coutfile << endl;
  outfile << "#";
  for (int i = 0; i < argc; i++)
    outfile << " " << argv[i];
  outfile << endl << "#  Input file: " << cinfile << endl 
	  << "#  Frequency threshold: " << minfreq << endl
	  << "#  Max gap: " << maxgap << endl << endl;
  
  /*************************  inits for stats ******************************/
  freq_pattern_nb = 0;
  tot_notcanonic = 0;
  nb_closed = 0;
  szMatchings = numMatchings = numEdges = quickpruned = count = countNotValid = totalSz= 0; //numbers
  totoutput = tott1 = tott2 = tottid = totEdge = totValid = totMap = totMatching = totExt = totIso = totCanonical = 0; //time
  clock_t finish = clock();
  double totInit = (finish - start)/(double)CLOCKS_PER_SEC;
  cout << "Searching... " << endl;

  /**************************** real things done here **********************/
  occ_list.reserve(100000);
  occ_list.resize(1000, GNONODEID);
  cout << "cap: " << occ_list.capacity() << endl;
  cout << "size: " << occ_list.size() << endl;
  search ( database, minfreq, maxgap);
  /*************************************************************************/
  outfile.close();

  totMatching = totMatching/(double)CLOCKS_PER_SEC;
  totMap= totMap/(double)CLOCKS_PER_SEC;
  totExt= totExt/(double)CLOCKS_PER_SEC;
  totIso = totIso/(double)CLOCKS_PER_SEC;
  tottid = tottid/(double)CLOCKS_PER_SEC;
  totoutput = totoutput/(double)CLOCKS_PER_SEC;
  totCanonical = totCanonical/(double)CLOCKS_PER_SEC;

  /************************** prints various stats **************************/
  cout << "**************************" << endl;
  cout << "init time: " << "[" << totInit << "]" << endl;	
  printf("Number of patterns: [%d] closed: [%d]\n",freq_pattern_nb,nb_closed);	
  cout << "------Total size of patterns: " << totalSz << endl; 
  cout << "Matching time: " << "[" << totMatching << "]" << endl;	
  cout << "------Total number of matchings: " << numMatchings << endl; 
  cout << "------Total size of matchings: " << szMatchings << endl;  
  cout << "Extension time (including map): " << "[" << totExt << "]" << endl;
  cout << "Map time: " << "[" << totMap << "]" << endl;
  cout << "------Total number of extensions: " << count << endl; 
  cout << "-----------Total number of edges: " << numEdges << endl;
  cout << "----------- number of quickpruned extensions: " << quickpruned << endl;
  cout << "nprocess " << nprocess << " nrun " << nrun << endl; 
  cout << "tott1: " << "[" << tott1/(double)CLOCKS_PER_SEC << "]" << endl;
  cout << "tott2: " << "[" << tott2/(double)CLOCKS_PER_SEC << "]" << endl;
  cout << "tottid: " << "[" << tottid << "]" << endl;
  cout << "totIso: " << "[" << totIso << "]" << endl;
  cout << "Canonical-test time: " << "[" << totCanonical << "]" << endl;
  cout << "non canonical patterns: " << "[" <<tot_notcanonic  << "]" << endl;
  cout << "output time: " << "[" << totoutput << "]" << endl;
  cout  << "**************************" << endl;
  /************************** prints various stats **************************/
  cout << "cap: " << occ_list.capacity() << endl;
  cout << "size: " << occ_list.size() << endl;

  //  printf("#!# 1:frequency 3:nb_patterns 4:patterns_size 5:nb_matchings 6:matchings_size 7:nb_extensions 8:extensions_size \t");
  //printf("9:tot_time 10:extension_time 11:matching_time 12:canonical_time 13:init_time\n");


  return EXIT_SUCCESS;
}
