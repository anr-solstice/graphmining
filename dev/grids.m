function [ d , f ] = grids(dirimg, limg, step, binsize, bounds, geometry,...
                      fast, norm)
%% Function for applying dense sift to a set of image
%
%Syntaxe
% [ d , f ] = grid(dirimg,limg)
% [ d , f ] = grid(dirimg,limg,step)
% [ d , f ] = grid(dirimg,limg,step,binsize)
% [ d , f ] = grid(dirimg,limg,step,binsize,bounds)
% [ d , f ] = grid(dirimg,limg,step,binsize,bounds,geometry)
% [ d , f ] = grid(dirimg,limg,step,binsize,bounds,geometry,fast)
% [ d , f ] = grid(dirimg,limg,step,binsize,bounds,geometry,fast,norm)
%
%Parameters
% If not specified, parameter are set either as default vl_dsift for some
% of them or as specified (for Step and Size). 
% Dirimg and Limg are required parameter !
% - Dirimg (required) !!
%       -> Path de the directory where image are stored.
% - Limg (required)
%       -> Vector or matrix where first dimension list all imgID.
% - Step = 12 
%       -> Extracts a SIFT descriptor each STEP pixels. 
%       -> Here, Step = 12, to avoid overlaping of descriptor
% - Size = 3
%       -> A spatial bin covers SIZE pixels. 
% - Bounds [whole image]
%       -> Specifies a rectangular area where descriptors should be 
% extracted. The format is [XMIN, YMIN, XMAX, YMAX]. If this option is not 
% specified, the entiere image is used. The bounding box is clipped to the 
% image boundaries. 
% - Norm
%       -> If specified, adds to the FRAMES ouptut argument a third row 
% containing the descriptor norm, or engergy, before contrast normalization
% This information can be used to suppress low contrast descriptors. 
% - Fast
%       -> If specified, use a piecewise-flat, rather than Gaussian,
% windowing function. While this breaks exact SIFT equivalence, in practice
% is much faster to compute. 
% - FloatDescriptors
%       -> If specified, the descriptor are returned in floating point 
% rather than integer format. 
% - Geomerty [4 4 8]
%       -> Specify the geometry of the descriptor as [NX NY NO], where NX 
% is the number of bin in the X direction, NY in the Y direction, and NO 
% the nubmer of orientation bins. 
%
%Description
% Return a set of frame f{i} = [x y scale angle] and sift descriptors
% d{i} = 1x128 for all image in dirimg specified by id in the first column
% of limg.
% For more information, please refer to vl_dsift documentation.

% =========================================================================
%% Initialise local variable for parfor loop ==============================
cinit = clock;
% Descriptor of all image
d = {};
f = {};

% Trace 
fprintf('Applying vl_dsift to %s Image set.  \n', dirimg);
% Temp Variable to not parse all image

limg = limg(1:round(size(limg,1)*0.05),:); % Only 5% of limg

switch nargin 
    case 2
        parfor i=1:size(limg,1)
            imc=imread(sprintf('%s%06d.jpg', dirimg, limg(i,1)));
            % Convert into gray image
            imb = single(rgb2gray(imc)) ;
            % Applying dsift
            [f{i},d{i}] = vl_dsift(imb);
        end
    case 3
        parfor i=1:size(limg,1)
            imc=imread(sprintf('%s%06d.jpg', dirimg, limg(i,1)));
            % Convert into gray image
            imb = single(rgb2gray(imc));
            % Applying dsift
            [f{i},d{i}] = vl_dsift(imb,'size', 3,...
                        'step', step);
        end
    case 4
        parfor i=1:size(limg,1)
            c1 = clock;
            imc=imread(sprintf('%s%06d.jpg', dirimg, limg(i,1)));
            % Convert into gray image
            imb = single(rgb2gray(imc)) ;
            % Applying dsift
            [f{i},d{i}] = vl_dsift(imb,'size', binsize,...
                        'step', step);
            c2 = clock;
            cdiff=etime(c2,c1)
            fprintf('Applying vl_dsift on image %02d. Time (in sec) : %03d \n',...
                i, cdiff );
            c(i)=cdiff;
        end
    case 5
        parfor i=1:size(limg,1)
            imc=imread(sprintf('%s%06d.jpg', dirimg, limg(i,1)));
            % Convert into gray image
            imb = single(rgb2gray(imc)) ;
            % Applying dsift
            [f{i},d{i}] = vl_dsift(imb,'size', binsize,...
                        'step', step,...
                        'bound', bounds);
        end
    case 6
        parfor i=1:size(limg,1)
            imc=imread(sprintf('%s%06d.jpg', dirimg, limg(i,1)));
            % Convert into gray image
            imb = single(rgb2gray(imc)) ;
            % Applying dsift
            [f{i},d{i}] = vl_dsift(imb,'size', binsize,...
                        'step', step,...
                        'bound', bounds,...
                        'geometry', geometry);
        end
    case 7
        parfor i=1:size(limg,1)
            imc=imread(sprintf('%s%06d.jpg', dirimg, limg(i,1)));
            % Convert into gray image
            imb = single(rgb2gray(imc)) ;
            % Applying dsift
            [f{i},d{i}] = vl_dsift(imb,'size', binsize,...
                        'step', step,...
                        'bound', bounds,...
                        'geometry', geometry,...
                        'fast', fast);
        end
    case 8
        parfor i=1:size(limg,1)
            imc=imread(sprintf('%s%06d.jpg', dirimg, limg(i,1)));
            % Convert into gray image
            imb = single(rgb2gray(imc)) ;
            % Applying dsift
            [f{i},d{i}] = vl_dsift(imb,'size', binsize,...
                        'step', step,...
                        'bound', bounds,...
                        'geometry', geometry,...
                        'fast', fast,...
                        'norm',  norm);
        end
end
cmean = mean(c);
cend = clock;
            fprintf('Applying vl_dsift in %06d sec with avg/img %03d. \n',...
                etime(cend,cinit), cmean);
end % End of function