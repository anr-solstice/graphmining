/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *   Copyright (C) 2010,2011 by Adriana Prado and Baptiste Jeudy           *
 *   baptiste.jeudy at univ-st-etienne fr                                  *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "ggraph.h"

GGraph::GGraph()
{
}


GGraph::~GGraph()
{
}

void GGraph::addNode ( GNodeData nodedata, bool outerface )
{ // used in this file and in gpattern.cpp
  GGraphNode gn;
  gn.data = nodedata;
  gn.outerface = outerface;
  nodes.push_back ( gn ); 
}

void GGraph::delNode ()
{ //used in gpattern.cpp
  nodes.pop_back ();
}


void GGraph::addEdge ( GNodeID from, GNodeID to, GEdgeData edgedata, 
		       GEdgeID edgeid, vector<GEdgeID> &equiv_edges )
{ // used in graph.read() when building database graphs from input file
  GGraphEdge ge1;
  
  ge1.data = edgedata;
  ge1.dest_nid = to;

  nodes[from].edges.push_back ( ge1 ); 

  //connect equivalent edges
  if (equiv_edges[edgeid] == GNOEDGEID) 
    equiv_edges[edgeid] = nodes[from].edges.size() - 1;
  else
    {
      nodes[from].edges.back().eq_eid = equiv_edges[edgeid];
      nodes[to].edges[equiv_edges[edgeid]].eq_eid = nodes[from].edges.size() - 1;
    }

}

void GGraph::addEdge ( GNodeID from, GNodeID to, GEdgeData edgedata, GTokenDirection direction )
{ // used in gpattern pushback
  GGraphEdge ge1, ge2;
	
  ge1.data = edgedata; 
  ge1.dest_nid = to;
  ge1.eq_eid = nodes[to].edges.size();
	
  ge2.data = edgedata;
  ge2.dest_nid = from;
  ge2.eq_eid = nodes[from].edges.size();
	
  if (direction == forward)
    {
      nodes[from].edges.push_back ( ge1 ); 
      nodes[to].edges.push_back ( ge2 );
    }
  else //backward edge: ge2 inserted at the beginning of nodes[to].edges
    {
      vector<GGraphEdge> &edges = nodes[to].edges;
      for ( uint i=0; i< edges.size(); i++)
	{ // since ge2 is inserted at beginning, other eq_eid are incremented
	  GNodeID destination = edges[i].dest_nid;
	  GEdgeID j = edges[i].eq_eid;
			
	  nodes[destination].edges[j].eq_eid++;
	}
      ge1.eq_eid = 0;
      nodes[from].edges.push_back( ge1 );
      nodes[to].edges.insert(nodes[to].edges.begin(), ge2 );
    }
}

void GGraph::delEdge ( GNodeID from, GNodeID to, GTokenDirection direction )
{ // used in gpattern
  // remove the edge (from,to). Assume this was the last added edge. 

  // this edge is at the end of the nodes[from].edges list
  nodes[from].edges.pop_back();
	
  if (direction == backward)
    { // last added backward edges is at the beginning of the "to" list
      nodes[to].edges.erase(nodes[to].edges.begin());
		
      for ( uint j=0; j< nodes[to].edges.size(); j++)
	{ //must change eq_eid of other edges of the "to" list 
	  GNodeID destination = nodes[to].edges[j].dest_nid;
	  GEdgeID i = nodes[to].edges[j].eq_eid;
	  nodes[destination].edges[i].eq_eid--;
	}
    }
  else
    // last added forward edge is at the end of the "to" list
    nodes[to].edges.pop_back();
	
}


void GGraph::read(Data *data, unsigned int *pframeid)
{ // Read one graph in the input file and return it in a GGraph object
  // pframeid is changed in this function (thus it is a pointer)
  // assume graph is empty when this function is called.
  frameid = *pframeid;
  vector<string> tokens;

  //read line of type "n" 
  data->getLine(&tokens); 
  GEdgeID numEdges = atoi(tokens[1].c_str());
  vector<GEdgeID> equiv_edges(numEdges, GNOEDGEID); //to "link" equivalent edges

  data->getLine(&tokens);
  while (tokens[0] == "v")
    {
      GNodeData nd = atoi(tokens[2].c_str()); //label of the vertex
      bool of = atoi(tokens[3].c_str()); //if it is part of the outer face
      addNode ( nd, of );
      data->getLine(&tokens);
    }	

  while (tokens[0] == "e")
    {
      GNodeID  ni1 = atoi(tokens[1].c_str()); // id from
      GNodeID  ni2 = atoi(tokens[2].c_str()); // id to
      GEdgeData ed = atoi(tokens[3].c_str()); // label edge
      GEdgeID  ied =  atoi(tokens[4].c_str()); // id of the edge

      addEdge ( ni1, ni2, ed, ied, equiv_edges );
      data->getLine(&tokens);
    } 
  //the last line read was of type "t" => change pframeid value 
  *pframeid = atoi(tokens[1].c_str());
}


ostream& operator<<(ostream& stream, GGraph &graph )
{ // print graph on stream
  int i, nb_nodes;

  nb_nodes = graph.nodes.size ();

  // output nodes
  for (i = 0; i < nb_nodes; ++i )
    stream << "v " << i << " " << 
      graph.nodes[i].data << " " << graph.nodes[i].outerface << endl;
    
  // output edges
  // to assign the same edgeid to edge i->j and j->i
  vector<vector<int> > edgeids(nb_nodes); 
  int dest_node, dest_edgeid, edge_id;
  int current_eid = 0;
  for ( i = 0; i < nb_nodes; ++i ) {
    // for each node
    vector<GGraphEdge> &edges = graph.nodes[i].edges;

    for ( int j = 0, m = edges.size(); j < m; ++j ) {
      // for each edge j from node i
      dest_node = edges[j].dest_nid;
      dest_edgeid = edges[j].eq_eid;

      if (dest_node < i) {
	// we already saw this edge
	edge_id = edgeids[dest_node][dest_edgeid];
      } else {
	// new edge
	edge_id = current_eid++;
      }
      if (dest_node == i) {
	cerr << "internal error : loop detected. ggraph << "<< endl; 
	exit(1);}

      // store current edge edge_id
      edgeids[i].push_back(edge_id);

      // output edge
      stream << "e " << i << " " << dest_node << " " << 
	edges[j].data  << " " << edge_id << endl;
    }
  }
  return stream;
}

GGraphEdge *GGraph::nextEdge(GNodeID nodeid, GEdgeID edgeid1, GEdgeID *edgeid2)
{ // return a pointer on the next edge (or NULL if no next edge)
  // edgeid2 is the id of the next edge (ie either 0 or edgeid1 + 1)
  bool isOuterFace = this->nodes[nodeid].outerface;
  vector<GGraphEdge> &edges = this->nodes[nodeid].edges;

  if (edgeid1 + 1 == (int) edges.size()) //last edge in the list of edges
    {
      if (isOuterFace)
	return NULL;
      else
	*edgeid2 = 0; 
    }
  else
    *edgeid2 = edgeid1+1;
  return &edges[*edgeid2];
}


GEdgeID GGraph::findEqEdge(GNodeID nodeid, GEdgeID edgeId)
{
  return this->nodes[nodeid].edges[edgeId].eq_eid;
}

void GGraph::updateNode(GNodeID nodeid, bool flag)
{
  this->nodes[nodeid].outerface = flag;
}
