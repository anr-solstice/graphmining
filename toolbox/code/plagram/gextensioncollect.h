/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *   Copyright (C) 2010,2011 by Adriana Prado and Baptiste Jeudy           *
 *   baptiste.jeudy at univ-st-etienne fr                                  *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef GEXTENSIONCOLLECT_H
#define GEXTENSIONCOLLECT_H


#include <map>
#include <list>
#include "global.h"
#include "gpattern.h"

class GSubGraphIso;
 
struct GExtensionData {
  GFrequency frequency; // frequency of extension: ie nb of graphs where it occurs.
  GFrequency nb_occ; // nb of occurrences of the extension
  GTid tid; // used to count the frequency correctly
  GNodeID start; //id of the activenode where the extension started 
  GNodeID stop; //id of the activenode where the extension stopped
};


class GExtensionCollect{
public:
  // for stats
  clock_t timeMap; // clock ticks to insert the last extension 
  // in the "extensions" map
  double totMap; // total clock ticks for all extensions
  
  // timeExt includes timeMap and timeValid
  clock_t timeExt; // clock ticks for the last "process" call (ie one occurrence of the pattern)
  double totExt;  // total for all "process" calls (all the occurrences of the pattern)
  
  clock_t timeValid; // clock ticks to check that the extensions of the last occurrence are valid.
  double totValid; // total clock ticks for all extensions of the pattern
  
  long long int quickpruned; // number of extensions that were quickpruned
  long long int count; // total number of extensions (including invalid ones)
  long long int countNotValid; // number ot invalid extensions
  long long int numEdges; // total number of edges of all extensions (inc. invalid ones)
  //-------------------------
  GFrequency minfreq;
  int maxgap;
  bool do_quickprune; // if true, tries to prune extensions before including them in the map
  // but in this case, we get only "pseudo-closed" patterns.

  map<vector<GToken>,GExtensionData,VGTokenLt> extensions;
  
  GExtensionCollect(GFrequency minfreq, int maxgap, bool do_qprune);
  
  ~GExtensionCollect();
  
  int process ( GSubGraphIso *subgraphiso ); 
  // called to compute all extensions of a matching
  // 3 steps : generate all extensions, validate extensions and insert them in the "extensions" map.
  // return  the number of valid extensions

  void validateListExtensions(list< pair<vector<GToken>,GExtensionData> > &lp, 
			     vector<bool> &bannedNodes, 
			     vector<GNodeID> &activenodes, 
			     int activenodesend);
  
  void print();
};

#endif
