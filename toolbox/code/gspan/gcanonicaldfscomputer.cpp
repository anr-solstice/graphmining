/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "gcanonicaldfscomputer.h"
#include <algorithm>

GCanonicalDFSComputer::GCanonicalDFSComputer( GGraph *graph ):
  graph ( graph ),
  mapstonew ( graph->nodes.size (), GNONODEID ),
  mapstoold ( graph->nodes.size (), GNONODEID )
{
}


GCanonicalDFSComputer::~GCanonicalDFSComputer()
{
}

struct GCanonicalDFSTokenLt {
  bool operator()(const GCanonicalDFSToken &t1, const GCanonicalDFSToken &t2 ) const
  {
    return cmpGToken ( t1.token, t2.token ) < 0;
     // the order of the nodeid does not matter.
  }
};

void GCanonicalDFSComputer::recurse2 ( GNodeID parentid, GNodeID nodeid ) {  
  int stacksizehere = stack.size ();
  mapstonew[nodeid] = nrnodes;
  mapstoold[nrnodes] = nodeid;
  
  vector<GGraphEdge> &edges = graph->nodes[nodeid].edges;
  
  for ( int i = 0, size = edges.size (); i < size; ++i ) {
    GNodeID tonodeid = edges[i].nodeid;
    if ( tonodeid != parentid ) {
      GCanonicalDFSToken t;
      t.token.from = mapstonew[nodeid];
      t.token.fromdata = graph->nodes[nodeid].data;
      t.token.todata = graph->nodes[tonodeid].data;
      t.token.data = edges[i].data;
      if ( mapstonew[tonodeid] == GNONODEID ) {
        t.token.to = GNONODEID;
        t.token.direction = forward;
        t.nodeid = tonodeid;
        stack.push_back ( t );
      }
      else {
        t.token.to = mapstonew[tonodeid];
        t.token.direction = backward;
        t.nodeid = tonodeid;
        if ( t.token.to < t.token.from ) 
          stack.push_back ( t );
      }
    }
  }
  if ( stacksizehere != stack.size () )
    sort ( stack.begin () + stacksizehere, stack.end (), GCanonicalDFSTokenLt () );
    
  ++nrnodes;
  recurse ();
  --nrnodes;
  
  mapstonew[nodeid] = GNONODEID;
  stack.resize ( stacksizehere ); 
}

void GCanonicalDFSComputer::recurse () {
  ++pos;
  vector<GCanonicalDFSToken> undostack;
  GCanonicalDFSToken st;
  int i;
  GCanonicalDFSToken *t;
  
  if ( stack.empty () ) 
    goto cleanup;
      // we need to clean up before leaving this function
      // I did not want to write a separate class to perform the clean up
      // in a destructor. This feels more "light weight"
  
  t = &stack.back ();
  
  while ( t->token.direction == forward && mapstonew[t->nodeid] != GNONODEID ) {
    undostack.push_back ( *t );
    stack.pop_back ();
    if ( stack.empty () )
      goto cleanup;
    t = &stack.back ();
  }
  
  int cmp;
  if ( t->token.direction == forward ) {
    t->token.to = nrnodes;
    cmp = cmpGToken ( code->tokens[pos], t->token );
    t->token.to = GNONODEID;
  }
  else
    cmp = cmpGToken ( code->tokens[pos], t->token );
    
  if ( cmp < 0 )  {
    stop = true;
    goto cleanup;
  }
  else 
    if ( cmp > 0 )
      goto cleanup;
    
      
  st = stack.back ();
  stack.pop_back ();
  
  if ( t->token.direction == forward ) {
  
    recurse2 ( mapstoold[st.token.from], st.nodeid );
    if ( stop ) 
      goto cleanup;
          
    i = stack.size () - 1;
    while ( i >= 0 && cmpGToken ( stack[i].token, st.token ) == 0 ) {
      if ( mapstonew[stack[i].nodeid] == GNONODEID ) {
        swap ( stack[i].nodeid, st.nodeid );
        recurse2 ( mapstoold[st.token.from], st.nodeid );
        swap ( stack[i].nodeid, st.nodeid );
        if (  stop ) 
          goto cleanup;
      }
      --i;
    }
  }
  else {
    recurse ();
    if ( stop )
      goto cleanup;
  }
      
  stack.push_back ( st );
  
cleanup:
  while ( !undostack.empty () ) {
    stack.push_back ( undostack.back () );
    undostack.pop_back ();
  }
  
  --pos;
}

bool GCanonicalDFSComputer::isCanonical ( GDFSCode *code ) {
  nrnodes = 0;
  pos = -1;
  stop = false;
  this->code = code;
  for ( int i = 0, size = graph->nodes.size (); i < size; ++i ) 
    if ( graph->nodes[i].data == code->tokens[0].fromdata ) {
      recurse2 ( GNONODEID, i );
      if ( stop )  
        return false;
    }
  return true;
}
