/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "gextensioncollect.h"
#include "gsubgraphiso.h"

GExtensionCollect::GExtensionCollect()
{
     count = 0;
     totMap = totExt = 0;
}


GExtensionCollect::~GExtensionCollect()
{
}

/*!
    \fn GExtensionCollect::process ( GSubGraphIso *subgraphiso )
 */
bool GExtensionCollect::process ( GSubGraphIso *subgraphiso )
{

  timeExt = clock();

  GSearchCode *searchcode = ((GSearchCode*) subgraphiso->smallgraph->code);
  vector<GNodeID> &activenodes = searchcode->activenodes;
  vector<GGraphNode> &smallnodes = subgraphiso->smallgraph->nodes,
                     &largenodes = subgraphiso->largegraph->nodes;
  
  for ( int i = searchcode->activenodesbegin; i < searchcode->activenodesend; ++i ) 
  {
  
    GGraphNode &smallnode = smallnodes[activenodes[i]];
    GGraphNode &largenode = largenodes[subgraphiso->smallgraphused[activenodes[i]]];
    
    // initialize extension tuple as much as possible here
    pair<GToken,GExtensionData> p;
    p.first.fromdata = smallnode.data;
    p.first.from = activenodes[i];
    
    // scan all possible extensions
    vector<GGraphEdge> &edges = largenode.edges;
    for ( int j = 0, size2 = edges.size (); j < size2; ++j )
    {
      
      /* SLOW: map<GNodeID,GNodeID>::iterator largegraphused_it = subgraphiso->largegraphused.find ( edges[j].nodeid ); */
      
      GNodeID largegraphused_it = subgraphiso->largegraphused[edges[j].nodeid];
      
      p.first.data = edges[j].data;
      p.first.todata = subgraphiso->largegraph->nodes[edges[j].nodeid].data;
      
      /* SLOW: if ( largegraphused_it != subgraphiso->largegraphused.end () ) { */
      if ( largegraphused_it != GNONODEID ) {
        // a cycle?
        /* SLOW: p.first.to = largegraphused_it->second; */
        p.first.to = largegraphused_it;
        p.first.direction = backward;
        if ( searchcode->quickPruneBackward ( p.first ) )
          continue; 
      }
      else {
        p.first.to = subgraphiso->smallgraph->nodes.size ();
        p.first.direction = forward;
        if ( searchcode->quickPruneForward ( p.first ) ) 
          continue;
      }
     
     count++; 
     timeMap = clock();
     
      // search in the extensions already determined
      pair<map<GToken,GExtensionData,GTokenLt>::iterator,bool> f = extensions.insert ( p );
      
      if ( f.second ) {
        // a new extension
        f.first->second.frequency = 1;
        f.first->second.tid = (GTid) subgraphiso->largegraph; 
      }
      else
        // an existing extension, check if found in the same large graph
        if ( f.first->second.tid != (GTid) subgraphiso->largegraph ) {
          ++f.first->second.frequency;
          f.first->second.tid = (GTid) subgraphiso->largegraph; 
        } 

      timeMap = clock() - timeMap;
      totMap += timeMap;

    }
  }
  
  timeExt = clock() - timeExt;
  totExt += timeExt;
  
  return false;
}
