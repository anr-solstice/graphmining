/***************************************************************************
 *   Copyright (C) 2010,2011 by Adriana Prado and Baptiste Jeudy           *
 *   baptiste.jeudy at univ-st-etienne fr                                  *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include "data.h"

Data::Data(const string& fn) 
{
        in.open(fn.c_str(), ifstream::in);

	if (!in.is_open()){
		cerr << "Error opening input file " << fn << endl;
		exit(1);
	}
	out = 0;
	line_nb = 0; //baptiste
}


Data::Data(const string& fn, ostream *stream) : out(stream)
{
        in.open(fn.c_str(), ifstream::in);
	if (!in.is_open()){
		cerr << "Error opening input file " << fn << endl;
		exit(1);
	}
	line_nb = 0; //baptiste
}



Data::~Data()
{ 
    in.close() ;
}

void Data::getLine(vector<string>* tokens)
{
  const int s = 2048;
  char line[s];
  tokens->clear();
  do {
    do {
      in.getline(line, s);
      if (in.fail() && !in.eof() ) {
	cerr << "Error input line "<< line_nb << " (line too long ?)" << endl;
	cerr << "Max line length: " << s << endl;
	exit (1);
      }
      line_nb++;
      if (line[0] == '#') 
	{ 
	  if (out)
	    *out << line << endl;
	}
    } while (line[0] == '#');
    
    char* token = NULL;
    char* ptr = line;
    
    token = strtok(ptr, " ");
    while (token != NULL)
      {
	tokens->push_back(token);
	token = strtok(NULL, " ");
      }
  } while (tokens->size() == 0 && !in.eof());
}

bool Data::finished()
{
	return in.eof();
}

int Data::get_line_nb()
{
  return line_nb;
}
