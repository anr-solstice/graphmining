function [ result ] = hists(conf, set, filename)
%% Function that create histogram from BOW by knnsearch 
%
%Syntaxe
% [ result ] = hist(conf, set)
%
%Parameters
% All parameter are required
% - Conf (required)
%       -> Configuration struct. Cf config parfor more information.
% TODO : Complete this documentation
% - Set (required)
% -> Sub structure from conf with a leas this information : 
%       Set.imgIdx : Index of all images
%       Set.nbImg : Number of image in the set.
% - filename
%       -> Filename where clocks stats will be written
%
%Description
% =========================================================================
% !!!!!!!!!                      TODO                             !!!!!!!!!
% =========================================================================

%% Compute Hist ===========================================================
if ( nargin == 3 )
    temp.fileId = fopen(sprintf('%s',filename),'a');
end

tmp.neighbor = {} ;
tmp.dist = {} ;
tmp.frame = {} ;


 
if strcmp(conf.method,'grid') 
    if strcmp('knn', conf.hist.method ) 
        if nargin == 3 
            [ tmp.hist , tmp.clust , ~ , ~ , tmp.frame ] = knn(conf,set,temp.fileId) ;
        else
            [ tmp.hist , tmp.clust , ~ , ~ , tmp.frame ] = knn(conf,set) ;
        end
    else
        if nargin == 3 
            [ tmp.hist , tmp.clust , ~ , ~ , tmp.frame ] = kdtree(conf,set,temp.fileId) ;
        else
            [ tmp.hist , tmp.clust , ~ , ~ , tmp.frame ] = kdtree(conf,set) ;
        end
    end
elseif strcmp(conf.method,'sift')
    if strcmp('knn', conf.hist.method ) 
        if nargin == 3 
            [ tmp.hist , tmp.clust , tmp.neighbor , tmp.dist , tmp.frame ] = knn(conf,set,temp.fileId) ;
        else
            [ tmp.hist , tmp.clust , tmp.neighbor , tmp.dist , tmp.frame ] = knn(conf,set) ;
        end
    else
        if nargin == 3 
            [ tmp.hist , tmp.clust , tmp.neighbor , tmp.dist , tmp.frame ] = kdtree(conf,set,temp.fileId) ;
        else
            [ tmp.hist , tmp.clust , tmp.neighbor , tmp.dist , tmp.frame ] = kdtree(conf,set) ;
        end
    end
    result.neighbor = tmp.neighbor;
    % Distance discretization (WIP)
    tmp.adist = vl_colsubset(cat(1, tmp.dist{:}), inf);
    tmp.adist = vl_colsubset(cat(2, tmp.adist(:)),inf);
    tmp.adist = sort(tmp.adist) ;
    tmp.adist = tmp.adist(find(tmp.adist == 0 , 1, 'last' ):end,:);
    tmp.lim(1) = 0;
    for i=1:conf.sift.kneighbor
        tmp.lim(i+1) = tmp.adist(round(size(tmp.adist,1)/conf.sift.kneighbor*i));
    end
    for i=1:size(tmp.dist,2)
        for j=1:conf.sift.kneighbor
            tmp.dist{i}(tmp.dist{i} > tmp.lim(j) & tmp.dist{i} < tmp.lim(j+1))=j;
        end
    end
    result.dist = tmp.dist;
end

result.frame = tmp.frame;
result.hknnclust = tmp.clust;
result.hknncell = tmp.hist ;
result.histsknn = cat(2,tmp.hist{:}) ;
result.kern = vl_homkermap(result.histsknn, 1, 'kchi2', 'gamma', .5) ;        

fclose(temp.fileId);

end % End of main function

% =========================================================================
%% Subfunction clusterize with knn ========================================
function [ hist , clust , neighbor , dist , frame ] = knn(conf,set,fileId)
tmpd = {}  ; % Temporary descriptor
tmpdc = {} ; % Temporary descriptor clustered
tmpk = {}  ; % Temporary frame and/or KNN
tmpkd = {} ; % Temporary distance for KNN
tmpf = {} ;  % Temporary frame coord
cinit = clock;

fprintf('# HIST - IMG ==============================\n');
if strcmp(conf.method, 'grid')
    % for i=1:set.nbImg     
    parfor i=1:set.nbImg
        c0 = clock;
        if strcmp(conf.setname, 'Simplicity')
            imc=imread(sprintf('%s%d.jpg', set.basedirimg, set.imgIdx(i)));
        else
            imc=imread(sprintf('%s%06d.jpg', set.basedirimg, set.imgIdx(i)));
        end
        % Convert into gray image
        imb = single(rgb2gray(imc)) ;
        % Applying dsift
        [ f , tmpd{i} ] = vl_dsift(imb,'size', conf.dsift.binSize ,...
                                       'step', conf.dsift.step);
        w = size(unique(f(1,:)),2);
        h = size(unique(f(2,:)),2);
        tmpf{i}=[w h];
        c1 = clock;
        % Applying 1nn search parfor grid clustering
        tmpdc{i} = knnsearch(conf.vocab.vocab', single(tmpd{i}'));
        tmpd{i} = histc(tmpdc{i},1:conf.vocab.nbwords);
        c2 = clock;
        fprintf('vl_dsift,%03d.jpg,%d\n',set.imgIdx(i),etime(c1,c0));
        fprintf('knn,%03d.jpg,%d \n', set.imgIdx(i), etime(c2,c1));
        fprintf('hist,%d.jpg,%d \n', set.imgIdx(i), etime(c2,c0));
        c(i)=etime(c2,c0);
    end
else
    % for i=1:set.nbImg
    parfor i=1:set.nbImg
        c0 = clock;
        if strcmp(conf.setname,'Simplicity')
            imc=imread(sprintf('%s%d.jpg', set.basedirimg, set.imgIdx(i)));
        else
            imc=imread(sprintf('%s%06d.jpg', set.basedirimg, set.imgIdx(i)));
        end
        % Convert into gray image
        imb = single(rgb2gray(imc)) ;
        % Applying dsift
        [ f , tmpd{i} ] = vl_sift(imb);
        c1 = clock;
        % Applying 1nn search parfor grid clustering
        tmpdc{i} = knnsearch(conf.vocab.vocab', single(tmpd{i}'));
        tmpd{i} = histc(tmpdc{i},1:conf.vocab.nbwords);
        c2 = clock;
        % Juste take x and y coord
        f = f(1:2,:)';
        % K+1 Cause first neighbor is same point.
        [ k , d ] = knnsearch(f,f,...
                            'k', conf.sift.kneighbor+1,... 
                            'distance', conf.sift.dist);
        % Because knn search from X to X, it automatically find X(i) as one
        % of the NN, so, I find this X(i), swap it with the first column
        % and drop the first column.
        for j=1:size(k,1)
            idx = find(k(j,:)==j);
            if idx~=1
                k(j,idx)=k(j,1);
                d(j,idx)=d(j,1);
            end
        end
        k = k(:,2:end); 
        d = d(:,2:end)/sqrt(size(imc,1)*size(imb,2));
        % =================================================================
        % Code for showing image with graph 
        % image(imc);
        % hold on;
        % vl_plotframe(f')
        % for j=1:size(f,1)
        %     for l=1:size(k,2)-1
        %         plot([f(j,1),f(k(j,l),1)],[f(j,2),f(k(j,l),2)],...
        %               'Color','r','LineWidth',0.5);
        %     end
        % end
        % hold off;
        tmpf{i} = f;
        tmpk{i} = k;
        tmpkd{i} = d;
        c3 = clock ;
        fprintf('vl_sift,%03d.jpg,%d\n',set.imgIdx(i),etime(c1,c0));
        fprintf('knn,%03d.jpg,%d \n', set.imgIdx(i), etime(c2,c1));
        fprintf('hist,%d.jpg,%d \n', set.imgIdx(i), etime(c2,c0));
        fprintf('knn-graph,%d.jpg,%d \n', set.imgIdx(i), etime(c3,c0));
        c(i)=etime(c3,c0);
    end
end

cend = clock;
if ( nargin == 3 )
    fprintf(fileId,'%f,', etime(cend,cinit));
else
    fprintf('# HIST - KNN ============================\n');
    fprintf('histograms,%d,%d \n', etime(cend,cinit), mean(c));
end
frame = tmpf ;
hist = tmpd;
clust = tmpdc;
neighbor = tmpk;
dist = tmpkd ;
end

%% Subfunction clusterize with kdtree =====================================
function [ hist , clust , neighbor , dist , frame ] = kdtree(conf,set,fileId)
tmpd = {}  ; % Temporary descriptor
tmpdc = {} ; % Temporary descriptor clustered
tmpk = {}  ; % Temporary frame
tmpkd = {} ; % Temporary distance for KNN
tmpf = {} ;  % Temporary frame coord

cinit = clock;

fprintf('# HIST - IMG ==============================\n');
if strcmp(conf.method,'grid')
    % for i=1:set.nbImg
    parfor i=1:set.nbImg
        c0 = clock;
        if strcmp(conf.setname, 'Simplicity')
            imc=imread(sprintf('%s%d.jpg', set.basedirimg, set.imgIdx(i)));
        else 
            imc=imread(sprintf('%s%06d.jpg', set.basedirimg, set.imgIdx(i)));
        end
        % Convert into gray image
        imb = single(rgb2gray(imc)) ;
        % Applying dsift
        [ f , tmpd{i} ] = vl_dsift(imb,'size', conf.dsift.binSize ,...
                                       'step', conf.dsift.step);
        w = unique(f(1,:));
        h = unique(f(2,:));
        tmpf{i}=[w h];
        c1 = clock;
        tmpdc{i} = vl_kdtreequery(conf.vocab.kdtree, ...
                                  conf.vocab.vocab, ...
                                  single(tmpd{i}), ...
                                  'MaxComparisons', 50);
        tmpd{i} = histc(double(tmpdc{i})) ;
        c2 = clock;
        fprintf('vl_sift,%03d.jpg,%d\n',set.imgIdx(i),etime(c1,c0));
        fprintf('kdtree,%03d.jpg,%d \n', set.imgIdx(i), etime(c2,c1));
        fprintf('hist,%d.jpg,%d \n', set.imgIdx(i), etime(c2,c0));
        c(i)=etime(c2,c0);
    end
else
    % for i=1:set.nbImg
    parfor i=1:set.nbImg
        c0 = clock;
        if strcmp(conf.setname, 'Simplicity')
            imc=imread(sprintf('%s%d.jpg', set.basedirimg, set.imgIdx(i)));
        else 
            imc=imread(sprintf('%s%06d.jpg', set.basedirmg, set.imgIdx(i)));
        end
        % Convert into gray image
        imb = single(rgb2gray(imc)) ;
        % Applying dsift
        [ f , tmpd{i} ] = vl_sift(imb)
        c1 = clock;
        tmpdc{i} = vl_kdtreequery(conf.vocab.kdtree, ...
                                  conf.vocab.vocab, ...
                                  single(tmpd{i}), ...
                                  'MaxComparisons', 50);
        tmpd{i} = histc(double(tmpdc{i})) ;
        c2 = clock;
        % Juste take x and y coord
        f = f(1:2,:)';
        % K+1 Cause first neighbor is same point.
        [ k , d ] = knnsearch(f,f,...
                            'k', conf.sift.kneighbor+1,... 
                            'distance', conf.sift.dist);
        % Because knn search from X to X, it automatically find X(i) as one
        % of the NN, so, I find this X(i), swap it with the first column
        % and drop the first column.
        for j=1:size(k,1)
            idx = find(k(j,:)==j);
            if idx~=1
                k(j,idx)=k(j,1);
                d(j,idx)=d(j,1);
            end
        end
        k = k(:,2:end); 
        d = d(:,2:end);
        % =================================================================
        % Code for showing image with graph 
        % image(imc);
        % hold on;
        % vl_plotframe(f')
        % for j=1:size(f,1)
        %     for l=1:size(k,2)-1
        %         plot([f(j,1),f(k(j,l),1)],[f(j,2),f(k(j,l),2)],...
        %               'Color','r','LineWidth',0.5);
        %     end
        % end
        % hold off;
        tmpf{i} = f;
        tmpk{i} = k;
        tmpkd{i} = d;
        c3 = clock ;
        fprintf('vl_sift,%03d.jpg,%d\n',set.imgIdx(i),etime(c1,c0));
        fprintf('kdtree,%03d.jpg,%d \n', set.imgIdx(i), etime(c2,c1));
        fprintf('hist,%d.jpg,%d \n', set.imgIdx(i), etime(c2,c0));
        fprintf('knn-graph,%d.jpg,%d \n', set.imgIdx(i), etime(c3,c0));
        c(i)=etime(c3,c0);
    end
end
cend = clock;


if ( nargin == 3 )
    fprintf(fileId,'%f,', etime(cend,cinit));
else
    fprintf('# HIST - KNN ============================\n');
    fprintf('histograms,%d,%d \n', etime(cend,cinit), mean(c));
end
frame = tmpf;
hist = tmpd;
clust = tmpdc;
neighbor = tmpk;
dist = tmpkd ;
end % End of function
