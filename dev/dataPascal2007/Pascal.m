function [ result ] = Pascal(param)
%% Pascal Grid ========================================================
% Function for applying dense sift to Pascal image set
%
%% Syntaxe 
% [ result ] = Pascal()
% [ result ] = Pascal(parameter)
%
%% Parameters 
% If not specified, parameter are set to default value, specified into
% squared brackets.
% Parameters must be in the exact defined order
% - parameter (REQUIRED)
%       -> Parameter structure as construct at the beginning of
%       PascalGeneration
% 
%% Parameter Structure 
% Structure of input paramter, default value in []
% HISTOGRAM PARAMETERS ====================================================
% - tmp.hist.method  [ 'knn' ] 
%        => Method for clustering desc. ('knn, kdtree')
%
% % DSIFT PARAMETERS ======================================================
% - tmp.dsift.binSize [ 3 ] 
%        =>  Bin size, in px, for dsift
% - tmp.dsift.step [ 1 ]
%        => Step, in px, for dsift between two desc
% 
% % SIFT PARAMETERS  ======================================================
% - tmp.sift.kneighbor [ 2 ]
%        => Number of neighbor for proximity graph
% - tmp.sift.dist [ 'euclidean' ] 
%        => Kind of distance to measure for prox graph
%        -> Possible distance : euclidean, cityblock, chebychev, minkowski
% 
% % LIBSVM PARAMETERS =====================================================
% - tmp.svm.T [ 0 ] 
%        => SVM Kernel
%        -> 0 = linear                : u'*v
%        -> 1 = polynomial            : (gamma*u'*v + coef0)^degree
%        -> 2 = radial basis function : exp(-gamma*|u-v|^2) 
%        -> 3 = sigmoid               : tanh(gamma*u'*v + coef0)
% - tmp.svm.S [ 0 ] 
%        => SVM type 
%        -> 0 = C-SVC
%        -> 1 = nu-SVC
%        -> 2 = one-class SVM
%        -> 3 = epsilon-SVR
%        -> 4 = nu-SVR
% - tmp.svm.C [ 1 ]
%        => Cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR
% - tmp.svm.G [ 4 ]
%        => Gamma : set gamma in kernel function (default 1/num_features)
% - tmp.svm.P [ 0.1 ]
%        => Epsilon : set the epsilon in loss function of epsilon-SVR 
% 
% % VOCAB PARAMETERS ======================================================
% - tmp.vocab.method [ 'grid' ]
%        => Method construction for vocab 
%        -> Possible value : grid, sift
% - tmp.vocab.nbwords [ 500 ]
%        -> Number of words for vocab.
% 
% % GLOBAL METHOD PARAMETERS ==============================================
% - tmp.method [ 'grid' ]
%        =>General computation method 
%        -> Possible value : grid, sift
%        -> Useful to know if we need frame matrix or not.
%
%Description
% =========================================================================
% !!!!!!!!!                      TODO                             !!!!!!!!!
% =========================================================================

%% CONSTANTES
const.hist.method = { 'knn', 'kdtree' } ;
const.sift.dist = { 'euclidean', 'cityblock', 'chebychev', 'minkowski' } ;
const.method = { 'grid', 'sift' } ;

% =========================================================================
%% Configurable variable ==================================================
% Setting parameter, see description above.
if nargin == 1
    conf = setparam(param) ;
else
    conf = setdefault ;
end

% =========================================================================
%% Configuration ==========================================================
% Base directory for training (Image and Set)
conf.train.basedirimg = 'pascal_dataset/VOCtrainval_06-Nov-2007/VOC2007/JPEGImages/';
conf.val.basedirimg = 'pascal_dataset/VOCtest_06-Nov-2007/VOC2007/JPEGImages/';
% Base directory for testing (Image and Set)
conf.train.basedirset = 'pascal_dataset/VOCtrainval_06-Nov-2007/VOC2007/ImageSets/Main/';
conf.val.basedirset = 'pascal_dataset/VOCtest_06-Nov-2007/VOC2007/ImageSets/Main/';
% List of categoy
conf.class = {
    'aeroplane'   ; 'bicycle'     ; 'bird'        ; 'boat'        ;
    'bottle'      ; 'bus'         ; 'car'         ; 'cat'         ;
    'chair'       ; 'cow'         ; 'diningtable' ; 'dog'         ;
    'horse'       ; 'motorbike'   ; 'person'      ; 'pottedplant' ; 
    'sheep'       ; 'sofa'        ; 'train'       ; 'tvmonitor'  };
% Number of class
conf.nbclasse = size(conf.class,1);





% =========================================================================
%% Init image data ========================================================
% Kind of set for Pascal
%   -Train : Training dataset
%   -Val   : Validation training dataset (suggested)
%   -Full  : Uniont of Train and Val

conf.train.img = dlmread(sprintf('%strainval.txt',...
                                            char(conf.train.basedirset)));
conf.train.img = conf.train.img(1:100,:);                                         
conf.train.imgIdx = conf.train.img(:,1);
conf.train.nbImg = size(conf.train.img,1);
conf.train.imgClass = [] ;

conf.val.img = dlmread(sprintf('%stest.txt',...
                                            char(conf.val.basedirset)));
conf.val.img = conf.val.img(1:100,:) ;                                   
conf.val.imgIdx = conf.val.img(:,1);
conf.val.nbImg = size(conf.val.img,1);
conf.val.imgClass = {} ;

for i=1:conf.nbclasse
    pos = dlmread(sprintf('%s%s_trainval.txt',...
                            char(conf.train.basedirset),...
                            char(conf.class{i})));
    posidx=(1==pos(:,2));
    conf.train.imgClass{i}=posidx;
    pos = dlmread(sprintf('%s%s_test.txt',...
                            char(conf.val.basedirset),...
                            char(conf.class{i})));
    posidx=(1==pos(:,2));
    conf.data.val.imgClass{i}=posidx;
end


for i=1:conf.nbclasse
    pos = dlmread(sprintf('%s%s_trainval.txt',...
                            char(conf.train.basedirset),...
                            char(conf.class{i})));
    posidx=find(1==pos(:,2));
    conf.train.imgClassAll(posidx)=i;
    pos = dlmread(sprintf('%s%s_test.txt',...
                            char(conf.val.basedirset),...
                            char(conf.class{i})));
    posidx=find(1==pos(:,2));
    conf.val.imgClassAll(posidx)=i;
end


% =========================================================================
%% Create file with stats =================================================
if strcmp(conf.method,'sift')
    tmp.fID = fopen(sprintf('data/Pascal-K%dD%sW%s%d-Stat.txt',...
                    conf.sift.kneighbor, conf.sift.dist(1:2),...
                    conf.vocab.method(1:1),conf.vocab.nbwords),'w');
else strcmp(conf.method,'grid')
    tmp.fID = fopen(sprintf('data/Pascal-B%dS%dW%s%d-Stat.txt',...
                    conf.dsift.binSize, conf.dsift.step,...
                    conf.vocab.method(1:1),conf.vocab.nbwords),'w');

end     
tmp.filename = fopen(tmp.fID);

%% Construct vocab ========================================================
% Test General Vocab Function 
[ conf.vocab.vocab , conf.vocab.kdtree ] = vocabPascal(conf, conf.train.nbImg, tmp.filename);
 
% =========================================================================
%% Compute Hist for Train img =============================================
train = hists(conf,conf.train, tmp.filename);  
% Putting lim in conf
% conf.sift.disclim = train.disclim;


% =========================================================================
%% Compute Hist for validation img ========================================
val = hists(conf,conf.val, tmp.filename);  
 
% =========================================================================
% Training & Testing SVM =================================================
tmp.fID = fopen(tmp.filename,'a'); 
fprintf(tmp.fID,'# SVM ============================\n');
fprintf(tmp.fID,'# SVM - TRAIN ====================\n');
cinit = clock ;
svm.model = ovrtrain(conf.train.imgClassAll', train.histsknn',...
            sprintf('-s %f -t %f -g %f -c %f -p %f',...
            conf.svm.t, conf.svm.s, conf.svm.g,...
            conf.svm.c, single(conf.svm.p)));
cend = clock ; 
fprintf(tmp.fID,'Training SVM,%d, \n', etime(cend,cinit));
 
fprintf(tmp.fID,'# SVM - TEST =====================\n');
[svm.res.pred svm.res.ac svm.res.decv] = ovrpredict(conf.val.imgClassAll',...
                      val.histsknn', svm.model);
fprintf(tmp.fID,'Testing SVM,%d, \n', etime(cend,cinit));
        
% Compute the confusion matrix
idx = sub2ind([conf.nbclasse, conf.nbclasse], ...
              conf.val.imgClass, svm.res.pred) ;
confus = zeros(conf.nbclasse) ;
svm.res.confus = vl_binsum(confus, ones(size(idx)), idx) ;
svm.res.mean = 1000 * mean(diag(svm.res.confus)/conf.val.nbImg) ;
fprintf(tmp.fID,'# SVM - RESULTAT ==================\n');
fprintf(tmp.fID,'Result LibSVM,%d,',svm.res.mean);

% % Plots
% h=figure(1) ; clf;
% imagesc(svm.res.confus) ;
% title(sprintf('Confusion matrix (%.2f %% accuracy)', ...
%               1000 * mean(diag(svm.res.confus)/conf.val.nbImg) )) ;
% if strcmp(conf.method,'sift')
%     saveas(h,sprintf('data/Pascal-K%dD%sW%s%d-Stat.txt',...
%                     conf.sift.kneighbor, conf.sift.dist(1:2),...
%                     conf.vocab.method(1:1),conf.vocab.nbwords),'jpg');
% else strcmp(conf.method,'grid')
%     saveas(h,sprintf('data/Pascal-B%dS%dW%s%d-Stat.txt',...
%                     conf.dsift.binSize, conf.dsift.step,...
%                     conf.vocab.method(1:1),conf.vocab.nbwords),'jpg');
% end     
% =========================================================================
%% Write weka file for 
% fprintf(tmp.fID, '# WEKA ========================\n');
% [ tmp.errmsg ] = weka(train.histsknn, conf.class,...
%                       conf.train.img,sprintf('%s',tmp.filename));
% fprintf(tmp.fID,'%s', tmp.errmsg);
% [ tmp.errmsg ] = weka (val.histsknn, conf.class,...
%                        conf.val.img,sprintf('%s',tmp.filename));
% fprintf(tmp.fID,'%s', tmp.errmsg);
% fclose(tmp.fID);
 
result.conf = conf;
result.train = train;
result.val = val;
result.svm = svm ;

    function [ conf ] = setparam(param) 
    %% Subfunction that set value send in param if exists, else set default
    
    % HISTOGRAM PARAMETERS ================================================
    % Check if field exist and is a correct value
    if isfield(param, 'hist') && isfield(param.hist, 'method') ...
                              && string('hist',param)
        conf.hist.method = param.hist.method; 
    else
        conf.hist.method = 'knn';
    end
    % % % Check Parameter
    % conf.hist

    % DSIFT PARAMETERS ====================================================
    if isfield(param,'dsift')
        % Last condition to test if variable are integer value
        if isfield(param.dsift,'binSize') && isnumeric(param.dsift.binSize)
            conf.dsift.binSize = param.dsift.binSize;
        else
            conf.dsift.binSize = 3 ;
        end
        if isfield(param.dsift,'step') && isnumeric(param.dsift.step)
            conf.dsift.step = param.dsift.step;
        else
            conf.dsift.step = 1 ;
        end
    else
        conf.dsift.binSize = 3 ;
        conf.dsift.step = 1 ; 
    end
    % % % Check Parameter
    % conf.dsift

    % SIFT PARAMETERS  ====================================================
    if isfield(param,'sift')
        % Last condition to test if variable are integer value
        if isfield(param.sift,'kneighbor') ...
        && isnumeric(param.sift.kneighbor)
            conf.sift.kneighbor = param.sift.kneighbor;
        else
            conf.sift.kneighbor = 2 ; 
        end
        if isfield(param.sift,'dist') && ischar(param.sift.dist) ...
                                      && string('dist',param)
            conf.sift.dist = param.sift.dist;
        else
            conf.sift.dist = 'euclidean';
        end
    else
        conf.sift.kneighbor = 2 ; 
        conf.sift.dist = 'euclidean';
    end
    % % % Check Parameter
    % conf.sift

    % LIBSVM PARAMETERS  ==================================================
    if isfield(param,'svm')
        % Last condition to test if variable are integer value
        if isfield(param.svm,'t') && isnumeric(param.svm.t)
            conf.svm.t = param.svm.t ;
        else
            conf.svm.t = 0 ; 
        end
        if isfield(param.svm,'s') && isnumeric(param.svm.s)
            conf.svm.s = param.svm.s ;
        else
            conf.svm.s = 0 ; 
        end
        if isfield(param.svm,'c') && isnumeric(param.svm.c)
            conf.svm.c = param.svm.c ;
        else
            conf.svm.c = 1 ; 
        end
        if isfield(param.svm,'g') && isnumeric(param.svm.g)
            conf.svm.g = param.svm.g ;
        else
            conf.svm.g = 4 ; 
        end
       if isfield(param.svm,'p') && isnumeric(param.svm.p)
            conf.svm.p = param.svm.p ;
        else
            conf.svm.p = 0.1 ; 
        end
    else
        conf.svm.t = 0 ; 
        conf.svm.s = 0 ; 
        conf.svm.c = 1 ;
        conf.svm.g = 4 ;
        conf.svm.p = 0.1 ;
    end
    % % % Check Parameter
    % conf.svm

    % VOCAB PARAMETERS  ===================================================
    if isfield(param,'vocab')
        if isfield(param.vocab,'method') && string('vocab',param)
            conf.vocab.method = param.vocab.method;
        else
            conf.vocab.method = 'grid';
        end
        if isfield(param.vocab,'nbwords') && isnumeric(param.vocab.nbwords)
            conf.vocab.nbwords = param.vocab.nbwords;
        else
            conf.vocab.nbwords = 500 ;
        end
    else
        conf.vocab.method = 'grid';
        conf.vocab.nbwords = 500 ;
    end
    % % % Check Parameter
    % conf.vocab

    % GLOBAL METHOD PARAMETERS ============================================
    if isfield(param,'method') && string('method',param)
        conf.method = param.method ;
    else
        conf.method = 'grid' ;
    end
    % % % Check Parameter
    % conf.method
    end

    function [ conf ] = setdefault
    %% Subunction that set all default value
    
    % HISTOGRAM PARAMETERS ================================================
    conf.hist.method = 'knn';
    % % % Check Parameter
    % conf.hist

    % DSIFT PARAMETERS ====================================================
    conf.dsift.binSize = 3 ;
    conf.dsift.step = 1 ; 
    % % % Check Parameter
    % conf.dsift

    % SIFT PARAMETERS  ====================================================
    conf.sift.kneighbor = 2 ; 
    conf.sift.dist = 'euclidean';
    % % % Check Parameter
    % conf.sift

    % LIBSVM PARAMETERS  ==================================================
    conf.svm.t = 0 ; 
    conf.svm.s = 0 ; 
    conf.svm.c = 1 ;
    conf.svm.g = 4 ;
    conf.svm.p = 0.1 ;
    % % % Check Parameter
    % conf.svm

    % VOCAB PARAMETERS  ===================================================
    conf.vocab.method = 'grid';
    conf.vocab.nbwords = 500 ;
    % % % Check Parameter
    % conf.vocab

    % GLOBAL METHOD PARAMETERS ============================================
    conf.method = 'grid' ;
    % % % Check Parameter
    % conf.method
    end

    function [ r ] = string(var, param)
    %% Subfunction that return check 
    switch var 
        case 'hist'
            r = ~isempty(intersect(const.hist.method,param.hist.method));
        case 'dist'
            r = ~isempty(intersect(const.sift.dist,param.sift.dist));
        case 'vocab'
            r = ~isempty(intersect(const.method,param.vocab.method));
        case 'method'
            r = ~isempty(intersect(const.method,param.method));
    end
    end
end % End of function
