function [ conf ] = config()
%% Function that initialize configuration
%
% Loading vlfeat for matlab
%run('vl_setup');

% Don't forget to include the following folder to the matlab path : 
% - PFE-fouille de Graphe
% - pascal_dataset : Wich contain the following folder :
%                   - VOCtrainval_06-Nov-2007
%                   - VOCtest_06-Nov-2007
%                   - VOCdevkit_08-Jun-2007
                    
% =========================================================================
%% Configuration ==========================================================
% Base directory for training (Image and Set)
conf.trainval.basedirimg = 'pascal_dataset-tiny/VOCtrainval_06-Nov-2007/VOC2007/JPEGImages/';
conf.test.basedirimg = 'pascal_dataset-tiny/VOCtest_06-Nov-2007/VOC2007/JPEGImages/';
% Base directory for testing (Image and Set)
conf.basedirset = 'dev/pascal_desc/';
% List of categoy
conf.class = {
    'aeroplane'   ; 'bicycle'     ; 'bird'        ; 'boat'        ;
    'bottle'      ; 'bus'         ; 'car'         ; 'cat'         ;
    'chair'       ; 'cow'         ; 'diningtable' ; 'dog'         ;
    'horse'       ; 'motorbike'   ; 'pottedplant' ; 'sheep'       ;
    'sofa'        ; 'train'       ; 'tvmonitor'  };
% Number of class
conf.nbclasse = size(conf.class,1);

% =========================================================================
%% Init image data ========================================================
% Kind of set provide by pascal
%   -Train : Training dataset
%   -Val : Validation training dataset (suggested)
%   -Trainval : Union of the two previous dataset
%   -Test : Testing dataset
% imgclass = Matrix 2xN such as ImgID, ClassID
%       ClassID correspond to the sorted class above.
conf.data.train.imgclass = dlmread(sprintf('%sTrain_Tiny.csv',...
                                            char(conf.basedirset)));
conf.data.val.imgclass = dlmread(sprintf('%sVal_Tiny.csv',...
                                            char(conf.basedirset)));
conf.data.test.imgclass = dlmread(sprintf('%sTest_Tiny.csv',...
                                            char(conf.basedirset)));


end % End of function