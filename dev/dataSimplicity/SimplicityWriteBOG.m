function [] = SimplicityWriteBOG ()
%% Script that allow me to generate multiple approach by parametrization.

%% PARAMETER CONFIGURATION
% ==> Callback of default value
% =========================================================================
% HIST PARAMETERS
tmp.hist.method = 'knn'; % Method for clustering desc. ('knn, kdtree')
% =========================================================================
% DSIFT PARAMETERS
tmp.dsift.binSize = 3 ;  % Bin size, in px, for dsift
tmp.dsift.step = 1 ;     % Step, in px, for dsift between two desc
% =========================================================================
% SIFT PARAMETERS
tmp.sift.kneighbor = 2 ; % Number of neighbor for proximity graph
tmp.sift.dist = 'euclidean'; % Kind of distance to measure for prox graph
% -> Possible distance : euclidean, cityblock, chebychev, minkowski
% =========================================================================
% LIBSVM PARAMETERS
tmp.svm.T = 0 ; % SVM Kernel
% 0 = linear                : u'*v
% 1 = polynomial            : (gamma*u'*v + coef0)^degree
% 2 = radial basis function : exp(-gamma*|u-v|^2)
% 3 = sigmoid               : tanh(gamma*u'*v + coef0)
tmp.svm.S = 0 ; % SVM type
% 0 = C-SVC
% 1 = nu-SVC
% 2 = one-class SVM
% 3 = epsilon-SVR
% 4 = nu-SVR
tmp.svm.C = 1 ;
% Cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)
tmp.svm.G = 4 ;
% Gamma : set gamma in kernel function (default 1/num_features)
tmp.svm.P = 0.1 ;
% Epsilon : set the epsilon in loss function of epsilon-SVR (default 0.1)
% =========================================================================
% VOCAB PARAMETERS
tmp.vocab.method = 'grid'; % Method construction for vocab (grid,sift).
tmp.vocab.nbwords = 500 ;  % Number of words for vocab.
% =========================================================================
% GLOBAL METHOD PARAMETERS
tmp.method = 'grid' ; % General computation method (grid, sift)
% -> Useful to know if we need frame matrix or not.

% =========================================================================
% Create Folder where will be stored data file (stats, weka and mat)
mkdir data ;
mkdir data/mat ;
mkdir data/graph ;

minfreq=[ 750 500 400 300 250 200 150 100 ];

% =========================================================================
%% DSIFT DATA GENERATION
% =========================================================================
fprintf('# ===========================================================\n');
fprintf('#                   DSIFT DATA GENERATION                    \n');
fprintf('# ===========================================================\n');

%% Multiple vocab size from grid vocab
% Grid parameter :
%   BinSize : 3px
%   Step    : 6px
% Other parameter are set to default
fprintf('# ===========================================================\n');
fprintf('#               MULTIPLE VOCAB SIZE FROM GRID VOCAB          \n');
fprintf('# ===========================================================\n');
% Set grid parameter
tmp.dsift.step = 6 ;
tmp.dsift.binSize = 3 ;
% Set vocab to grid
tmp.vocab.method = 'sift';
% Vocab Variation from 250 to 1000 with step 250
for i=1000:1000
    tmp.vocab.nbwords = i;
    for j=1:size(minfreq,2)
    load(sprintf('data/mat/Simplicity-B%dS%dW%s%d.mat',...
        tmp.dsift.binSize, tmp.dsift.step,...
        tmp.vocab.method(1:1), tmp.vocab.nbwords),...
        'data');
    occ=csvread(sprintf('data/graph/resultplg/grid/csvfile/Simplicity-B%dS%dW%s%d-Full-%d.csv',...
        tmp.dsift.binSize, tmp.dsift.step,...  
        tmp.vocab.method(1:1), tmp.vocab.nbwords, minfreq(j)),1,0);
    data.full.hgraph = histgraph(data,occ);
    data.conf.full.imgClass=cat(1,data.conf.train.imgClass,data.conf.val.imgClass);
    weka(data.full.hgraph,data.conf.class,data.conf.full.imgClass,...
        sprintf('data/graph/resultplg/grid/wekafile/Simplicity-B%dS%dW%s%d-Full-%d',...
            tmp.dsift.binSize, tmp.dsift.step,...
            tmp.vocab.method(1:1), tmp.vocab.nbwords, minfreq(j)));
    save(sprintf('data/mat/Simplicity-B%dS%dW%s%d.mat',...
                data.conf.dsift.binSize, data.conf.dsift.step,...
                data.conf.vocab.method(1:1), data.conf.vocab.nbwords),...
                'data');
   end
end

%% Multiple vocab size from sift vocab
% All parameter are set to default
fprintf('# ===========================================================\n');
fprintf('#               MULTIPLE VOCAB SIZE FROM SIFT VOCAB          \n');
fprintf('# ===========================================================\n');
% Set vocab to grid
tmp.vocab.method = 'sift';
% Vocab Variation from 250 to 1000 with step 250
for i=250:250:1000
    tmp.vocab.nbwords = i;
    for j=1:size(minfreq,2)
    load(sprintf('data/mat/Simplicity-B%dS%dW%s%d.mat',...
        tmp.dsift.binSize, tmp.dsift.step,...
        tmp.vocab.method(1:1), tmp.vocab.nbwords),...
        'data');
    occ=csvread(sprintf('data/graph/resultplg/grid/csvfile/Simplicity-B%dS%dW%s%d-Full-%d.csv',...
        tmp.dsift.binSize, tmp.dsift.step,...
        tmp.vocab.method(1:1), tmp.vocab.nbwords, minfreq(j)),1,0);
    data.full.hgraph = histgraph(data,occ);
    data.conf.full.imgClass=cat(1,data.conf.train.imgClass,data.conf.val.imgClass);
    weka(data.full.hgraph,data.conf.class,data.conf.full.imgClass,...
        sprintf('data/graph/resultplg/grid/wekafile/Simplicity-B%dS%dW%s%d-Full-%d',...
            tmp.dsift.binSize, tmp.dsift.step,...
            tmp.vocab.method(1:1), tmp.vocab.nbwords, minfreq(j)));
    save(sprintf('data/mat/Simplicity-B%dS%dW%s%d.mat',...
                data.conf.dsift.binSize, data.conf.dsift.step,...
                data.conf.vocab.method(1:1), data.conf.vocab.nbwords),...
                'data');
   end
end

%% Multiple step size
% Vocab parameter :
%   Method  : Grid
%   NbWords : 500
% Grid Parameter :
%   BinSize : 3px
fprintf('# ===========================================================\n');
fprintf('#                       MULTIPLE STEP SIZE                   \n');
fprintf('# ===========================================================\n');
% Setting vocab parameter
tmp.vocab.method = 'grid' ;
tmp.vocab.nbwords = 500 ;

for i=4:2:12
    tmp.dsift.step = i ;
    for j=1:size(minfreq,2)
    load(sprintf('data/mat/Simplicity-B%dS%dW%s%d.mat',...
        tmp.dsift.binSize, tmp.dsift.step,...
        tmp.vocab.method(1:1), tmp.vocab.nbwords),...
        'data');
    occ=csvread(sprintf('data/graph/resultplg/grid/csvfile/Simplicity-B%dS%dW%s%d-Full-%d.csv',...
        tmp.dsift.binSize, tmp.dsift.step,...
        tmp.vocab.method(1:1), tmp.vocab.nbwords, minfreq(j)),1,0);
    data.full.hgraph = histgraph(data,occ);
    data.conf.full.imgClass=cat(1,data.conf.train.imgClass,data.conf.val.imgClass);
    weka(data.full.hgraph,data.conf.class,data.conf.full.imgClass,...
        sprintf('data/graph/resultplg/grid/wekafile/Simplicity-B%dS%dW%s%d-Full-%d',...
            tmp.dsift.binSize, tmp.dsift.step,...
            tmp.vocab.method(1:1), tmp.vocab.nbwords, minfreq(j)));
    save(sprintf('data/mat/Simplicity-B%dS%dW%s%d.mat',...
                data.conf.dsift.binSize, data.conf.dsift.step,...
                data.conf.vocab.method(1:1), data.conf.vocab.nbwords),...
                'data');
    end
end


%% Multiple binSize
% Vocab parameter :
%   Method  : Grid
%   NbWords : 500
% Grid Parameter
%   Step    : 6px
fprintf('# ===========================================================\n');
fprintf('#                       MULTIPLE BIN SIZE                    \n');
fprintf('# ===========================================================\n');
% Setting vocab parameter
tmp.vocab.method = 'grid' ;
tmp.vocab.nbwords = 500 ;
% Setting BinSize parameter
tmp.dsift.step = 6 ;

for i=6:3:12
    tmp.dsift.binSize = i ;
    for j=1:size(minfreq,2)
    load(sprintf('data/mat/Simplicity-B%dS%dW%s%d.mat',...
        tmp.dsift.binSize, tmp.dsift.step,...
        tmp.vocab.method(1:1), tmp.vocab.nbwords),...
        'data');
    occ=csvread(sprintf('data/graph/resultplg/grid/csvfile/Simplicity-B%dS%dW%s%d-Full-%d.csv',...
        tmp.dsift.binSize, tmp.dsift.step,...
        tmp.vocab.method(1:1), tmp.vocab.nbwords, minfreq(j)),1,0);
    data.full.hgraph = histgraph(data,occ);
    data.conf.full.imgClass=cat(1,data.conf.train.imgClass,data.conf.val.imgClass);
    weka(data.full.hgraph,data.conf.class,data.conf.full.imgClass,...
        sprintf('data/graph/resultplg/grid/wekafile/Simplicity-B%dS%dW%s%d-Full-%d',...
            tmp.dsift.binSize, tmp.dsift.step,...
            tmp.vocab.method(1:1), tmp.vocab.nbwords, minfreq(j)));
    save(sprintf('data/mat/Simplicity-B%dS%dW%s%d.mat',...
                data.conf.dsift.binSize, data.conf.dsift.step,...
                data.conf.vocab.method(1:1), data.conf.vocab.nbwords),...
                'data');
   end
 end

fclose(tmp.fID);


%% SIFT DATA GENERATION
% =========================================================================
fprintf('# ===========================================================\n');
fprintf('#                    SIFT DATA GENERATION                    \n');
fprintf('# ===========================================================\n');
% Set method to sift
tmp.method = 'sift';
tmp.sift.kneighbor = 3 ; % Kneighbor Initialisation

% Multiple vocab size from grid vocab
% Grid parameter :
%   BinSize : 3px
%   Step    : 6px
% Other parameter are set to default
fprintf('# ===========================================================\n');
fprintf('#               MULTIPLE VOCAB SIZE FROM GRID VOCAB          \n');
fprintf('# ===========================================================\n');
% Grid parameter for vocab
tmp.dsift.binSize = 3;
tmp.dsift.step = 6 ;
% Set vocab to grid
tmp.vocab.method = 'grid';
% Vocab Variation from 250 to 1000 with step 250
for i=250:250:1000
    tmp.vocab.nbwords = i;
    for j=1:size(minfreq,2)
    load(sprintf('data/mat/Simplicity-K%dD%sW%s%d.mat',...
        tmp.sift.kneighbor, tmp.sift.dist(1:2),...
        tmp.vocab.method(1:1), tmp.vocab.nbwords),...
        'data');
    occ=csvread(sprintf('data/mat/Simplicity-K%dD%sW%s%d.mat',...
        tmp.sift.kneighbor, tmp.sift.dist(1:2),...
        tmp.vocab.method(1:1), tmp.vocab.nbwords, minfreq(j)),1,0);
    data.full.hgraph = histgraph(data,occ);
    data.conf.full.imgClass=cat(1,data.conf.train.imgClass,data.conf.val.imgClass);
    weka(data.full.hgraph,data.conf.class,data.conf.full.imgClass,...
        sprintf('data/mat/Simplicity-K%dD%sW%s%d.mat',...
        tmp.sift.kneighbor, tmp.sift.dist(1:2),...
        tmp.vocab.method(1:1), tmp.vocab.nbwords, minfreq(j)));
    save(sprintf('data/mat/Simplicity-K%dD%sW%s%d.mat',...
        tmp.sift.kneighbor, tmp.sift.dist(1:2),...
        tmp.vocab.method(1:1), tmp.vocab.nbwords),...
        'data');
   end
end

%% Multiple vocab size from sift vocab
% Other parameter are set to default
fprintf('# ===========================================================\n');
fprintf('#               MULTIPLE VOCAB SIZE FROM SIFT VOCAB          \n');
fprintf('# ===========================================================\n');
% Set vocab to sift
tmp.vocab.method = 'sift';
% Vocab Variation from 250 to 1000 with step 250
for i=250:250:1000
    tmp.vocab.nbwords = i;
    for j=1:size(minfreq,2)
    load(sprintf('data/mat/Simplicity-K%dD%sW%s%d.mat',...
        tmp.sift.kneighbor, tmp.sift.dist(1:2),...
        tmp.vocab.method(1:1), tmp.vocab.nbwords),...
        'data');
    occ=csvread(sprintf('data/mat/Simplicity-K%dD%sW%s%d.mat',...
        tmp.sift.kneighbor, tmp.sift.dist(1:2),...
        tmp.vocab.method(1:1), tmp.vocab.nbwords, minfreq(j)),1,0);
    data.full.hgraph = histgraph(data,occ);
    data.conf.full.imgClass=cat(1,data.conf.train.imgClass,data.conf.val.imgClass);
    weka(data.full.hgraph,data.conf.class,data.conf.full.imgClass,...
        sprintf('data/mat/Simplicity-K%dD%sW%s%d.mat',...
        tmp.sift.kneighbor, tmp.sift.dist(1:2),...
        tmp.vocab.method(1:1), tmp.vocab.nbwords, minfreq(j)));
    save(sprintf('data/mat/Simplicity-K%dD%sW%s%d.mat',...
        tmp.sift.kneighbor, tmp.sift.dist(1:2),...
        tmp.vocab.method(1:1), tmp.vocab.nbwords),...
        'data');
   end
end


%% Multiple kneighbor
% Vocab parameter :
%   Method  : Grid
%   NbWords : 500
% Grid parameter :
%   BinSize : 3px
%   Step    : 6px
% Other parameter are set to default
fprintf('# ===========================================================\n');
fprintf('#                       MULTIPLE KNEIGHBOR                   \n');
fprintf('# ===========================================================\n');
% Grid parameter for vocab
tmp.dsift.binSize = 3;
tmp.dsift.step = 6 ;
% Setting vocab parameter
tmp.vocab.method = 'grid' ;
tmp.vocab.nbwords = 500 ;
tmp.sift.kneighbor = 5 ; % Kneighbor Initialisation
for i=0:3
    tmp.sift.kneighbor = tmp.sift.kneighbor + i*2  ;
    for j=1:size(minfreq,2)
    load(sprintf('data/mat/Simplicity-K%dD%sW%s%d.mat',...
        tmp.sift.kneighbor, tmp.sift.dist(1:2),...
        tmp.vocab.method(1:1), tmp.vocab.nbwords),...
        'data');
    occ=csvread(sprintf('data/mat/Simplicity-K%dD%sW%s%d.mat',...
        tmp.sift.kneighbor, tmp.sift.dist(1:2),...
        tmp.vocab.method(1:1), tmp.vocab.nbwords, minfreq(j)),1,0);
    data.full.hgraph = histgraph(data,occ);
    data.conf.full.imgClass=cat(1,data.conf.train.imgClass,data.conf.val.imgClass);
    weka(data.full.hgraph,data.conf.class,data.conf.full.imgClass,...
        sprintf('data/mat/Simplicity-K%dD%sW%s%d.mat',...
        tmp.sift.kneighbor, tmp.sift.dist(1:2),...
        tmp.vocab.method(1:1), tmp.vocab.nbwords, minfreq(j)));
    save(sprintf('data/mat/Simplicity-K%dD%sW%s%d.mat',...
        tmp.sift.kneighbor, tmp.sift.dist(1:2),...
        tmp.vocab.method(1:1), tmp.vocab.nbwords),...
        'data');
   end
end
end
