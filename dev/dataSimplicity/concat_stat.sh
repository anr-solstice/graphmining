#!/bin/bash

STATDIR="data/"

for f in $STATDIR*.txt ; do 
    echo "" >> All_Stat.txt
    echo "=====================================================================" >> All_Stat.txt
    echo "           $f                "  >> All_Stat.txt
    echo "=====================================================================" >> All_Stat.txt
    cat $f >> All_Stat.txt
done
