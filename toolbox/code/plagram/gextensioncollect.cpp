/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *   Copyright (C) 2010,2011 by Adriana Prado and Baptiste Jeudy           *
 *   baptiste.jeudy at univ-st-etienne fr                                  *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "gextensioncollect.h"
#include "gsubgraphiso.h"

GExtensionCollect::GExtensionCollect(GFrequency minf, int maxg, bool do_qp):
  totMap(0), totExt(0), totValid(0), 
  quickpruned(0),count(0), countNotValid(0), numEdges(0), 
  minfreq(minf), maxgap(maxg), do_quickprune(do_qp)
{
}


GExtensionCollect::~GExtensionCollect()
{
}


int GExtensionCollect::process ( GSubGraphIso *subgraphiso )
{    
  timeExt = clock();
	
  GPattern *pattern = subgraphiso->pattern;

  // active nodes list
  vector<GNodeID> &nextactive = pattern->nextactive;
	
  // nodes in the pattern
  vector<GGraphNode> &patternnodes = pattern->graph.nodes;
  // nodes in the current database graph
  vector<GGraphNode> &largenodes = subgraphiso->largegraph->nodes;

  // necessary to validate whether an extension is an external path:
  vector<bool> bannedNodes(pattern->activenodesend, false); 
	
  list<pair<vector<GToken>,GExtensionData> > lp;
		
  // check whether it is the first iteration
  bool firstIteration = pattern->tokens.size() == 1; 

  GNodeID ext_start_node = pattern->activenodesbegin;
  if (firstIteration)
    ext_start_node = nextactive[ext_start_node]; // the extension must close the first face


  /****************************** Step One : compute all extensions **********/
  do{ // for each active node i, build a new extension if possible
    pair<vector<GToken>,GExtensionData> p; // new extension
    GToken token;
		
    token.fromdata = patternnodes[ext_start_node].data; //label of the first active node
    token.from = ext_start_node;
    token.direction = forward;
    p.first.push_back(token);

    GNodeID j;
    GNodeID j_large, i_large = (*(subgraphiso->largegraph_index))[subgraphiso->offset+ext_start_node];
    GEdgeID lastEdgeId = subgraphiso->lastEdgeUsed[ext_start_node];

    GEdgeID edgeId;
    GGraphEdge *edge;
    /* while it is not a face, grows the extension by one edge (i,j)
        i,j : index of the nodes in pattern graph and code
        i_large, j_large : corresponding index in database graph */
    while (p.first.back().direction != backward) {
      // the next possible edge (i_large, j_large) in database graph for the extension
      edge = subgraphiso->largegraph->nextEdge(i_large, lastEdgeId, &edgeId);
	
      if(edge == NULL)
	{ //on outer face: no way to extend from this node
	  bannedNodes[ext_start_node]=true;
	  p.first.resize(0);
	  break;
	}
	
      j_large = edge->dest_nid;
      p.first.back().data = edge->data; // edge label
      p.first.back().todata = largenodes[j_large].data; // destination node label  

      j = subgraphiso->pattern_index[j_large]; 
      if ( j != GNONODEID ) { // j is in the pattern => edge is backward
	p.first.back().to = j;
	p.first.back().direction = backward;

	if (edge->eq_eid == subgraphiso->lastEdgeUsed[j]) {
	  /* this is not a real extension: there is no edge from i_large
	     that is not already in the pattern */
	  p.first.resize(0);
	  break;
	}
	// an extension was found from ext_start_node
	bannedNodes[ext_start_node] = true; // this line was forgotten in previous versions: bug************ 
	if (do_quickprune && pattern->quickPrune(p.first.back()) ){
	  p.first.resize(0);
	  quickpruned++;
	  break;
	}
      }
      else { // edge is forward
	bannedNodes[ext_start_node] = true;

	p.first.back().to = patternnodes.size() + p.first.size() - 1; //create another index in the pattern
	p.first.back().direction = forward;

	if (do_quickprune && pattern->quickPrune(p.first.back()) ) {
	  p.first.resize(0);
	  quickpruned++;
	  break;
	}
	// new edge
	token.fromdata = p.first.back().todata; 
	token.from = p.first.back().to;
	token.direction = forward; // on sait pas encore ? => pour le test de la boucle while
	p.first.push_back(token);
				
	i_large = j_large;
	lastEdgeId = edge->eq_eid; //take the equivalent edge of node j_large
      }
    } //end while loop "not a backward edge"
		
    if (p.first.size() > 0) {
      p.second.start = ext_start_node;
      p.second.stop =  p.first.back().to;
      lp.push_back(p);
      numEdges += p.first.size();
    }
    ext_start_node = nextactive[ext_start_node];
  } while (ext_start_node != pattern->activenodesbegin); // end loop active nodes

  /*************************** Step Two : validation of extensions **********/
  //validate the extensions: select only the extensions which are external paths
  // do this only if it is not the first iteration 

  long long int numExt = lp.size();
  long long int numValid = 0;

  if (numExt > 0)
    {
      count += numExt;

      if (!firstIteration) 
	validateListExtensions(lp,bannedNodes,nextactive,pattern->activenodesend);
      numValid = lp.size();
      countNotValid += numExt - numValid;

      /************** Step Three : insert extensions in the map  **********/
      timeMap = clock();
      if (numValid > 0) //if there are valid extensions
	{
	  list<pair<vector<GToken>,GExtensionData> >::iterator it;
	  for (it=lp.begin(); it!=lp.end(); it++)
	    {
	      // search in the extensions already determined
	      pair<map<vector<GToken>,GExtensionData,VGTokenLt>::iterator,bool> f = extensions.insert ( *it );
				
	      if ( f.second ) 
		{ //if the extension does not exist yet...
		  // a new extension
		  f.first->second.frequency = 1;
		  f.first->second.nb_occ = 1;
		  f.first->second.tid = (GTid) subgraphiso->largegraph;
		}
	      else
		{ // an existing extension
		  f.first->second.nb_occ ++;
		  // frequency is increased only if last occurrence was not in the same large graph
		  if ( f.first->second.tid != (GTid) subgraphiso->largegraph ) 
		    { 
		      unsigned int newFrameid = ((GTid) subgraphiso->largegraph)->frameid;

		      //for the temporal patterns
		      if ((newFrameid > f.first->second.tid->frameid + maxgap) && // if gap is exceeded
			  (f.first->second.frequency < minfreq)) // and previous occurrences freq less than minfreq
			f.first->second.frequency = 1; // previous occurrences cannot constitute a temp pattern
		      else
			++f.first->second.frequency;
		      //****************************
		      f.first->second.tid = (GTid) subgraphiso->largegraph; 
		    }  
		}
	    }
	}
      timeMap = clock() - timeMap;
      totMap += timeMap;
    }
     
  timeExt = clock() - timeExt;
  totExt += timeExt;
  return numValid;
  // -- cout << "  All extensions : " << endl;
  // -- print();
} 

void GExtensionCollect::validateListExtensions(list<pair<vector<GToken>,GExtensionData> > &lp, 
					      vector<bool> &bannedNodes, vector<GNodeID> &nextactive, 
					      int activenodesend) {
  timeValid = clock();

  list<pair<vector<GToken>,GExtensionData> >::iterator it;

  it = lp.begin();
  while (it != lp.end()) {
    GNodeID start = it->second.start;
    GNodeID stop = it->second.stop;
    GNodeID i = nextactive[start];
    while (i != stop) {
      if (bannedNodes[i]) break;
      i=nextactive[i];
    }
    if (i != stop && bannedNodes[i])
      it = lp.erase(it);
    else
      it++;
  }
  
  timeValid = clock() - timeValid;
  totValid += timeValid;
}



void GExtensionCollect::print()
{
  for ( map<vector<GToken>,GExtensionData,VGTokenLt>::iterator i = extensions.begin ();
	i != extensions.end (); i++)
    {
      // -- cout << "  " ;
      for (uint j=0; j<i->first.size(); j++)
	{
	  GToken token =  i->first[j];
	  printGToken(token);
	}
      // -- cout << "(" << i->second.frequency << ")" << endl << endl;
    }
}
