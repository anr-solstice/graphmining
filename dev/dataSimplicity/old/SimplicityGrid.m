function [ result ] = SimplicityGrid(nbwords, binSize, step, clust,...
                                     svmT, svmS, svmC, svmG, svmP)
%% Simplicity Grid ========================================================
% Function for applying dense sift to simplicity image set
%
%Syntaxe
% [ result ] = SimplicityGrid(nbwords)
% [ result ] = SimplicityGrid(nbwords, binSize)
% [ result ] = SimplicityGrid(nbwords, binSize, Step)
% [ result ] = SimplicityGrid(nbwords, binSize, Step, svmC)
% [ result ] = SimplicityGrid(nbwords, binSize, Step, svmC, svmG)
% [ result ] = SimplicityGrid(nbwords, binSize, Step, svmC, svmG, svmP)
% [ result ] = SimplicityGrid(nbwords, binSize, Step, svmC, svmG, svmP, svmT)
%
%Parameters
% If not specified, parameter are set to default value, specified into
% squared brackets.
% Parameters must be in the exact defined order
% - nbWorkds (REQUIRED)
%       -> Number of words for our vocabulary 
% - clust   ['knn']
%       -> Define method to compute nearest neighbor search.
% - binSize [3]
%       -> Size, in px, of bin from SIFT descriptor.
% - Step    [1]
%       -> Step, in px, between two descriptor
% - svmT    [0]
%       -> Value of parameter T for libsvm that define kernel to use
%           --> Possible value : 
%	0 -- linear: u'*v
%	1 -- polynomial: (gamma*u'*v + coef0)^degree
%	2 -- radial basis function: exp(-gamma*|u-v|^2)
%	3 -- sigmoid: tanh(gamma*u'*v + coef0)
% - svmS    [0]
%       -> Value of parameter S for libsvm that define type of SVM
%           --> Possible value
%	0 -- C-SVC
%	1 -- nu-SVC
%	2 -- one-class SVM
%	3 -- epsilon-SVR
%	4 -- nu-SVR
% - svmC    [1] 
%       -> Value of C for libsvm
% - svmG    [1]
%       -> Value of parameter G, gamma, for libsvm 
% - svmP    [0.01]
%       -> Value of parameter P, epsilon, for libsvm

%Description
% Return a set of frame f{i} = [x y scale angle] and sift descriptors
% d{i} = 1x128 for all image in dirimg specified by id in the first column
% of limg.
% For more information, please refer to vl_dsift documentation.

% =========================================================================
%% Loading vlfeat for matlab
% run('vl_setup');

% Don't forget to include the following folder to the matlab path : 
% - PFE-fouille de Graphe
% - Simplicity 
%
% =========================================================================
%% Configuration ==========================================================
% Base directory (Image and Set)
conf.basedirimg= 'Simplicity/';
% List of categoy
conf.class = {
    'Africa'    ; 'Beach'     ; 'Buildings' ; 'Buses'    ; 'Dinosaurs' ;
    'Elephants' ; 'Flowers'   ; 'Horses'    ; 'Mountain' ; 'Food'     };
% Number of class
conf.nbclasse = size(conf.class,1);

% =========================================================================
%% Init image data ========================================================
% Kind of set for simplicity
%   -Train : Training dataset
%   -Val : Validation training dataset (suggested)
conf.train.img = dlmread(sprintf('%sSimplicityTrain.csv',...
                                  char(conf.basedirimg)));
%conf.train.img = conf.train.img(1:100,:);
conf.train.imgIdx = conf.train.img(:,1);
conf.train.imgClass = conf.train.img(:,2);
conf.train.nbImg = size(conf.train.img,1);

conf.val.img = dlmread(sprintf('%sSimplicityVal.csv',...
                                  char(conf.basedirimg)));
%conf.val.img = conf.val.img(1:100,:);
conf.val.imgIdx = conf.val.img(:,1);
conf.val.imgClass = conf.val.img(:,2);
conf.val.nbImg = size(conf.val.img,1);

conf.full.img = dlmread(sprintf('%sSimplicityFull.csv',...
                                  char(conf.basedirimg)));
%conf.full.img = conf.full.img(1:100,:);
conf.full.imgIdx = conf.full.img(:,1);
conf.full.imgClass = conf.full.img(:,2);
conf.full.nbImg = size(conf.full.img,1);

% =========================================================================
%% Configurable variable ==================================================
conf.quantizer = 'knn' ; % 'kdtree' by default

% Default value : 
conf.dsift.bin = 3;
conf.dsift.step = 1;

conf.svm.t = '0' ; % Linear SVM
conf.svm.s = '0' ; % C-SVC
conf.svm.c = '1' ;
conf.svm.g = '4' ;
conf.svm.p = '0.01' ;

conf.kmean.nbwords = nbwords;

switch nargin 
    case 2
        conf.dsift.bin = binSize;
    case 3
        conf.dsift.bin = binSize;
        conf.dsift.step = step;
    case 4
        conf.quantizer = clust; 
        conf.dsift.bin = binSize;
        conf.dsift.step = step;
    case 5
        conf.quantizer = clust; 
        conf.dsift.bin = binSize;
        conf.dsift.step = step;

        conf.svm.t = sprintf('%s', svmT) ;
    case 6
        conf.quantizer = clust; 
        conf.dsift.bin = binSize;
        conf.dsift.step = step;

        conf.svm.t = sprintf('%s', svmT) ;
        conf.svm.s = sprintf('%s', svmS) ;
    case 7
        conf.quantizer = clust; 
        conf.dsift.bin = binSize;
        conf.dsift.step = step;

        conf.svm.t = sprintf('%s', svmT) ;
        conf.svm.s = sprintf('%s', svmS) ;
        conf.svm.c = sprintf('%s', svmC) ;
    case 8
        conf.quantizer = clust; 
        conf.dsift.bin = binSize;
        conf.dsift.step = step;

        conf.svm.t = sprintf('%s', svmT) ;
        conf.svm.s = sprintf('%s', svmS) ;
        conf.svm.c = sprintf('%s', svmC) ;
        conf.svm.g = sprintf('%s', svmG) ;
    case 9
        conf.quantizer = clust; 
        conf.dsift.bin = binSize;
        conf.dsift.step = step;

        conf.svm.t = sprintf('%s', svmT) ;
        conf.svm.s = sprintf('%s', svmS) ;
        conf.svm.c = sprintf('%s', svmC) ;
        conf.svm.g = sprintf('%s', svmG) ;
        conf.svm.p = sprintf('%s', svmP) ;
end
% =========================================================================
%% Create file with stats =================================================
tmp.fID = fopen(sprintf('SimplB%dS%dW%d.txt',conf.dsift.bin, ...
                conf.dsift.step, conf.kmean.nbwords),'a');
tmp.filename = fopen(tmp.fID);
% =========================================================================
%% Construct vocab ========================================================
conf.kmean =  vocabdsift(conf, conf.train.nbImg, conf.kmean.nbwords,...
                    sprintf('SimplB%dS%dW%d.txt',conf.dsift.bin, ...
                    conf.dsift.step, conf.kmean.nbwords));
% =========================================================================
%% Compute Hist for Train img =============================================
train = histsdsift(conf,conf.train, sprintf('SimplB%dS%dW%d.txt',conf.dsift.bin, ...
                               conf.dsift.step, conf.kmean.nbwords));  

%% Compute Hist for validation img ========================================
val = histsdsiftdsift(conf,conf.val, sprintf('SimplB%dS%dW%d.txt',conf.dsift.bin, ...
                           conf.dsift.step, conf.kmean.nbwords));  

%% Training & Testing SVM =================================================
fprintf(tmp.fID,'%% === Train SVM, , \n');
cinit = clock ;
svm.model = ovrtrain(conf.train.imgClass, train.histsdsiftknn',...
            sprintf('-s %s -t %s -g %s -c %s -p %s',...
            conf.svm.t, conf.svm.s, conf.svm.g,...
            conf.svm.c, conf.svm.p));
cend = clock ;
fprintf(tmp.fID,'%Training SVM,%d, \n', etime(cend,cinit));

fprintf(tmp.fID,'%% === Test SVM, , \n');
[svm.res.pred svm.res.ac svm.res.decv] = ovrpredict(conf.val.imgClass,...
                      val.histsdsiftknn', svm.model);
fprintf(tmp.fID,'%Testing SVM,%d, \n', etime(cend,cinit));
       
% Compute the confusion matrix
idx = sub2ind([conf.nbclasse, conf.nbclasse], ...
              conf.val.imgClass, svm.res.pred) ;
confus = zeros(conf.nbclasse) ;
confus = vl_binsum(confus, ones(size(idx)), idx) ;

% % Plots
% h=figure(1) ; clf;
% imagesc(confus) ;
% title(sprintf('Confusion matrix (%.2f %% accuracy)', ...
%               1000 * mean(diag(confus)/conf.val.nbImg) )) ;
% saveas(h,sprintf('SimplB%dS%dW%d',conf.dsift.bin, ...             
%           conf.dsift.step, conf.kmean.nbwords),'jpg');
%% Write weka file for 
fprintf(tmp.fID, '%% === Writing weka file');
[ tmp.errmsg ] = weka (train.histsdsiftknn, conf.class, conf.train.img,...
                    sprintf('Simpl-GridB%dS%d-Train-%dW',...
                    conf.dsift.bin, conf.dsift.step, conf.kmean.nbwords));
fprintf(tmp.fID,'%s', tmp.errmsg);
[ tmp.errmsg ] = weka (val.histsdsiftknn, conf.class, conf.val.img,...
                    sprintf('Simpl-GridB%dS%d-Val-%dW',...
                    conf.dsift.bin, conf.dsift.step, conf.kmean.nbwords));
fprintf(tmp.fID,'%s', tmp.errmsg);
fclose(tmp.fID);

result.conf = conf;
result.train = train;
result.val = val;
result.svm = svm ;

end % End of function
