function [ m ] = vocab (conf, nbimg, nbwords, filename)
%% Function that create Bag of Word (BOW) 
%
%Syntaxe
% [ m ] = grid(conf, nbwords)
%
%Parameters
% All parameter are required
% - Conf (required)
%       -> Configuration struct. Cf config for more information.
% TODO : Complete this documentation
% - NbImg 
%       -> Number of image to use for vocab
% - NbWords
%       -> Number of cluster (i.e. words) that will construct our
%vocabulary.
%
%Description
% Return a struct such that : 
%   m.nbword -> Give the number of word (center of cluster) that discribe 
% our vocabulary.
%   m.vocab -> matrix of size [nbwords]x[desc.dimension] with the center 
%of clusters.
%   m.kdtree -> kdtree from vocab.

% =========================================================================
%% Construct vocab ========================================================
% Descriptor of all image
d = {};
c = [];
tmp.sel = randperm(conf.train.nbImg,nbimg) ; 

if (nargin > 3 )
    tmp.fileId = fopen(sprintf('%s',filename),'a');
    fprintf(tmp.fileId,'%% ==== Construct vocabulary, ,  \n') ;
    fprintf(tmp.fileId,'function, img / time , time /avg \n') ;
end

tmp.cinit = clock;
parfor i=1:nbimg
    c1 = clock ;
    %fprintf('Applying vl_dsift to %d.jpg, image %04d / %04d \n',...
    %	      conf.train.imgIdx(tmp.sel(i)),i,nbimg);
    imc=imread(sprintf('%s%d.jpg', conf.basedirimg, conf.train.imgIdx(tmp.sel(i))));
    % Convert into gray image
    imb = single(rgb2gray(imc)) ;
    % Applying dsift
    [~,d{i}] = vl_dsift(imb,'size', conf.dsift.binSize,...
                            'step', conf.dsift.step);
    c2 = clock;
    c(i) = etime(c2,c1);
end
tmp.cend = clock;
tmp.cmean = mean(c);
if (nargin > 3 )
    fprintf(tmp.fileId,'vl_dsift,%03d,%03d. \n',...
            etime(tmp.cend,tmp.cinit), tmp.cmean);
else 
    fprintf('=================================\nvl_dsift,%03d,%03d. \n',...
             etime(tmp.cend,tmp.cinit), tmp.cmean);
end   
%% Data Model Initialisation ==============================================
m.nbwords = nbwords;
% Concat all descriptor into one matrix. Converted to single for k-means.
% Take 500000 random desc.
tmp.cinit = clock;
d = single(vl_colsubset(cat(2, d{:}), 500000));
tmp.cend = clock;
if (nargin > 3 )
    fprintf(tmp.fileId,'Concat,%d sec. \n',etime(tmp.cend,tmp.cinit));
else 
    fprintf('Concat,%d\n',etime(tmp.cend,tmp.cinit));
end
%% Applying K-means =====================================================
% C = Center, A = Assignements
tmp.cinit = clock;
[ m.vocab , ~ ] = vl_kmeans(d, m.nbwords);
m.kdtree = vl_kdtreebuild(m.vocab);
tmp.cend = clock;
if (nargin > 3 )
    fprintf(tmp.fileId,...
    'vl_kmeans,%04d,%d\n',m.nbwords, etime(tmp.cend,tmp.cinit));
else
    fprintf(...
    'vl_kmeans, %04d , %d\n',m.nbwords, etime(tmp.cend,tmp.cinit));
end

if (nargin > 3 )
    fclose(tmp.fileId);
end
end % Enf of function
