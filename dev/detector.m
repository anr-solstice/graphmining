function [ d , f ] = detector(dirimg, limg, windowsize, frames, orientation, ...
                          octaves, levels, firstoctave, magnif,...
                          peakthresh, edgethresh, normthresh)
%% Function for applying sift to a set of image
%
%Syntaxe
% [ d , f ] = detector(dirimg, limg)
% [ d , f ] = detector(dirimg, limg, frames)                      
% [ d , f ] = detector(dirimg, limg, frames, orientation)
% [ d , f ] = detector(dirimg, limg, frames, orientation, windowsize)
% [ d , f ] = detector(dirimg, limg, frames, orientation, windowsize, octaves)
% [ d , f ] = detector(dirimg, limg, frames, orientation, windowsize, ...
%                          octaves, levels)
% [ d , f ] = detector(dirimg, limg, frames, orientation, windowsize,...
%                          octaves, levels, firstoctave)
% [ d , f ] = detector(dirimg, limg, frames, orientation, windowsize,...
%                          octaves, levels, firstoctave, magnif)
% [ d , f ] = detector(dirimg, limg, frames, orientation, windowsize,...
%                          octaves, levels, firstoctave, magnif,...
%                          peakthresh)
% [ d , f ] = detector(dirimg, limg, frames, orientation, windowsize,...
%                          octaves, levels, firstoctave, magnif,...
%                          peakthresh, edgethresh)                      
% [ d , f ] = detector(dirimg, limg, frames, orientation, windowsize,...
%                          octaves, levels, firstoctave, magnif,...
%                          peakthresh, edgethresh, normthresh)  
%
%Parameters
% If not specified, parameter are set either as default vl_dsift for some
% of them or as specified (for Step and Size). 
% Dirimg and Limg are required parameter !
% - Dirimg (required) !!
%       -> Path de the directory where image are stored.
% - Limg (required)
%       -> Vector or matrix where first dimension list all imgID.
% - Frames
%       -> If specified, set the frames to use (bypass the detector). If 
% frames are not passed in order of increasing scale, they are re-orderded. 
% - Orientations
%       -> If specified, compute the orientations of the frames overriding
% the orientation specified by the 'Frames' option. 
% - WindowSize 2
%       -> Setthe variance of the Gaussian window that determines the 
% descriptor support. It is expressend in units of spatial bins. 
% - Octaves maximum possible
%       -> Setthe number of octave of the DoG scale space. 
% - Levels 3
%       -> Setthe number of levels per octave of the DoG scale space. 
% - FirstOctave 0
%       -> Setthe index of the first octave of the DoG scale space. 
% - Magnif 3
%       -> Setthe descriptor magnification factor. The scale of the 
% keypoint is multiplied by this factor to obtain the width (in pixels) of
% the spatial bins. For instance, if there are there are 4 spatial bins
% along each spatial direction, the ``side'' of the descriptor is 
% approximatively 4 * MAGNIF. 
% - PeakThresh 0
%       -> Setthe peak selection threshold. 
% - EdgeThresh 10
%       -> Setthe non-edge selection threshold. 
% - NormThresh -inf
%       -> Setthe minimum l2-norm of the descriptors before normalization.
% Descriptors below the threshold are set to zero. 
%
%Description
% Return a set of frame f{i} = [x y scale angle] and sift descriptors
% d{i} = 1x128 for all image in dirimg specified by id in the first column
% of limg.
% For more information, please refer to vl_sift documentation.

% =========================================================================
%% Initialise local variable for parfor loop ==============================
% Descriptor of all image
d = {};
f = {};

% Trace 
fprintf('Applying vl_sift to %s Image set.  \n', dirimg);

% Temp Variable to not parse all image
limg = limg(1:round(size(limg,1)/100),:); % Only 1% of limg

switch nargin 
    case 2
        parfor i=1:size(limg,1)
            imc=imread(sprintf('%s%06d.jpg', dirimg, limg(i,1)));
            % Convert into gray image
            imb = single(rgb2gray(imc)) ;
            % Applying dsift
            [f{i},d{i}] = vl_sift(imb);
        end
    case 3
        parfor i=1:size(limg,1)
            imc=imread(sprintf('%s%06d.jpg', dirimg, limg(i,1)));
            % Convert into gray image
            imb = single(rgb2gray(imc)) ;
            % Applying dsift
            [f{i},d{i}] = vl_sift(imb,'windowsize', windowsize);
        end
    case 4
        parfor i=1:size(limg,1)
            imc=imread(sprintf('%s%06d.jpg', dirimg, limg(i,1)));
            % Convert into gray image
            imb = single(rgb2gray(imc)) ;
            % Applying dsift
            [f{i},d{i}] = vl_sift(imb,'windowsize', windowsize,...
                        'orientation', orientation);
        end
    case 5
        for i=1:size(limg,1)
            imc=imread(sprintf('%s%06d.jpg', dirimg, limg(i,1)));
            % Convert into gray image
            imb = single(rgb2gray(imc)) ;
            % Applying dsift
            [f{i},d{i}] = vl_sift(imb,'windowsize', windowsize,...
                        'orientation', orientation,...
                        'windowsize', windowsize);
        end
    case 6
        for i=1:size(limg,1)
            imc=imread(sprintf('%s%06d.jpg', dirimg, limg(i,1)));
            % Convert into gray image
            imb = single(rgb2gray(imc)) ;
            % Applying dsift
            [f{i},d{i}] = vl_sift(imb,'windowsize', windowsize,...
                        'orientation', orientation,...
                        'frames', frames,...
                        'octaves', octaves);
        end
    case 7
        parfor i=1:size(limg,1)
            imc=imread(sprintf('%s%06d.jpg', dirimg, limg(i,1)));
            % Convert into gray image
            imb = single(rgb2gray(imc));
            % Applying dsift
            [f{i},d{i}] = vl_sift(imb,'windowsize', windowsize,...
                        'orientation', orientation,...
                        'frames', frames,...
                        'octaves', octaves,...
                        'levels', levels);
        end
    case 8
        parfor i=1:size(limg,1)
            imc=imread(sprintf('%s%06d.jpg', dirimg, limg(i,1)));
            % Convert into gray image
            imb = single(rgb2gray(imc)) ;
            % Applying dsift
            [f{i},d{i}] = vl_sift(imb,'windowsize', windowsize,...
                        'orientation', orientation,...
                        'frames', frames,...
                        'octaves', octaves,...
                        'levels', levels,...
                        'firstoctave',  firstoctave);
        end
    case 9
        parfor i=1:size(limg,1)
            imc=imread(sprintf('%s%06d.jpg', dirimg, limg(i,1)));
            % Convert into gray image
            imb = single(rgb2gray(imc)) ;
            % Applying dsift
            [f{i},d{i}] = vl_sift(imb,'windowsize', windowsize,...
                        'orientation', orientation,...
                        'frames', frames,...
                        'octaves', octaves,...
                        'levels', levels,...
                        'firstoctave',  firstoctave,...
                        'magnigication', magnif);
        end
    case 10
        parfor i=1:size(limg,1)
            imc=imread(sprintf('%s%06d.jpg', dirimg, limg(i,1)));
            % Convert into gray image
            imb = single(rgb2gray(imc)) ;
            % Applying dsift
            [f{i},d{i}] = vl_sift(imb,'windowsize', windowsize,...
                        'orientation', orientation,...
                        'frames', frames,...
                        'octaves', octaves,...
                        'levels', levels,...
                        'firstoctave',  firstoctave,...
                        'magnigication', magnif,...
                        'peakthresh', peakthresh);
        end
    case 11
        parfor i=1:size(limg,1)
            imc=imread(sprintf('%s%06d.jpg', dirimg, limg(i,1)));
            % Convert into gray image
            imb = single(rgb2gray(imc)) ;
            % Applying dsift
            [f{i},d{i}] = vl_sift(imb,'windowsize', windowsize,...
                        'orientation', orientation,...
                        'frames', frames,...
                        'octaves', octaves,...
                        'levels', levels,...
                        'firstoctave',  firstoctave,...
                        'magnigication', magnif,...
                        'peakthresh', peakthresh,...
                        'edgethresh', edgethresh);
        end
    case 12
        parfor i=1:size(limg,1)
            imc=imread(sprintf('%s%06d.jpg', dirimg, limg(i,1)));
            % Convert into gray image
            imb = single(rgb2gray(imc)) ;
            % Applying dsift
            [f{i},d{i}] = vl_sift(imb,'windowsize', windowsize,...
                        'orientation', orientation,...
                        'frames', frames,...
                        'octaves', octaves,...
                        'levels', levels,...
                        'firstoctave',  firstoctave,...
                        'magnigication', magnif,...
                        'peakthresh', peakthresh,...
                        'edgethresh', edgethresh,...
                        'normthresh', normthresh);
        end
end
end
