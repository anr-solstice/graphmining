function [ errmsg ] = weka (hist, class, imgclass, filename)
%% Function creating file for weka
%
%Syntaxe
% [ ] = weka (hist, nbwords, filename)
%
%Parameters
% All parameter are required
% - Hist (required)
%       -> Histograme construct from [h,c]=hist(desc, model).
% - Class (required)
%       -> List of all class
% - imgclass (required)
%       -> matrix of size nbimg x 2 such that : 
%       -> Dimension 1 : imgID
%       -> Dimension 2 : classID
% - filename
%       -> Name of the file where data will be written.
%
%Description
% Return an error message if the openning file failed.

% =========================================================================
%% Initialise train file for weka : 
% Trace
fprintf('Writing wekafile %s.arff \n',char(filename));


% Openinf file
[fileID,errmsg] = fopen(sprintf('%s.arff',filename), 'w');

% Writing header
fprintf(fileID,'@relation ImgClassif \n \n');

% Writing words
for i=1:size(hist,1)
     fprintf(fileID,'@attribute word%04d numeric \n',i); 
end
fprintf(fileID,'\n@attribute class {');

% Writing class name
for i=1:size(class,1)
     fprintf(fileID,'%s',class{i});
     if i ~= size(class,1)
         fprintf(fileID,',');
     end
end
fprintf(fileID,'}\n\n');
fprintf(fileID,'@data \n');

% Write image histogram and class
for i=1:size(hist,2)
    for j=1:size(hist,1)
        fprintf(fileID,'%d,',hist(j,i));    
    end
    temp = imgclass(i,1);
    fprintf(fileID,'%s\n',char(class(imgclass(i,1))));
end

end % End of function
