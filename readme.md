PFE - Fouille de Graph
===

5IF INSA de Lyon   
20, Avenue Albert Einstein   
69100 Villeurbane   


Auteur
---
Romain Deville   

Encadrant 
---
Christine Solnon
Elisa Fromont
Baptiste Jeudy

Description
--- 
Dossier git comportant l'ensemble de mes fichiers relatifs à mon PFE - Fouille de graph pour l'analyse d'images et de vidéos.