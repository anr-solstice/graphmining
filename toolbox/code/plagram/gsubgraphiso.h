/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *   Copyright (C) 2010,2011 by Adriana Prado and Baptiste Jeudy           *
 *   baptiste.jeudy at univ-st-etienne fr                                  *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef GSUBGRAPHISO_H
#define GSUBGRAPHISO_H


#include <map>
#include "global.h"
#include "ggraph.h"
#include "gpattern.h"

/* This class is used to compute all the occurrences of a pattern. */

/* One object of this class for each pattern (so this object is reused for
   all occurrences of the pattern) */

class GSubGraphIso{
 public:
  GPattern *pattern;   // the pattern
  GGraph *largegraph;  // a graph of the database

  // stats
  clock_t timeMatching; // number of clock ticks for the last call to "run"
  double totMatching;   // total number of clock ticks for all call to "run"
  long long int numMatchings; // total number of occurrence found for all call to "run" 
  long long int szMatchings; // total size of the occurrences (nb of edges) for all call to "run"

  // ******** Mapping between nodes of pattern and nodes of database graph.
  // If i is a node of the database graph, pattern_index[i] is 
  // the corresponding node in the pattern (or GNONODEID there is no
  // correspondance). If there is a correspondance, then:
  // -  (*largegraph_index)[offset +pattern_index[i]] = i
  // -  pattern_index[(*largegraph_index)[offset + i]] = i
  uint offset; // start of new occurrence in ::run, start of last occurrence outside ::run
  bool last_occ_valid; /* if false, means that the last call to ::run
			  returned 'false'.  So the content of
			  pattern_index and largegraph_index is only a partial
			  occurrence  */
  vector<GNodeID> pattern_index; 
  vector<GNodeID> *largegraph_index;
  /* for all i > pattern->graph.nodes.size(), pattern_index[i] = GNONODEID and
     for all i > offset + pattern->graph.nodes.size(), largegraph_index[i] = GNONODEID*/

  /* lastEdgeUsed[i] : index of the last edge from node largegraph_index[i]
     used in the current pattern (i is an index in the pattern graph and
     lastEdgeUsed[i] is an index in the database graph) */
  vector<GEdgeID> lastEdgeUsed; 
    
  GSubGraphIso(GPattern *pattern, vector<GNodeID> *occ_list); // define the pattern that must be found and the vector to store the occurrences
  ~GSubGraphIso();
    
  /* Returns true if an occurrence of the pattern is found in largegraph.
     largeid and edgeid indicate where the occurrence must be looked for in
     largegraph. The first edge of the pattern must already match. */
  bool run (GGraph *largegraph, GNodeID largeid, GEdgeID edgeid );
  void clear_occ_list(); // to fill largegraph_index[] with GNONODEID
 private:
  bool run3();
};

#endif
