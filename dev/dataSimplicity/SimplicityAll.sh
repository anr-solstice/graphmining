#!/bin/bash

#==============================================================================
#VARIABLE
#==============================================================================
## DIRECTORTY
DATADIR="data/"
MATDIR="data/dir/"
LOGDIR="data/stat/"
GRAPHDIR="data/graph/"
PLGDIR="data/graph/resultplg/"
GSPDIR="data/graph/resultgsp/"
PLGGRID="data/graph/resultplg/grid/"
PLGTRI="data/graph/resultplg/tri/"
GSPGRID="data/graph/resultgsp/grid/"
GSPKNN="data/graph/resultplg/knn/"
WEKADIR="wekafile/"
CSVDIR="csvfile/"
WEKAOUT="wekaresult/"
## LOG FILE
FULLLOG="SimplicityAll.out"
GENELOG="SimplicityGeneration.out"
WGRAPHLOG="SimplicityWriteGraph.out"
GRAPHMIN="SimplicityGraphMining.out"
GETOCC="GetOccurenct.out"
GSPGRIDLOG="GraphMiningGridGSP.log"
PLGGRIDLOG="GraphMiningGridPLG.log"
GSPKNNLOG="GraphMiningKNNGSP.log"
PLGTRILOG="GraphMiningTriPLG.log"
GRIDCLASSLOG="WekaClassifGrid.csv"
TRICLASSLOG="WekaClassifTri.csv"
# GRAPH EXT
GSP=".gsp"
PLG=".plg"
ARFF=".arff"

#==============================================================================
# FUNCTION
#==============================================================================
# GET OCCURENCES FUNCTION
#==============================================================================
get_occurence(){
echo "======================================================"
echo "!                     GET OCCURENCES                 !"
echo "======================================================"
for f in $PLGGRID*.plg; do
    echo "FILE : $f"
    tmp="${f%${PLG}}.csv"
    echo $(grep "o " $f | sed 's/o //g' | sed 's/ /,/g' | awk -F, '{print $1","$2}' >> $tmp)
    rm $f
done                   
mv ${PLGGRID}*.csv $PLGGRID$CSVDIR
#for f in $PLGTRI*.plg; do
#    echo "FILE : $f"
#    tmp="${f%${PLG}}.csv"
#    echo $(grep "o " $f | sed 's/o //g' | sed 's/ /,/g' | awk -F, '{print $1","$2}' >> $tmp)
#    rm $f
#done                   
#mv ${PLGTRI}*.csv $PLGTRI$CSVDIR
}

# GRAPH MINING FUNCTION
#==============================================================================
graph_mining(){
echo "======================================================"
echo "!                      GRID MINING                   !"
echo "======================================================"
echo "===== PLAGRAM  ====="
for f in ${GRAPHDIR}*B*S*Full.plg ; do
    echo "====="
    echo "FILE : $f"
    tmp="$PLGGRID${f#${GRAPHDIR}}";
    for i in 1000 750 500 400 300 250 200 150 100 ; do 
    	tmp="${tmp%${PLG}}"
    	echo "FREQ : $i"
        ./plagram -cqo -f $i $f $tmp-$i$PLG >> ${LOGDIR}$PLGGRIDLOG
    	echo "================================================== " >> ${LOGDIR}$PLGGRIDLOG
#		get_occurence
    done
    get_occurence
done
#echo "===== GSPAN ====== EXTENDED GRAPH"
#for f in ${GRAPHDIR}*B*S*Full.plg ; do 
#    echo "====="
#    echo "FILE : $f"
#    tmp="$GSPGRID${f#${GRAPHDIR}}"
#    for i in 1000  ; do 
#    	tmp="${tmp%${PLG}}"
#    	echo "FREQ : $i"
#        ./gSpan2005 $i $f $tmp-$i$PLG 60 60 >> ${LOGDIR}$GSPGRIDLOG
#    	echo "================================================== " >> ${LOGDIR}$GSPGRIDLOG
#    done
#done
#echo "===== GSPAN ===== NORMAL GRAPH"
#for f in ${GRAPHDIR}*B*S*Full.gsp ; do 
#    echo "====="
#    echo "FILE : $f"
#    tmp="$GSPGRID${f#${GRAPHDIR}}"
#    for i in 1000  ; do 
#        tmp="${tmp%${GSP}}"
#        echo "FREQ : $i"
#        ./gSpan2005 $i $f $tmp-$i$GSP 60 60 >>  ${LOGDIR}$GSPGRIDLOG
#        echo "================================================= " >> ${LOGDIR}$GSPGRIDLOG
#    done
#done
echo "======================================================"
echo "!                      TRI MINING                    !"
echo "======================================================"
echo "===== PLAGRAM ====="
for f in ${GRAPHDIR}*K*D*Full.plg ; do 
    echo "====="
    echo "FILE : $f"
    tmp="$PLGTRI${f#${GRAPHDIR}}"
    for i in 1000 750 500 400 300 250 200 150 100 ; do 
        tmp="${tmp%${PLG}}"
        echo "FREQ : $i"
        ./plagram -cqo -f $i $f $tmp-$i$PLG >>  ${LOGDIR}$PLGTRILOG
        echo "=============================================== " >> ${LOGDIR}$PLGTRILOG
		get_occurence
    done
done
#echo "======================================================"
#echo "!                     KNN MINING                     !"
#echo "======================================================"
#echo "===== GSPAN ====="
#for f in ${GRAPHDIR}*K*D*Full.gsp ; do
#    echo "====="
#    echo "FILE : $f"
#    tmp="$GSPKNN${f#${GRAPHDIR}}"
#    for i in 1000  ; do 
#        tmp="${tmp%${GSP}}"
#        echo "FREQ : $i"
#        ./gSpan2005 $i $f $tmp-$i$GSP 60 60 >> ${LOGDIR}$GSPKNNLOG
#        echo "=============================================== " >> ${LOGDIR}$GSPKNNLOG
#    done
#done
}

# WEKA CLASSIFICATION THROUGH SMO
#==============================================================================
weka_classif(){
for f in $PLGGRID$WEKADIR* ; do
    echo $f
    for classifier in functions.SMO
    do
    res="$PLGGRID$WEKAOUT${f#$PLGGRID$WEKADIR}"
    res="${res%${ARFF}}.out"
    echo $res
    java -cp ./weka.jar: weka.classifiers.$classifier -t $f -x 10 >> $res
    done
done
for f in $PLGTRI$WEKADIR* ; do
    echo $f
    for classifier in functions.SMO
    do
    res="$PLGTRI$WEKAOUT${f#$PLGTRI$WEKADIR}"
    res="${res%${ARFF}}.out"
    echo $res
    java -cp ./weka.jar: weka.classifiers.$classifier -t $f -x 10 >> $res
    done
done
}

# CONDENSE WEKA RES
#==============================================================================
condense_res(){
echo "BinSize, Step, Word Method, Word Number, Min Freq, Result" > tmp
for f in $PLGGRID$WEKAOUT*.out ; do
    line=$(grep -n "=== Stratified cross-validation ===" $f | cut -d ':' -f1);
    line=$(($line + 2))
    b=$(echo $line | cut -d ' ' -f1);
    cl=$(sed "$b!d" $f)
    res=${cl:${#cl}-10:10}
    IFS=-
    set $f 
    dataset=$2
    B=${dataset:1:1}
    S=${dataset:3:1}
    Wm=${dataset:5:1}
    W=${dataset:6}
    freq=${4%.out}
    echo "$B,$S,$Wm,$W,$freq,$res"  >> tmp
done
sed 's/ //g' tmp >> $LOGDIR$GRIDCLASSLOG
echo "K neighbor, Word Method, Word Number, Min Freq, Result" > tmp
for f in $PLGTRI$WEKAOUT*.out ; do
    line=$(grep -n "=== Stratified cross-validation ===" $f | cut -d ':' -f1);
    line=$(($line + 2))
    b=$(echo $line | cut -d ' ' -f1);
    cl=$(sed "$b!d" $f)
    res=${cl:${#cl}-10:10}
    IFS=-
    set $f 
    set $f 
    dataset=$2
    K=${dataset:1:1}
    Wm=${dataset:7:1}
    W=${dataset:8}
    freq=${4%.out}
    echo "$k,$Wm,$W,$freq,$res"  >>  tmp
done
sed 's/ //g' tmp >$LOGDIR$TRICLASSLOG
rm tmp
}

# MAIN FUNCTION3
#==============================================================================
main(){
#First, create all dir
mkdir $MATDIR $GRAPHDIR $PLGDIR $GSPDIR $PLGGRID $PLGTRI $GSPGRID $GSPKNN $PLGGRID$WEKADIR $PLGGRID$WEKAOUT $PLGTRI$WEKADIR $PLGTRI$WEKAOUT $PLGGRID$CSVDIR $PLGTRI$CSVDIR


# First compute all matlab matrix
matlab -nodisplay -nosplash -nojvm -r "SimplicityGeneration;exit;" 2>&1 | tee ${LOGDIR}$GENELOG
# Write graph full for all image in simplicity
matlab -nodisplay -nosplash -nojvm -r "SimplicityWriteGraph;exit;" 2>&1 | tee ${LOGDIR}$WGRAPHLOG
# Call graph mining function that mine all graph
#graph_mining 2>&1 | tee ${LOGDIR}$GRAPHMIN
# Getting all occurence from plagram.
#get_occurence 2>&1 | tee ${LOGDIR}$GETOCC
# Create Bag Of Graph
matlab -nodisplay -nosplash -nojvm -r "SimplicityWriteBOG;exit;" 2>&1 | tee ${LOGDIR}$WGRAPHLOG
# Classification through WEKA Functions.SMO
weka_classif
# Condense classification res
condense_res

}


# Min Freq : 1000 750 500 400 300 250 200 150 100 
mkdir $DATADIR $LOGDIR
touch $LOGDIR$FULLLOG
main 2>&1 | tee ${LOGDIR}$FULLLOG
