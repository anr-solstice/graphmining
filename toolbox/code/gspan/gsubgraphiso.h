/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef GSUBGRAPHISO_H
#define GSUBGRAPHISO_H
#include <map>
#include "global.h"
#include "ggraph.h"
#include "gsubgraphmatchproc.h"

/**
@author Siegfried Nijssen
*/
class GSubGraphIso{
public:
    GSubGraphMatchProc *proc;
    GGraph *smallgraph, *largegraph;
    GGraphCode *smallcode;
    bool foundone;

   //Only for experiments!!
    clock_t timeMatching;
    double totMatching; 
    long long int numMatchings;
    long long int szMatchings;
    
    /* SLOW: map<GNodeID,GNodeID> largegraphused; */
    vector<GNodeID> largegraphused;
    vector<GNodeID> smallgraphused;
    
    GSubGraphIso( GSubGraphMatchProc *proc, GGraph *smallgraph );

    ~GSubGraphIso();
    
    // returns true if one matching was found. Smallgraph must be connected
    bool run ( GGraph *largegraph );
private:
    void init ( GGraph *largegraph );
    bool run3 ( GNodeID smallpos );
    bool run2 ( GNodeID largeid );
};

#endif
