#!/bin/bash

DIR=./*.txt
for f in $DIR
do
    echo $f;
    echo "=========================================================" >> condense.txt ;
    echo "=========================================================" >> condense.txt ;
    echo "=========================================================" >> condense.txt ;
    echo "=========================================================" >> condense.txt ;
    echo "$f" >> condense.txt;
    echo "=========================================================" >> condense.txt ;
    echo "=========================================================" >> condense.txt ;
    line=$(grep -n "Confusion Matrix" $f | cut -d ':' -f1);
    echo $line;
    for i in {1..7}
    do 
        case $i in 
        1 ) echo "******************* Classifier = rules.ZeroR" >> condense.txt;
            b=$(echo $line | cut -d ' ' -f1);
            b=$(($b-12));e=$(($b+5));
            sed -n $b,$e'p' $f >> condense.txt ;
            b=$(echo $line | cut -d ' ' -f2);
            b=$(($b-12));e=$(($b+5));
            sed -n $b,$e'p' $f >> condense.txt ;;
        2 ) echo "******************* Classifier = trees.J48" >> condense.txt;
            b=$(echo $line | cut -d ' ' -f3);
            b=$(($b-12));e=$(($b+5));
            sed -n $b,$e'p' $f >> condense.txt ;
            b=$(echo $line | cut -d ' ' -f4);
            b=$(($b-12));e=$(($b+5));
            sed -n $b,$e'p' $f >> condense.txt ;;
        3 ) echo "******************* Classifier = bayes.NaiveBayes" >> condense.txt;
            b=$(echo $line | cut -d ' ' -f5);
            b=$(($b-12));e=$(($b+5));
            sed -n $b,$e'p' $f >> condense.txt ;
            b=$(echo $line | cut -d ' ' -f6);
            b=$(($b-12));e=$(($b+5));
            sed -n $b,$e'p' $f >> condense.txt ;;
        4 ) echo "******************* Classifier = rules.JRip" >> condense.txt;
            b=$(echo $line | cut -d ' ' -f7);
            b=$(($b-12));e=$(($b+5));
            sed -n $b,$e'p' $f >> condense.txt ;
            b=$(echo $line | cut -d ' ' -f8);
            b=$(($b-12));e=$(($b+5));
            sed -n $b,$e'p' $f >> condense.txt ;;
        5 ) echo "******************* Classifier = meta.ClassificationViaRegression" >> condense.txt;
            b=$(echo $line | cut -d ' ' -f9);
            b=$(($b-12));e=$(($b+5));
            sed -n $b,$e'p' $f >> condense.txt ;
            b=$(echo $line | cut -d ' ' -f10);
            b=$(($b-12));e=$(($b+5));
            sed -n $b,$e'p' $f >> condense.txt ;;
        6 ) echo "******************* Classifier = functions.SMO" >> condense.txt;
            b=$(echo $line | cut -d ' ' -f11);
            b=$(($b-12));e=$(($b+5));
            sed -n $b,$e'p' $f >> condense.txt ;
            b=$(echo $line | cut -d ' ' -f12);
            b=$(($b-12));e=$(($b+5));
            sed -n $b,$e'p' $f >> condense.txt ;;
        7 ) echo "******************* Classifier = lazy.KStar" >> condense.txt;
            b=$(echo $line | cut -d ' ' -f13);
            b=$(($b-12));e=$(($b+5));
            sed -n $b,$e'p' $f >> condense.txt ;
            b=$(echo $line | cut -d ' ' -f14);
            b=$(($b-12));e=$(($b+5));
            sed -n $b,$e'p' $f >> condense.txt ;;
        esac
    done
done

#>> condense.txt ;

