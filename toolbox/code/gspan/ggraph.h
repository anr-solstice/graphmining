/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef GGRAPH_H
#define GGRAPH_H

#include <stdlib.h> // new baptiste
#include <iostream>
#include <vector>
#include <istream>
#include <ostream>
#include "global.h"
#include "ggraphcode.h"
#include "../data/data.h" 

struct GGraphEdge {
    GEdgeData data;
    GNodeID nodeid;
};

struct GGraphNode {
    GNodeData data;
    vector<GGraphEdge> edges;
};

/**
@author Siegfried Nijssen
*/
class GGraph{
public:
    GGraph();

    ~GGraph();
    void addNode ( GNodeData nodedata );
    void delNode ();
    void addEdge ( GNodeID from, GNodeID to, GEdgeData edgedata );
    void delEdge ( GNodeID from, GNodeID to );
    bool isEdge ( GNodeID from, GNodeID to, GEdgeData data );
        
    vector<GGraphNode> nodes;
    
    GGraphCode *code; // a code for this graph; NULL if not available. Code is not automatically updated.

    //new
    void read(Data *data);

};

//istream& operator>>(istream& stream, GGraph &graph );
ostream& operator<<(ostream& stream, GGraph &graph );

#endif
