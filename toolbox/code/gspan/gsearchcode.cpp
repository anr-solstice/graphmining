/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "gsearchcode.h"

GSearchCode::GSearchCode()
{
  graph.code = this;
}


GSearchCode::~GSearchCode()
{
}

void GSearchCode::setup ( const GToken &token ) {
  if ( !graph.nodes.empty () ) 
    graph.delNode ();
  graph.addNode ( token.fromdata );
}

void GSearchCode::push_back ( const GToken &token ) {
  tokens.push_back ( token );
  // we assume that token.to == graph->nodes.size ()
  if ( token.direction == forward )
    graph.addNode ( token.todata ); 
  graph.addEdge ( token.from, token.to, token.data );
}

void GSearchCode::pop_back () {
  GToken &token = tokens.back ();
  graph.delEdge ( token.from, token.to );
  if ( token.direction == forward )
    graph.delNode ();
  tokens.pop_back ();
}

