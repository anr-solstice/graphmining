function [ conf ] = config()
%% Function that initialize configuration
%
% Loading vlfeat for matlab
%run('vl_setup');

% Don't forget to include the following folder to the matlab path : 
% - PFE-fouille de Graphe
% - pascal_dataset : Wich contain the following folder :
%                   - VOCtrainval_06-Nov-2007
%                   - VOCtest_06-Nov-2007
%                   - VOCdevkit_08-Jun-2007
                    
% =========================================================================
%% Configuration ==========================================================
% Base directory for training (Image and Set)
conf.trainval.basedirimg = 'pascal_dataset/VOCtrainval_06-Nov-2007/VOC2007/JPEGImages/';
conf.test.basedirimg = 'pascal_dataset/VOCtest_06-Nov-2007/VOC2007/JPEGImages/';
% Base directory for testing (Image and Set)
conf.train.basedirset = 'pascal_dataset/VOCtrainval_06-Nov-2007/VOC2007/ImageSets/Main/';
conf.test.basedirset = 'pascal_dataset/VOCtest_06-Nov-2007/VOC2007/ImageSets/Main/';
% List of categoy
conf.class = {
    'aeroplane'   ; 'bicycle'     ; 'bird'        ; 'boat'        ;
    'bottle'      ; 'bus'         ; 'car'         ; 'cat'         ;
    'chair'       ; 'cow'         ; 'diningtable' ; 'dog'         ;
    'horse'       ; 'motorbike'   ; 'person'      ; 'pottedplant' ; 
    'sheep'       ; 'sofa'        ; 'train'       ; 'tvmonitor'  };
% Number of class
conf.nbclasse = size(conf.class,1);

% =========================================================================
%% Init image data ========================================================
% Kind of set provide by pascal
%   -Train : Training dataset
%   -Val : Validation training dataset (suggested)
%   -Trainval : Union of the two previous dataset
%   -Test : Testing dataset
% imgclass = Matrix 2xN such as ImgID, ClassID
%       ClassID correspond to the sorted class above.
conf.data.train.img = dlmread(sprintf('%strain.txt',...
                                            char(conf.train.basedirset)));
conf.data.val.img = dlmread(sprintf('%sval.txt',...
                                            char(conf.train.basedirset)));
conf.data.test.img = dlmread(sprintf('%stest.txt',...
                                            char(conf.test.basedirset)));

for i=1:conf.nbclasse
    pos = dlmread(sprintf('%s%s_train.txt',...
                            char(conf.train.basedirset),...
                            char(conf.class{i})));
    posidx=find(1==pos(:,2));
    conf.data.train.imgClass(posidx)=i;
    pos = dlmread(sprintf('%s%s_val.txt',...
                            char(conf.train.basedirset),...
                            char(conf.class{i})));
    posidx=find(1==pos(:,2));
    conf.data.val.imgClass(posidx)=i;
    pos = dlmread(sprintf('%s%s_test.txt',...
                            char(conf.test.basedirset),...
                            char(conf.class{i})));
    posidx=find(1==pos(:,2));
    conf.data.test.imgClass(posidx)=i;
end

conf.data.train.imgClass = conf.data.train.imgClass';
conf.data.val.imgClass = conf.data.val.imgClass';
conf.data.test.imgClass = conf.data.test.imgClass';

conf.data.train.full = cat(2,conf.data.train.img, conf.data.train.imgClass);
conf.data.val.full = cat(2,conf.data.val.img, conf.data.val.imgClass);
conf.data.test.full = cat(2,conf.data.test.img, conf.data.test.imgClass);

end % End of function
