/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "gsubgraphiso.h"
#include <queue>

GSubGraphIso::GSubGraphIso( GSubGraphMatchProc *proc, GGraph *smallgraph )
{
  this->proc = proc;
  this->smallgraph = smallgraph;
  if ( smallgraph->code )
    smallcode = smallgraph->code; 

   totMatching  = 0;
   numMatchings = 0;
   szMatchings = 0;
}


GSubGraphIso::~GSubGraphIso()
{
  if ( !smallgraph->code )
    delete smallcode;
}

struct GSubGraphPriority {
  int pos; // order of insertion in the queue
  GToken token;
  int degree; // the degree
};

bool operator< ( const GSubGraphPriority &p1, const GSubGraphPriority &p2 ) {
  return 
           ( p1.token.todata < p2.token.todata ||
             ( p1.token.todata == p2.token.todata &&
               ( p1.degree < p2.degree ||
                 ( p1.degree == p2.degree &&
                   ( p1.token.data < p2.token.data ||
                     ( p1.token.data == p2.token.data &&
                       ( p1.pos > p2.pos ) // prefer the first inserted
                 )
               )
             )
           )
         )
        );
}

void GSubGraphIso::init ( GGraph *largegraph )
{
  this->largegraph = largegraph;
  foundone = false;
  
  /* SLOW: largegraphused.clear (); */
  largegraphused.resize ( 0 );
  largegraphused.resize ( largegraph->nodes.size (), GNONODEID );
  smallgraphused.resize ( 0 );
  smallgraphused.resize ( smallgraph->nodes.size () );
}

/*!
    \fn GSubGraphIso::run ( const GGraph &smallgraph, const GGraph &largegraph )
 */
bool GSubGraphIso::run ( GGraph *largegraph )
{

  timeMatching = clock();

  init ( largegraph );
  
  // alternatively, we could use a run function which uses a tid list
  for ( int i = 0, size = largegraph->nodes.size (); i < size; ++i )
 
     if ( smallcode->tokens[0].fromdata == largegraph->nodes[i].data && run2 ( i ) )
              return true;

  timeMatching = clock() - timeMatching;
  totMatching += timeMatching;

  return foundone;
}

bool GSubGraphIso::run2 ( GNodeID largeid )
 {
  
  vector<GGraphEdge> &edges = largegraph->nodes[largeid].edges;
  GToken &token = smallcode->tokens[0];
  
  largegraphused[largeid] = token.from;
  smallgraphused[token.from] = largeid;
  
 for ( int i = 0, size = edges.size (); i < size; ++i ) 
 {
    if ( edges[i].data == token.data &&
         largegraph->nodes[edges[i].nodeid].data == token.todata ) 
    {
      largegraphused[edges[i].nodeid] = token.to;
      smallgraphused[token.to] = edges[i].nodeid;

      if ( run3 ( 1 ) )
        return true;
      
      smallgraphused[token.to] = GNONODEID;
      /* SLOW: largegraphused.erase ( edges[i].nodeid ); */
      largegraphused[edges[i].nodeid] = GNONODEID;
    }         
 }
  
 smallgraphused[token.from] = GNONODEID;
 /* SLOW: largegraphused.erase ( largeid ); */
 largegraphused[largeid] = GNONODEID;

 return false;

}

bool GSubGraphIso::run3 ( GNodeID smallpos ) 
{

 if ( smallpos == smallcode->tokens.size () ) 
 {
    numMatchings++;
    szMatchings += smallcode->tokens.size ();
//	cout << numMatchings << "\t" << szMatchings << endl;

    foundone = true;
    if ( proc->process ( this ) )
      return true;
  }
  else 
  {

    GToken &token = smallcode->tokens[smallpos];
    
    if ( token.direction == backward ) 
   {
      // check cycle
      if ( largegraph->isEdge ( smallgraphused[token.from], smallgraphused[token.to], token.data ) )
        return run3 ( smallpos + 1 );
    }
    else 
   {
      vector<GGraphEdge> &edges = largegraph->nodes[smallgraphused[token.from]].edges;
      
      // try to match the next node
      for ( int i = 0, size = edges.size (); i < size; ++i )
      {

        if ( edges[i].data == token.data &&
             largegraph->nodes[edges[i].nodeid].data == token.todata &&
             largegraphused[edges[i].nodeid] == GNONODEID
             /* SLOW: largegraphused.find ( edges[i].nodeid ) == largegraphused.end () */ ) {
          largegraphused[edges[i].nodeid] = token.to;
          smallgraphused[token.to] = edges[i].nodeid;
          
          if ( run3 ( smallpos + 1 ) )
            return true;
          
          smallgraphused[token.to] = GNONODEID;
          largegraphused[edges[i].nodeid] = GNONODEID;
            /* SLOW: largegraphused.erase ( edges[i].nodeid ); */
        }
      }
    }
  }

  return false;
}
