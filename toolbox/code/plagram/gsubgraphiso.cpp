/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *   Copyright (C) 2010,2011 by Adriana Prado and Baptiste Jeudy           *
 *   baptiste.jeudy at univ-st-etienne fr                                  *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "gsubgraphiso.h"
#include "ggraphcode.h"
#include <queue>

GSubGraphIso::GSubGraphIso( GPattern *ppattern, vector<GNodeID> *occ_list):
  pattern (ppattern),
  largegraph (NULL),
  totMatching (0), numMatchings (0), szMatchings (0), offset(1), last_occ_valid (false)  
{
  largegraph_index = occ_list;
  //if (largegraph_index->size() < ppattern->graph.nodes.size()) 
  //  largegraph_index->resize(ppattern->graph.nodes.size(), GNONODEID);
  lastEdgeUsed.resize(ppattern->graph.nodes.size());
}


GSubGraphIso::~GSubGraphIso()
{
}

void GSubGraphIso::clear_occ_list() { // to fill largegraph_index[] with GNONODEID
  /* largegraph_index is already filled with GNONODEID 
     for i>=  offset+pattern->graph.nodes.size(). */
  for (uint i = 0; i < offset+pattern->graph.nodes.size() 
	 && i < largegraph_index->size(); i++) 
    (*largegraph_index)[i] = GNONODEID;
}


bool GSubGraphIso::run ( GGraph *newlargegraph, GNodeID largeid, GEdgeID edgeid )
{ // called in plagram.cpp
  // largegraph = current database graph
  // this->pattern = current pattern P
  /*
    This function check if an occurrence of pattern P occurs at (largeid,edgeid)
    in largegraph.
    
    Return true if:
    - an occurrence of P is found in largegraph where the first node and
      first edge of P match respectively largeid and edgeid in largegraph

    Assume that the first edge of P already match.
  */

  timeMatching = clock();

  if (largegraph != NULL) {
  // fill pattern_index (and largegraph_index if necessary) with GNONODEID
    for (uint i = offset; i < offset + pattern->graph.nodes.size(); i++) {
      if (largegraph_index->at(i) != GNONODEID) {
	if (pattern_index.at((*largegraph_index)[i]) != i - offset)
	  cerr << "****************** INTERNAL ERROR: BUG" << endl;
	pattern_index[(*largegraph_index)[i]] = GNONODEID;
	if (! last_occ_valid) // delete last occ (which was incomplete)
	  (*largegraph_index)[i] = GNONODEID;
      }
    }
    if (last_occ_valid) 
      offset += pattern->graph.nodes.size() + 1;
  }
  // pattern_index and largegraph_index are clean (ie full of GNONODEID).
  
  // resize largegraph_index if necessary
  if (largegraph_index->size() < offset + pattern->graph.nodes.size())
    largegraph_index->resize(offset + pattern->graph.nodes.size(), GNONODEID );
  // size of largegraph_index ok.

  if (largegraph != newlargegraph) {
    largegraph = newlargegraph;
    if (pattern_index.size() < largegraph->nodes.size())
      pattern_index.resize ( largegraph->nodes.size (), GNONODEID );
  }
  // size of pattern_index ok.
  
  (*largegraph_index)[offset-1] = largegraph->frameid;
  
  GToken &token = pattern->tokens[0]; // the first edge of the pattern
  GGraphEdge &edge = largegraph->nodes[largeid].edges[edgeid];
  
  // token.from in P maps to largeid in largegraph
  pattern_index[largeid] = token.from; 
  (*largegraph_index)[offset + token.from] = largeid;
  
  // and token.to in P maps to edge.nodeid in largegraph
  pattern_index[edge.dest_nid] = token.to;
  (*largegraph_index)[offset + token.to] = edge.dest_nid;

  //keep track of the last edge used in the match
  lastEdgeUsed[token.from] = edgeid;
  lastEdgeUsed[token.to] = edge.eq_eid;

  last_occ_valid = run3();

  timeMatching = clock() - timeMatching;
  totMatching += timeMatching;

  return last_occ_valid;
}


bool GSubGraphIso::run3 () 
{ /* match one edge at each iteration of the while loop (the matched edge is the one
     with id smallpos in pattern.tokens, the edges with id < smallpos have
     already been matched).
  */
  GEdgeID lastEdgeId, edgeid; 	  
  GGraphEdge *edge;

  uint smallpos = 1;

  while (1) {

    if ( smallpos == pattern->tokens.size() ) { 
      // the matching succeded  
      /* Up to now, for an internal node x (of the pattern) corresponding
	 to node y (in the graph), we only checked that the neighbors list
	 of x is included in y. We must now check they are equal.  For this
	 we need to check that the degree of x is equal to the degree of y.
      */ 
    
      for (GNodeID x = 0; x < pattern->graph.nodes.size(); x++)
	if (! pattern->graph.nodes[x].outerface)
	  if (pattern->graph.nodes[x].edges.size() != 
	      largegraph->nodes[(*largegraph_index)[offset+x]].edges.size())
	    return false; // not a real match
    
      numMatchings++;
      szMatchings += pattern->tokens.size();
      return true;
    }
  
    // else tries to match the next edge of pattern
    GToken &token = pattern->tokens[smallpos];
		
    lastEdgeId = lastEdgeUsed[token.from]; 
    edge = largegraph->nextEdge((*largegraph_index)[offset+token.from], 
				lastEdgeId, &edgeid);
	
    if (edge == NULL) return false;

    if ( token.direction == backward ) {
      // check the matching of the "to" node
      if (edge->dest_nid == (*largegraph_index)[offset+token.to]
	  && edge->data == token.data
	  && edge->eq_eid != lastEdgeUsed[token.to]) 
	{
	  lastEdgeUsed[token.from] = edgeid;
	  smallpos++;
	}
      else return false;
    } else { //forward edge 
      // try to match the next node
      if ( edge->data == token.data &&
	   largegraph->nodes[edge->dest_nid].data == token.todata &&
	   pattern_index[edge->dest_nid] == GNONODEID)
	{
	  pattern_index[edge->dest_nid] = token.to;
	  (*largegraph_index)[offset+token.to] = edge->dest_nid;
	      
	  lastEdgeUsed[token.from] = edgeid;
	  lastEdgeUsed[token.to] = edge->eq_eid;
	      
	  smallpos++;
	}
      else return false;
    }
  }
}
