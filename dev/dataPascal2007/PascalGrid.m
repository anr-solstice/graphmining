function [ result ] = lPascalGrid(nbwords, binSize, step, clust,...
                                     svmT, svmS, svmC, svmG, svmP)
%% Pascalicity Grid ========================================================
% Function for applying dense sift to Pascalicity image set
%
%Syntaxe
% [ result ] = PascalGrid(nbwords)
% [ result ] = PascalGrid(nbwords, binSize)
% [ result ] = PascalGrid(nbwords, binSize, Step)
% [ result ] = PascalGrid(nbwords, binSize, Step, svmC)
% [ result ] = PascalGrid(nbwords, binSize, Step, svmC, svmG)
% [ result ] = PascalGrid(nbwords, binSize, Step, svmC, svmG, svmP)
% [ result ] = PascalGrid(nbwords, binSize, Step, svmC, svmG, svmP, svmT)
%
%Parameters
% If not specified, parameter are set to default value, specified into
% squared brackets.
% Parameters must be in the exact defined order
% - nbWorkds (REQUIRED)
%       -> Number of words for our vocabulary 
% - clust   ['knn']
%       -> Define method to compute nearest neighbor search.
% - binSize [3]
%       -> Size, in px, of bin from SIFT descriptor.
% - Step    [1]
%       -> Step, in px, between two descriptor
% - svmT    [0]
%       -> Value of parameter T for libsvm that define kernel to use
%           --> Possible value : 
%	0 -- linear: u'*v
%	1 -- polynomial: (gamma*u'*v + coef0)^degree
%	2 -- radial basis function: exp(-gamma*|u-v|^2)
%	3 -- sigmoid: tanh(gamma*u'*v + coef0)
% - svmS    [0]
%       -> Value of parameter S for libsvm that define type of SVM
%           --> Possible value
%	0 -- C-SVC
%	1 -- nu-SVC
%	2 -- one-class SVM
%	3 -- epsilon-SVR
%	4 -- nu-SVR
% - svmC    [1] 
%       -> Value of C for libsvm
% - svmG    [1]
%       -> Value of parameter G, gamma, for libsvm 
% - svmP    [0.01]
%       -> Value of parameter P, epsilon, for libsvm

%Description
% Return a set of frame f{i} = [x y scale angle] and sift descriptors
% d{i} = 1x128 for all image in dirimg specified by id in the first column
% of limg.
% For more information, please refer to vl_dsift documentation.

% =========================================================================
%% Loading vlfeat for matlab
% run('vl_setup');

% Don't forget to include the following folder to the matlab path : 
% - PFE-fouille de Graphe
% - Pascalicity 
%
% =========================================================================

%% Configuration ==========================================================
% Base directory for training (Image and Set)
conf.basedirimg = 'pascal_dataset/VOCtrainval_06-Nov-2007/VOC2007/JPEGImages/';
%conf.test.basedirimg = 'pascal_dataset/VOCtest_06-Nov-2007/VOC2007/JPEGImages/';
% Base directory for testing (Image and Set)
conf.basedirset = 'pascal_dataset/VOCtrainval_06-Nov-2007/VOC2007/ImageSets/Main/';
%conf.test.basedirset = 'pascal_dataset/VOCtest_06-Nov-2007/VOC2007/ImageSets/Main/';
% List of categoy
conf.class = {
    'aeroplane'   ; 'bicycle'     ; 'bird'        ; 'boat'        ;
    'bottle'      ; 'bus'         ; 'car'         ; 'cat'         ;
    'chair'       ; 'cow'         ; 'diningtable' ; 'dog'         ;
    'horse'       ; 'motorbike'   ; 'person'      ; 'pottedplant' ; 
    'sheep'       ; 'sofa'        ; 'train'       ; 'tvmonitor'  };
% Number of class
conf.nbclasse = size(conf.class,1);

% =========================================================================
%% Init image data ========================================================
% Kind of set provide by pascal
%   -Train : Training dataset
%   -Val : Validation training dataset (suggested)
%   -Trainval : Union of the two previous dataset
%   -Test : Testing dataset
% imgclass = Matrix 2xN such as ImgID, ClassID
%       ClassID correspond to the sorted class above.
conf.train.imgIdx = dlmread(sprintf('%strain.txt',...
                                            char(conf.train.basedirset)));
conf.val.imgIdx = dlmread(sprintf('%sval.txt',...
                                            char(conf.train.basedirset)));
conf.full.imgIdx = dlmread(sprintf('%strainval.txt',...
                                            char(conf.test.basedirset)));

for i=1:conf.nbclasse
    pos = dlmread(sprintf('%s%s_train.txt',...
                            char(conf.train.basedirset),...
                            char(conf.class{i})));
    posidx=find(1==pos(:,2));
    conf.data.train.imgClass(posidx)=i;
    pos = dlmread(sprintf('%s%s_val.txt',...
                            char(conf.train.basedirset),...
                            char(conf.class{i})));
    posidx=find(1==pos(:,2));
    conf.data.val.imgClass(posidx)=i;
    pos = dlmread(sprintf('%s%s_test.txt',...
                            char(conf.test.basedirset),...
                            char(conf.class{i})));
    posidx=find(1==pos(:,2));
    conf.data.test.imgClass(posidx)=i;
end

conf.data.train.imgClass = conf.data.train.imgClass';
conf.data.val.imgClass = conf.data.val.imgClass';
conf.data.test.imgClass = conf.data.test.imgClass';

conf.data.train.full = cat(2,conf.data.train.img, conf.data.train.imgClass);
conf.data.val.full = cat(2,conf.data.val.img, conf.data.val.imgClass);
conf.data.test.full = cat(2,conf.data.test.img, conf.data.test.imgClass);

% =========================================================================
%% Init image data ========================================================
% Kind of set for Pascalicity
%   -Train : Training dataset
%   -Val : Validation training dataset (suggested)
conf.train.img = dlmread(sprintf('%sPascalicityTrain.csv',...
                                  char(conf.basedirimg)));
%conf.train.img = conf.train.img(1:100,:);
conf.train.imgIdx = conf.train.img(:,1);
conf.train.imgClass = conf.train.img(:,2);
conf.train.nbImg = size(conf.train.img,1);

conf.val.img = dlmread(sprintf('%sPascalicityVal.csv',...
                                  char(conf.basedirimg)));
%conf.val.img = conf.val.img(1:100,:);
conf.val.imgIdx = conf.val.img(:,1);
conf.val.imgClass = conf.val.img(:,2);
conf.val.nbImg = size(conf.val.img,1);

conf.full.img = dlmread(sprintf('%sPascalicityFull.csv',...
                                  char(conf.basedirimg)));
%conf.full.img = conf.full.img(1:100,:);
conf.full.imgIdx = conf.full.img(:,1);
conf.full.imgClass = conf.full.img(:,2);
conf.full.nbImg = size(conf.full.img,1);

% =========================================================================
%% Configurable variable ==================================================
conf.quantizer = 'kdtree' ; % 'kdtree' by default

% Default value : 
conf.dsift.bin = 3;
conf.dsift.step = 1;

conf.svm.t = '0' ; % Linear SVM
conf.svm.s = '0' ; % C-SVC
conf.svm.c = '1' ;
conf.svm.g = '4' ;
conf.svm.p = '0.01' ;

conf.kmean.nbwords = nbwords;

switch nargin 
    case 2
        conf.dsift.bin = binSize;
    case 3
        conf.dsift.bin = binSize;
        conf.dsift.step = step;
    case 4
        conf.quantizer = clust; 
        conf.dsift.bin = binSize;
        conf.dsift.step = step;

        conf.svm.t = sprintf('%s', svmT) ;
    case 5
        conf.quantizer = clust; 
        conf.dsift.bin = binSize;
        conf.dsift.step = step;

        conf.svm.t = sprintf('%s', svmT) ;
    case 6
        conf.quantizer = clust; 
        conf.dsift.bin = binSize;
        conf.dsift.step = step;

        conf.svm.t = sprintf('%s', svmT) ;
        conf.svm.s = sprintf('%s', svmS) ;
    case 7
        conf.quantizer = clust; 
        conf.dsift.bin = binSize;
        conf.dsift.step = step;

        conf.svm.t = sprintf('%s', svmT) ;
        conf.svm.s = sprintf('%s', svmS) ;
        conf.svm.c = sprintf('%s', svmC) ;
    case 8
        conf.quantizer = clust; 
        conf.dsift.bin = binSize;
        conf.dsift.step = step;

        conf.svm.t = sprintf('%s', svmT) ;
        conf.svm.s = sprintf('%s', svmS) ;
        conf.svm.c = sprintf('%s', svmC) ;
        conf.svm.g = sprintf('%s', svmG) ;
    case 9
        conf.quantizer = clust; 
        conf.dsift.bin = binSize;
        conf.dsift.step = step;

        conf.svm.t = sprintf('%s', svmT) ;
        conf.svm.s = sprintf('%s', svmS) ;
        conf.svm.c = sprintf('%s', svmC) ;
        conf.svm.g = sprintf('%s', svmG) ;
        conf.svm.p = sprintf('%s', svmP) ;
end
% =========================================================================
%% Create file with stats =================================================
tmp.fID = fopen(sprintf('PascalB%dS%dW%d.txt',conf.dsift.bin, ...
                conf.dsift.step, conf.kmean.nbwords),'a');
tmp.filename = fopen(tmp.fID);
% =========================================================================
%% Construct vocab ========================================================
conf.kmean =  vocab (conf, conf.train.nbImg, conf.kmean.nbwords,...
                    sprintf('PascalB%dS%dW%d.txt',conf.dsift.bin, ...
                    conf.dsift.step, conf.kmean.nbwords));
% =========================================================================
%% Compute Hist for Train img =============================================
train = hists(conf,conf.train, sprintf('PascalB%dS%dW%d.txt',conf.dsift.bin, ...
                               conf.dsift.step, conf.kmean.nbwords));  

%% Compute Hist for validation img ========================================
val = hists(conf,conf.val, sprintf('PascalB%dS%dW%d.txt',conf.dsift.bin, ...
                           conf.dsift.step, conf.kmean.nbwords));  

%% Training & Testing SVM =================================================
fprintf(tmp.fID,'%% === Train SVM, , \n');
cinit = clock ;
svm.model = ovrtrain(conf.train.imgClass, train.histsknn',...
            sprintf('-s %s -t %s -g %s -c %s -p %s',...
            conf.svm.t, conf.svm.s, conf.svm.g,...
            conf.svm.c, conf.svm.p));
cend = clock ;
fprintf(tmp.fID,'%Training SVM,%d, \n', etime(cend,cinit));

fprintf(tmp.fID,'%% === Test SVM, , \n');
[svm.res.pred svm.res.ac svm.res.decv] = ovrpredict(conf.val.imgClass,...
                      val.histsknn', svm.model);
fprintf(tmp.fID,'%Testing SVM,%d, \n', etime(cend,cinit));
       
% % Compute the confusion matrix
% idx = sub2ind([conf.nbclasse, conf.nbclasse], ...
%               conf.val.imgClass, svm.res.pred) ;
% confus = zeros(conf.nbclasse) ;
% confus = vl_binsum(confus, ones(size(idx)), idx) ;
% 
% % Plots
% h=figure(1) ; clf;
% imagesc(confus) ;
% title(sprintf('Confusion matrix (%.2f %% accuracy)', ...
%               1000 * mean(diag(confus)/conf.val.nbImg) )) ;
% saveas(h,sprintf('PascalB%dS%dW%d',conf.dsift.bin, ...             
%           conf.dsift.step, conf.kmean.nbwords),'jpg');
%% Write weka file for 
fprintf(tmp.fID, '%% === Writing weka file');
[ tmp.errmsg ] = weka (train.histsknn, conf.class, conf.train.img,...
                    sprintf('Pascal-GridB%dS%d-Train-%dW',...
                    conf.dsift.bin, conf.dsift.step, conf.kmean.nbwords));
fprintf(tmp.fID,'%s', tmp.errmsg);
[ tmp.errmsg ] = weka (val.histsknn, conf.class, conf.val.img,...
                    sprintf('Pascal-GridB%dS%d-Val-%dW',...
                    conf.dsift.bin, conf.dsift.step, conf.kmean.nbwords));
fprintf(tmp.fID,'%s', tmp.errmsg);
fclose(tmp.fID);

result.conf = conf;
result.train = train;
result.val = val;
result.svm = svm ;

end % End of function
