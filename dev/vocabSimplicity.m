function [ vocab , kdtree ] = vocabSimplicity (conf, nbimg, filename)
%% Function that create Bag of Word (BOW) 
%
%Syntaxe
% [ m ] = vocab (conf, )
% [ m ] = vocab (conf, nbImg)
% [ m ] = vocab (conf, nbImg, filename)
%
%Parameters
% All parameter are required
% - conf (required)
%       -> Configuration struct. Cf config parfor more information.
% TODO : Complete this documentation
% - nbimg 
%       -> Number of image to use parfor vocab
% - filename
%       -> Filename where clocks stats will be written
%
%Description
% =========================================================================
% !!!!!!!!!                      TODO                             !!!!!!!!!
% =========================================================================

%% Construct vocab ========================================================
%d = {}; % Descriptor of all image
%c = []; % Clock
tmp.sel = randperm(conf.train.nbImg,nbimg) ; % Random selection of nbimg 

% If filename given
if ( nargin == 1 )
    nbimg = conf.train.nbImg;
end 
if ( nargin == 3 )
    tmp.fileId = fopen(sprintf('%s',filename),'a');
end

tmp.cinit = clock;
if strcmp(conf.vocab.method,'grid')
    parfor i=1:nbimg
        c1 = clock ;
        fprintf('Applying vl_dsift to %d.jpg, image %04d / %04d \n',...
        	      conf.train.imgIdx(tmp.sel(i)),i,nbimg);
        imc=imread(sprintf('%s%d.jpg', conf.train.basedirimg, conf.train.imgIdx(tmp.sel(i))));
        % Convert into gray image
        imb = single(rgb2gray(imc)) ;
        % Applying dsift
        [~,d{i}] = vl_dsift(imb,'size', conf.dsift.binSize,...
                                'step', conf.dsift.step);
        c2 = clock;
        c(i) = etime(c2,c1);
    end
else
    parfor i=1:nbimg
        c1 = clock ;
        fprintf('Applying vl_sift to %d.jpg, image %04d / %04d \n',...
                 conf.train.imgIdx(tmp.sel(i)),i,nbimg);
        imc=imread(sprintf('%s%d.jpg', conf.train.basedirimg, conf.train.imgIdx(tmp.sel(i))));
        % Convert into gray image
        imb = single(rgb2gray(imc)) ;
        % Applying dsift
        [~,d{i}] = vl_sift(imb);
        c2 = clock;
        c(i) = etime(c2,c1);
    end
end      
tmp.cend = clock;

tmp.cmean = mean(c);

if ( nargin == 3 )
    fprintf(tmp.fileId,'%f,',etime(tmp.cend,tmp.cinit));
else 
    fprintf('# K-MEANS - VLSIFT ============================\nvl_sift,%03d,%03d. \n',...
             etime(tmp.cend,tmp.cinit), tmp.cmean);
end 

% =========================================================================
%% Data Model Initialisation ==============================================
% Concat all descriptor into one matrix. Converted to single parfor k-means.
% Take 500000 random desc.
tmp.cinit = clock;
d = single(vl_colsubset(cat(2, d{:}), 500000));
tmp.cend = clock;

if ( nargin == 3 ) 
    fprintf(tmp.fileId,'%f,',etime(tmp.cend,tmp.cinit));
else 
    fprintf('Concat,%d\n',etime(tmp.cend,tmp.cinit));
end

% =========================================================================
%% Applying K-means =======================================================
% C = Center, A = Assignements
tmp.cinit = clock;
[ vocab , ~ ] = vl_kmeans(d, conf.vocab.nbwords);
kdtree = vl_kdtreebuild(vocab);
tmp.cend = clock;

if ( nargin == 3 )
    fprintf(tmp.fileId,'%f,',etime(tmp.cend,tmp.cinit));
    fclose(tmp.fileId);
else
    fprintf('# K-MEANS - VLKMEANS ============================\nvl_kmeans,%04d,\n',...
            etime(tmp.cend,tmp.cinit));
end

% =========================================================================
end % Enf of function
