/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *   Copyright (C) 2010,2011 by Adriana Prado and Baptiste Jeudy           *
 *   baptiste.jeudy at univ-st-etienne fr                                  *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "gdatabase.h"
#include <fstream>

GDatabase::GDatabase()
{
}


GDatabase::~GDatabase()
{
}

void GDatabase::read ( string filename )
{
  GGraph *graph;
  vector<string> tokens;
  unsigned int frameid;

  Data *data = new Data(filename);

  //read line of type "t"
  data->getLine(&tokens); 
  frameid = atoi(tokens[1].c_str());
  
  while ( !data->finished() ) 
    {
      graph = new GGraph ();
      graph->read(data, &frameid); //read one graph in file and store it
      process ( graph );
      graphs.push_back ( graph );
    }
  delete data;
} 


void GDatabase::process ( GGraph *graph ) 
{ // used in GDatabase::read to compute the tid list and freq of edges
  // of graph.
  // the frequency (and tid list) of each edge is stored in
  // GDatabase.tokendata

  pair<GToken,GTokenDatabaseData> d;
  d.first.from = 0;
  d.first.to = 1;
  d.first.direction = forward;
  d.second.frequency = 1;

  /* for each edge of the graph (ie for each node and each edge of origin
     this node)*/
  for ( int i = 0, size = graph->nodes.size (); i < size; ++i ) {
    GGraphNode &node = graph->nodes[i];
    d.first.fromdata = node.data;
    vector<GGraphEdge> &edges = graph->nodes[i].edges;

    for ( int j = 0, size2 = edges.size (); j < size2; ++j ) 
      { // for each edge, its code (if cannonical) is inserted
	// in the extension list
	d.first.data = edges[j].data;
	d.first.todata =  graph->nodes[edges[j].dest_nid].data;
	
	if ( d.first.fromdata >= d.first.todata ) 
	  // if this test is false, the code of the edge is not canonical,
	  // so it is not included in the extension list.
	  { 
	    // insert the edge code in the map of all edge codes
	    pair<map<GToken,GTokenDatabaseData,GTokenLt>::iterator,bool> 
	      p = tokendata.insert ( d );

	    /* if the edge was already in tokendata, frequency++ only if its
	       last occurrence was in another graph*/
	    if (!p.second && p.first->second.tids.back().tid != (GTid) graph)
	      p.first->second.frequency++;

	    GTidEid g;
	    g.tid = (GTid) graph;
	    g.nodeid = i;
	    g.edgeid = j;
	    // store this occurrence in the tid list
	    p.first->second.tids.push_back(g);
	  }
      }
  }
}
