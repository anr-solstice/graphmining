/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "ggraph.h"

GGraph::GGraph()
{
  code = NULL;
}


GGraph::~GGraph()
{
}




/*!
    \fn GGraph::addNode ( GNodeData nodedata )
 */
void GGraph::addNode ( GNodeData nodedata )
{
  GGraphNode gn;
  gn.data = nodedata;
  nodes.push_back ( gn );
}


/*!
    \fn GGraph::delNode ()
 */
void GGraph::delNode ()
{
  nodes.pop_back ();
}


/*!
    \fn GGraph::addEdge ( GNodeID from, GNodeID to, GEdgeData edgedata )
 */
void GGraph::addEdge ( GNodeID from, GNodeID to, GEdgeData edgedata )
{
  GGraphEdge ge1, ge2;
  
  ge1.data = edgedata;
  ge1.nodeid = to;
  ge2.data = edgedata;
  ge2.nodeid = from;
  nodes[from].edges.push_back ( ge1 );
  nodes[to].edges.push_back ( ge2 );
}


/*!
    \fn GGraph::delEdge ( GNodeID from, GNodeID to )
 */
void GGraph::delEdge ( GNodeID from, GNodeID to )
{
  vector<GGraphEdge> &edges1 = nodes[from].edges;
  
  for ( vector<GGraphEdge>::iterator i = edges1.begin (); i != edges1.end (); ++i )
    if ( i->nodeid == to ) {
      edges1.erase ( i );
      break;
    }
  
  vector<GGraphEdge> &edges2 = nodes[to].edges;
  
  for ( vector<GGraphEdge>::iterator i = edges2.begin (); i != edges2.end (); ++i )
    if ( i->nodeid == from ) {
      edges2.erase ( i );
      break;
    }
}



void end_of_line(istream& stream) 
{ //baptiste
  char m;
  do m = stream.get (); 
  while ( m != '\n' ); 
}


void end_of_line_only_spaces(istream& stream, int linenb) 
{ //baptiste
  char m = ' ';
  do {
    if (m != ' ') {
      cout << "Unexpected character at line " << linenb << endl;
      exit(1);
    }
    m = stream.get (); 
  }
  while ( m != '\n' ); 
}

//new
void GGraph::read(Data *data)
{
	vector<string> tokens;
	data->getLine(&tokens); //read line of type "n"

	data->getLine(&tokens);
	while (tokens[0] == "v")
        {
	    GNodeData nd = atoi(tokens[2].c_str()); //label of the vertex

	    addNode ( nd ); 
	    data->getLine(&tokens);
	}	

 	while (tokens[0] == "e")
	{
	    GNodeID  ni1 = atoi(tokens[1].c_str());; // id from
	    GNodeID  ni2 = atoi(tokens[2].c_str());; // id to
	    GEdgeData ed = atoi(tokens[3].c_str());; // label edge

	    addEdge ( ni1, ni2, ed );
	    data->getLine(&tokens);
	} 
	//the last line read was of type "t" 
}


/* istream& operator>>(istream& stream, GGraph &graph )
{
  char m;
  GNodeData nd;
  GEdgeData ed;
  GNodeID ni, ni1, ni2;
  int line_nb, edge_nb; // to check input

 
   // baptiste
  //new
  // remove the comments (lines starting with a '#')
  line_nb = 1;
  m = stream.get ();
  while (! stream.eof() && m == '#') {
    cout << "Remove comment line:" << line_nb << endl;
    end_of_line(stream);
    line_nb++;
    m = stream.get (); // first character of next line
  }
 
  if ( stream.eof () )
    return stream;

  // first line of graph (line starting with 't')
  if (m != 't') {
    cout << "Bad input file format at line "<< line_nb << ". ";
    cout << "First line of graph should be \"t ...\"" << endl;
    exit(1);
  }
  end_of_line(stream);
  line_nb++;

  /////////////

  m = stream.get ();
  while ( !stream.eof () && m == 'v' ) {
    stream >> ni;
    stream >> nd;
    graph.addNode ( nd );
    do {
      m = stream.get (); // get end of line
    } 
    while ( m != '\n' ); 
    m = stream.get ();
  }
  while ( !stream.eof () && m == 'e' ) {
    stream >> ni1;
    stream >> ni2;
    stream >> ed;
    graph.addEdge ( ni1, ni2, ed );
    do {
      m = stream.get (); // get end of line
    }
    while ( m != '\n' ); 
    m = stream.get ();
  }
  
  stream.unget (); 
  stream.clear (); // also unput eof if we read 
  
  return stream;
} */

ostream& operator<<(ostream& stream, GGraph &graph )
{
  for ( int i = 0, n = graph.nodes.size (); i < n; ++i )
    stream << "v " << i << " " << graph.nodes[i].data << endl;
    
  for ( int i = 0, n = graph.nodes.size (); i < n; ++i ) {
    vector<GGraphEdge> &edges = graph.nodes[i].edges;
    for ( int j = 0, m = edges.size (); j < m; ++j )
      if ( i < edges[j].nodeid )
        stream << "e " << i << " " << edges[j].nodeid << " " << edges[j].data << endl;
  }
    
  return stream;
}


/*!
    \fn GGraph::isEdge ( GNodeID from, GNodeID to, GEdgeData data )
 */
bool GGraph::isEdge ( GNodeID from, GNodeID to, GEdgeData data )
{
  if ( nodes[from].edges.size () > nodes[to].edges.size () ) {
    for ( int i = 0, size = nodes[to].edges.size (); i < size; ++i )
      if ( nodes[to].edges[i].nodeid == from && nodes[to].edges[i].data == data )
        return true;
    return false;
  }
  else {
    for ( int i = 0, size = nodes[from].edges.size (); i < size; ++i )
      if ( nodes[from].edges[i].nodeid == to && nodes[from].edges[i].data == data )
        return true;
    return false;
  }
}
