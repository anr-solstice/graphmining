
#include "../data/data.h"

#include <string.h>
#include <stdlib.h>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

//indexes of each specific information in the input file
#define indexFrame 1
#define indexVertice 1
#define indexLabel 2
#define indexX 4
#define indexY 5
#define indexSize 6
#define indexR 7
#define indexG 8
#define indexB 9

#define numAttr 5

typedef vector<string> Line;

typedef struct _TRIANGLE
{
  int v1, v2, v3;
}Triangle;


typedef struct _EDGE
{
  int vertice;
  int edgeid;
  bool used;
}Edge;


typedef struct _VERTICE
{
  int outerface;
  int label;
  int x;
  int y;
  int size;
  int R;
  int G;
  int B;
  vector<Triangle> tr;
  vector<Edge> neighbours;
}Vertice;

void flushGraph( int graphid, int numEdges, vector<Vertice> &vv );
bool process(const char *fn, int graphid);

ofstream out; //output file

bool process(const char *fn, int graphid)
{
  // fn : file name without extension
  // graphid : frame id of graph
  cout << "*******" << graphid << endl;

  //read vertices
  ostringstream fileStr;
  fileStr << fn << ".1.node";

  string file(fileStr.str());
  ifstream in;
  in.open (file.c_str(), ifstream::in);
  if (!in.is_open()){
    cout << "Error: could not open "<< file <<endl;
    return false;
  }

  cout << "Reading vertices..." << endl;

  int numVertices;
  Vertice v;
  vector<Vertice> vv;
  string s;
	
  in >> numVertices; 
  in >> s; //dummy
  in >> s; //dummy
  in >> s; //dummy

  vv.resize(numVertices);	

  int i = 0;
  while (i<numVertices)
    {
      in >> s;
      in >> v.x;
      in >> v.y;
      in >> v.label;
      in >> v.size;
      in >> v.R;
      in >> v.G;
      in >> v.B;
      in >> v.outerface;

      vv[i] = v;
      vv[i].tr.resize(0);
      i++;
    }
  in.close();

  cout << "Reading triangles..." << endl;

  //read triangles
  fileStr.str("");
  fileStr << fn << ".1.ele";

  file = fileStr.str();
  in.open (file.c_str(), ifstream::in);
  if (!in.is_open()){
    cout << "Error: could not open "<< file <<endl;
    return false;
  }
  Triangle t;
  int numTriangles;
	
  in >> numTriangles;
  in >> s; //dummy
  in >> s; //dummy

  i = 0;
  while (i<numTriangles)
    {
      in >> s; //skip id of triangle
      in >> t.v1;
      in >> t.v2;
      in >> t.v3;

      //for each vertice v in the triangle, keep the triangles in which v participates
      vv[t.v1].tr.push_back(t);
      vv[t.v2].tr.push_back(t);
      vv[t.v3].tr.push_back(t);
					
      i++;
    }
  in.close();

  vector<int> next(numVertices,-1);
  vector<int> prev(numVertices,-1);
  vector<vector<int> > edgeid(numVertices,next);

	
  cout << "Generating edges..." << endl;
  int id=0;
  for (i=0;i<numVertices;i++) //for each vertice v...
    {

      if (vv[i].tr.size() > 0) 
	{
	  vector<Triangle> &vt = vv[i].tr;

	  //initializing vectors next and prev
	  for (int h=0;h<numVertices;h++)
	    next[h]=-1;

	  for (int h=0;h<numVertices;h++)
	    prev[h]=-1;
	  //--------------------

	  //constructing vectors next and prev
	  for (size_t j=0;j<vt.size();j++) //for each triangle in which v participate...
	    {
	      if (vt[j].v1 == i)
		{
		  next[vt[j].v2] = vt[j].v3;
		  prev[vt[j].v3] = vt[j].v2;
		}
	      else if (vt[j].v2 == i)
		{
		  next[vt[j].v3] = vt[j].v1;
		  prev[vt[j].v1] = vt[j].v3;
		}
	      else
		{
		  next[vt[j].v1] = vt[j].v2;
		  prev[vt[j].v2] = vt[j].v1;
		}
	    }
		
	  /*cout << "vertice " << i << " " << vv[i].outerface << endl;
	    for (int h=0; h<numVertices;h++)
	    cout << "next de " << h << " is " << next[h] << endl;
	    cout << endl << endl; */

	  int k=0;
	  int firstk;
	  if (vv[i].outerface == 1)
	    {
	      while (prev[k]==-1)
		k++;

	      while (prev[k]!=-1)
		k = prev[k];
	      firstk = -1;
	    }
	  else
	    {
	      while (next[k]==-1)
		k++;
	      firstk = k;
	    }

	  //construct the edges of vertice v...
	  do
	    {
	      Edge e;
	      e.vertice = k;
	      e.used = true; 

	      int numEdge = edgeid[i][k];
	      if (numEdge == -1)
		{
		  edgeid[i][k] = id;
		  edgeid[k][i] = id;
		  numEdge = id;
		  id++;
		  e.used = false;
		}
	      e.edgeid = numEdge; 
	      vv[i].neighbours.push_back(e); 
	      k = next[k];

	    }while (k!=firstk);

	} //end "for each triangle"

    } //end "for each vertice"

  cout << "Updating File..." << endl;
  flushGraph(graphid, id, vv);

  return true;
}


void flushGraph( int graphid, int numEdges, vector<Vertice> &vv )
{
  out << "t " << graphid << endl; 
  out << "n " << numEdges << endl;

  for (size_t i=0; i<vv.size();i++)
    out << "v " << i << " " << vv[i].label << " " << vv[i].outerface << " " << vv[i].x << " " << vv[i].y << " " << vv[i].size << " " << vv[i].R << " " << vv[i].G << " " << vv[i].B << endl;

  for (size_t i=0; i<vv.size();i++)
    for (size_t j=0;j<vv[i].neighbours.size();j++)
      out << "e " << i << " " << vv[i].neighbours[j].vertice << " 0 " << vv[i].neighbours[j].edgeid << endl;
}


int main(int argc, char *argv[])
{
  if(argc != 3)
    {
      cerr << "usage: " << argv[0] << " <input_file> <frame_id> " << endl;
      cerr << "  Generate <input_file>_graph.txt from <input_file>.1.node and" << endl;
      cerr << "  <input_file>.1.ele (which are output of triangle)." << endl;
      cerr << "The first line of the generated graph is \"t <frame_id>\"" << endl;
      return -1;
    }

  ostringstream fileStr;
  fileStr << argv[1] << "_graph.txt";
  string file(fileStr.str());
  out.open(file.c_str(), ofstream::out);
  if (!out.is_open()){
    cerr << "Error creating output file " << file << endl;
    return -1;
  }

  // generates the output file from the files in triangles/
  if (!process(argv[1], atoi(argv[2])))
    {
      cerr << "Error writing output file!" << file << endl;
      return -1;
    } 
  out.close();

  cout << endl << "***Triangulation successfully executed***" << endl << endl;
  return 0;
}
