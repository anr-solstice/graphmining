/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *   Copyright (C) 2010,2011 by Adriana Prado and Baptiste Jeudy           *
 *   baptiste.jeudy at univ-st-etienne fr                                  *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef GGRAPH_H
#define GGRAPH_H


#include <stdlib.h>
#include <iostream>
#include <vector>
#include <istream>
#include <ostream>
#include "global.h"
#include "ggraphcode.h"
#include "../data/data.h" 


/* Class to represent graphs.
 
   The graphs are represented by adjacency lists: each nodes[i] is a
   GGraphNode struct containing an vector of the edges around node i.  
   An edge (i,j,label) of the graph appears at position id1 in
   the edges of i and at position id2 in the edges of j:
   in nodes[i].edges[id1] :
         data = label
	 dest_nid = j
	 eq_eid = id2
   and in nodes[j].edges[id2] :
         data = label
	 dest_nid = i
	 eq_eid = id1
 
   The graph edge (i,j,label) is thus represented in the data-structure as
   two "equivalent" edges (corresponding to darts in combinatorial maps..)

   Each vector nodes[i].edges is seen as a circular list except if
   nodes[i].outerface is true (there is a notion of "next edge").
*/ 


struct GGraphEdge {
  GEdgeData data;   // label of the edge
  GNodeID dest_nid; // destination node of the edge
  GEdgeID eq_eid; //edgeid;   // the equivalent edge is nodes[dest_nid].edges[eq_eid]
};

struct GGraphNode {
  GNodeData data; // label of the node
  // edges of origin the node in clockwise order (circular list)
  vector<GGraphEdge> edges; 
  bool outerface; // tells if the node is part of the outer face
  // if it is part of the outer face, "edges" field is not circular.
};

class GGraph{
 public:
  vector<GGraphNode> nodes; // the graph
  // The frame the graph represent (for database graphs)
  // or the id of the graph (for pattern graphs)
  unsigned int frameid; 
  
  GGraph();
  ~GGraph();

  void addNode ( GNodeData nodedata, bool outerface);
  void delNode();
  void addEdge ( GNodeID from, GNodeID to, GEdgeData edgedata, 
		 GEdgeID edgeid, vector<GEdgeID> &equiv_edges );
  void addEdge ( GNodeID from, GNodeID to, GEdgeData edgedata, 
		 GTokenDirection direction );
  void delEdge ( GNodeID from, GNodeID to, GTokenDirection direction );

  // return a pointer on the next edge (or NULL if no next edge)
  // edgeid2 is the id of the next edge (ie either 0 or edgeid1 + 1)  
  GGraphEdge *nextEdge(GNodeID nodeid, GEdgeID edgeid, GEdgeID *edgeid2);

  // return the index of the equivalent edge in the destination node edge list
  GEdgeID findEqEdge(GNodeID nodeid, GEdgeID edgeId);

  //update outer face flag
  void updateNode(GNodeID nodeid, bool flag); 

  // Read one graph in the input file and return it in a GGraph object
  // frameid is changed in this function (thus it is a pointer)
  void read(Data *data, unsigned int *frameid);
};

// to print a graph
ostream& operator<<(ostream& stream, GGraph &graph );

#endif
