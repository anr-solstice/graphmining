/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *   Copyright (C) 2010,2011 by Adriana Prado and Baptiste Jeudy           *
 *   baptiste.jeudy at univ-st-etienne fr                                  *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "gpattern.h"
#include "gcanonicaldfscomputer.h"

GPattern::GPattern() {
}

GPattern::~GPattern(){
}


bool GPattern::quickPrune( const GToken &token)
{ // true means adding token to the code would yield a non-canonical code
  // false means we don't know if the code would be canonical

  // if the new node or the new edge is > first edge of the code, then 
  // the code cannot be cannonical because a canonical code always start
  // with the greatest edge.
  if ( token.todata > tokens[0].fromdata ) 
    // the cannonical code should start from the "to" node of token
    return true;
  if (token.fromdata == tokens[0].fromdata && ( token.data > tokens[0].data || 
           ( token.data == tokens[0].data && token.todata > tokens[0].todata)))
    // token edge > first edge of code
    return true; 
  return false;
}

void GPattern::setup ( const GToken &token ) 
{ // To set up the pattern, called when adding the first extension to 
  // an empty pattern (only called by push_back)

  // the graph contains 2 nodes and one edge
  if ( !graph.nodes.empty () ) graph.delNode ();
  graph.addNode(token.fromdata, true );
  graph.addNode(token.todata, true );
  graph.addEdge(token.from, token.to, token.data, token.direction);
  
  // undo info. not important: the first extension will never be undone.
  GUndo u;
  undo.push_back(u);
  undo.back().start = 0;
  undo.back().startNext = 1;
  undo.back().numRemoved = 0;

  // activenodes list 
  nextactive.resize( 0 );
  nextactive.push_back(1); // nextactive[0] is 1
  nextactive.push_back(0); // nextactive[1] is 0

  isactive.resize( 0 );
  isactive.push_back(true); // node 0 is active
  isactive.push_back(true); // node 1 is active

  activenodesbegin = 0;
  activenodesend = 2;  

  // the code
  tokens.push_back(token);
}


void GPattern::push_back ( const vector<GToken> &vtoken ) {
  /* Add a new extension (path) to the pattern: update graph, code and
     undo info. ie:
     - add edges to the code
     - add edges and nodes to the graph
     - change the list of activenodes (nodes on the outer face)
     - store information to be able to undo the changes later */
  /* the first extension added to an empty pattern must be a single edge,
     then all other extensions must be valid */


  if (tokens.size() == 0) {
    // if the pattern is empty (first level): special case
    setup(vtoken[0]);
    if (vtoken.size() > 1) {
      cerr << "Internal error GPattern::push_back " << vtoken.size() << endl;
      exit (1);
    }
    return;
  }

  /* Ends of the new extension path.
     The nodes that lies between them in activenodes are no
     longer on the outer face and should be removed from activenode list.
  */
  GNodeID start = vtoken[0].from;
  GNodeID stop = vtoken.back().to;

  // to be able to undo the changes when the vtoken is pop_back later
  GUndo u;
  undo.push_back(u);
  undo.back().start = start;
  undo.back().startNext=nextactive[start];
  undo.back().numRemoved=0;
	
  //remove nodes between start and stop from active nodes list
  GNodeID lastRemovedNext = 0;
  for (GNodeID i = nextactive[start]; i != stop; i = nextactive[i])
    {
      isactive[i] = false; 
      lastRemovedNext = nextactive[i];

      updateNode(i,false); 
      undo.back().numRemoved++;
    }
  if (undo.back().numRemoved>0)
    nextactive[start] = lastRemovedNext;

  // insert nodes of the extension path in the active nodes list
  // and add the edges and nodes of the extension path in the code and graph
  if (nextactive.size() < activenodesend+vtoken.size()-1)
    nextactive.resize ( nextactive.size() + vtoken.size() -1); 
  if (isactive.size() <= vtoken[vtoken.size()-1].from)
    isactive.resize ( vtoken[vtoken.size()-1].from + 1,GNONODEID );
  
  GNodeID i = start; // the new nodes are inserted after start in activenodes
  GNodeID startNext = nextactive[start];
  for (uint j=0; j<vtoken.size(); j++) {
    tokens.push_back ( vtoken[j] ); // add the edge to the code
    if (vtoken[j].direction == forward) {
      graph.addNode(vtoken[j].todata, true); // add new node to the graph 
     // insert node vtoken[j].to in activenodes list after node i
      if (activenodesend != vtoken[j].to) cout << "hoho" << endl; 
      nextactive[i] = activenodesend;
      isactive[vtoken[j].to] = true;
      i = activenodesend; 
      activenodesend++; 
    }
    graph.addEdge(vtoken[j].from, vtoken[j].to, vtoken[j].data, 
		  vtoken[j].direction ); // add edge to the graph
  }
  nextactive[i] = startNext;
  activenodesbegin = start;
}


void GPattern::pop_back (const vector<GToken> &vtoken) 
{ //remove an extension from the pattern. Undo what was done in push_back
  GUndo &u = undo.back();

  // undo changes in active nodes list
  activenodesbegin = u.start;
  nextactive[u.start] =  u.startNext;
  GNodeID i=u.startNext;
  uint j;
  // put back active nodes that were removed
  for (j=0; j<u.numRemoved; j++) { 
    isactive[i] = true;
    updateNode(i, true);
    i = nextactive[i];
  }
  // remove active nodes that were inserted
  activenodesend = activenodesend - (vtoken.size()-1);
  for (j=0;j<vtoken.size()-1;j++)
    isactive[vtoken[j].to] = false;

  // remove added nodes and edges in the graph and code
  for (j=0; j < vtoken.size();j++) {
    GToken &token = tokens.back(); 
    graph.delEdge(token.from, token.to, token.direction); // rm edge from graph
    if ( token.direction == forward )
      graph.delNode (); // remove node from graph
    tokens.pop_back (); // remove edge from code
  }
  undo.pop_back(); // remove undo info
} 


void GPattern::updateNode (GNodeID nodeid, bool flag) {
  // change the flag (which indicate if the node is in outerface)
  graph.updateNode(nodeid, flag);
}


bool GPattern::isCanonical ()
{
  GCanonicalDFSComputer computer ( this );
  return computer.isCanonical();
}
 

void GPattern::printActiveNodes() {
  // print the activenodes list. Can be used to debug
  int i= activenodesbegin;
  
  cerr << activenodesbegin << " " << "**active nodes: " << 
    i << " -> " ;
  i = nextactive[i];
  while (i!=activenodesbegin) {
    cerr << i << " -> ";
    i = nextactive[i];
  }
  cerr << endl << endl;
}
 
