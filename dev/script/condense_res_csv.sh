#!/bin/bash

DIR=./*W.txt
    echo "Set,BinSize,Step,Words,rules.ZeroR.Train,rules.ZeroR.Test,trees.j48.Train,trees.j48.Test,bayes.NaiveBayes.Train,bayes.NaiveBayes.Test,rules.JRip.Train,rules.JRip.Test,meta.ClassificationViaRegression.Train,meta.ClassificationViaRegression.Test,functions.SMO.Train,functions.SMO.Test,	lazy.KStar.Train,lazy.KStar.Test" >> condense1.csv;

for f in $DIR
do
    line=$(grep -n "=== Stratified cross-validation ===" $f | cut -d ':' -f1);
    echo $line;
    b=$(echo $line | cut -d ' ' -f1);
    cl=$(sed "$b!d" $f)
    c1=${cl:${#cl}-10:10}
    b=$(echo $line | cut -d ' ' -f2);
    cl=$(sed "$b!d" $f)
    c2=${cl:${#cl}-10:10}
    b=$(echo $line | cut -d ' ' -f3);
    cl=$(sed "$b!d" $f)
    c3=${cl:${#cl}-10:10}
    b=$(echo $line | cut -d ' ' -f4);
    cl=$(sed "$b!d" $f)
    c4=${cl:${#cl}-10:10}
    b=$(echo $line | cut -d ' ' -f5);
    cl=$(sed "$b!d" $f)
    c5=${cl:${#cl}-10:10}
    b=$(echo $line | cut -d ' ' -f6);
    cl=$(sed "$b!d" $f)
    c6=${cl:${#cl}-10:10}
    b=$(echo $line | cut -d ' ' -f7);
    cl=$(sed "$b!d" $f)
    c7=${cl:${#cl}-10:10}
    b=$(echo $line | cut -d ' ' -f8);
    cl=$(sed "$b!d" $f)
    c8=${cl:${#cl}-10:10}
    b=$(echo $line | cut -d ' ' -f9);
    cl=$(sed "$b!d" $f)
    c9=${cl:${#cl}-10:10}
    b=$(echo $line | cut -d ' ' -f10);
    cl=$(sed "$b!d" $f)
    c10=${cl:${#cl}-10:10}
    b=$(echo $line | cut -d ' ' -f11);
    cl=$(sed "$b!d" $f)
    c11=${cl:${#cl}-10:10}
    b=$(echo $line | cut -d ' ' -f12);
    cl=$(sed "$b!d" $f)
    c12=${cl:${#cl}-10:10}
    b=$(echo $line | cut -d ' ' -f13);
    cl=$(sed "$b!d" $f)
    c13=${cl:${#cl}-10:10}
    b=$(echo $line | cut -d ' ' -f14);
    cl=$(sed "$b!d" $f)
    c14=${cl:${#cl}-10:10}
    echo "$f,0,0,0,$c1,$c2,$c3,$c4,$c5,$c6,$c7,$c8,$c9,$c10,$c11,$c12,$c13,$c14" >> condense1.csv;
    
done

#>>  condense1.csv;

