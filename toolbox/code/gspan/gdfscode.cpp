/***************************************************************************
 *   Copyright (C) 2005 by Siegfried Nijssen                               *
 *   snijssen@informatik.uni-freiburg.de                                   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "gdfscode.h"
#include "gcanonicaldfscomputer.h"

GDFSCode::GDFSCode()
{
  activenodesbegin = activenodesend = 0;
}


GDFSCode::~GDFSCode()
{
}


bool GDFSCode::quickPruneBackward ( const GToken &token ) {        
  return parents[token.from] == token.to ||
         token.to > token.from ||
         in_activenodes[token.to] >= activenodesend ||
         ( tokens.back ().to != token.from && 
           cmpGToken ( token, tokens.back () ) >= 0 );
           // we have a tricky mechanism for flagging nodes on the rightmost path.
           // Every node has either GNONODEID (which is a very high number),
           // or the position in the activenodes array,
           // even beyond the activenodesend.
           // Nodes after activenodesend will only be set correctly after we enlarge the graph.
           // Everything after activenodesend is a remainder of earlier pushes/pops.
}

bool GDFSCode::quickPruneForward ( const GToken &token ) {        
   // compare with root label
  if ( token.todata > tokens[0].fromdata )
    return true;
  GToken &token2 = tokens.back ();
  if ( token.from != token2.to &&
       ( token.from > token2.from ||
         ( token.from == token2.from &&
           ( token.data > token2.data ||
             ( token.data == token2.data &&
               token.todata > token2.todata 
             )
           ) 
         )
       )
     )
    return true; 
  return false;
}

void GDFSCode::setup ( const GToken &token ) {
  GSearchCode::setup ( token );
  
  activenodes.resize ( 0 );
  activenodes.push_back ( 0 );
  activenodesend = 1;
  
  in_activenodes.resize ( 1 );
  in_activenodes[0] = 0;
  
  parents.resize ( 1 );
  parents[0] = GNONODEID;
}

void GDFSCode::push_back ( const GToken &token ) {
  GSearchCode::push_back ( token );
  
  int index = in_activenodes[token.from] + 1;
  if ( activenodes.size () <= index )
    activenodes.resize ( index + 1, GNONODEID );
  
  undo.push_back ( make_pair ( activenodes[index], activenodesend ) ) ;
  
  if ( token.direction == forward ) {
    // clean up in_activenodes such that we correctly flag this node
    // as not active using the >=activenodesend test.
    // (as in_activenodes[inactivenodes[index]] would be <activenodesend, while not active)
    if ( activenodes[index] != GNONODEID )
      in_activenodes[activenodes[index]] = GNONODEID;
    
    activenodes[index] = token.to;
    activenodesend = index + 1;
    if ( in_activenodes.size () <= token.to ) {
      in_activenodes.resize ( token.to + 1, GNONODEID );
      parents.resize ( token.to + 1, GNONODEID );
    }
    in_activenodes[token.to] = index;
    parents[token.to] = token.from;
  }
  else 
    activenodesend = index;
}

void GDFSCode::pop_back () {
  GToken &token = tokens.back ();
  int index = in_activenodes[token.from] + 1;
  pair<GNodeID,int> p = undo.back () ;
  undo.pop_back ();
  if ( token.direction == forward ) {
    activenodes[index] = p.first;
    activenodesend = p.second;
    in_activenodes[token.to] = GNONODEID;
    if ( p.first != GNONODEID )
      in_activenodes[p.first] = index;
  }
  else 
    activenodesend = p.second;
  
  GSearchCode::pop_back ();
}

bool GDFSCode::isCanonical () {
  GCanonicalDFSComputer computer ( &graph );
  return computer.isCanonical ( this );
}